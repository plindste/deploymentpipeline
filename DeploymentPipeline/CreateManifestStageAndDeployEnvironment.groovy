#!groovy
@Library('jenkinsCommonLibrary') _


properties([
    [$class         : 'RebuildSettings',
     autoRebuild    : false,
     rebuildDisabled: false],
    parameters([
		string(
            name: 'STACK_NAME',
            defaultValue: '',
            description: 'Specify stack name if it is not specified then it will be taken from branch name.'),
        booleanParam(
            defaultValue: true,
            description: 'Set this to False if you do not want to stage stuff',
            name: 'STAGE_TO_S3'),
        booleanParam(
            defaultValue: true,
            description: 'Set this to False if you do not want to deploy an environment',
            name: 'DEPLOY_ENVIRONMENT'),
		booleanParam(
			defaultValue: false, 
			description: 'Set this check box if you want to deploy network stack.', 
			name: 'DEPLOY_NETWORK_STACK'),
		booleanParam(
			defaultValue: true, 
			description: 'Set this check box if you want to deploy RDS stack.', 
			name: 'DEPLOY_RDS_STACK'),
		booleanParam(
			defaultValue: true, 
			description: 'Set this check box if you want to deploy application stack.', 
			name: 'DEPLOY_APP_STACK'),
		booleanParam(
			defaultValue: false, 
			description: 'Set this check box if you want to perform service instance status check.', 
			name: 'CHECK_SERVICE_INSTANCE_STATUS'),
		booleanParam(
			defaultValue: false, 
			description: 'Set this check box if you want to perform service instance status check.', 
			name: 'EXECUTE_BRC'),
		booleanParam(
			defaultValue: false, 
			description: 'Set this check box if you want to terminate the environment at the end automatically.', 
			name: 'TERMINATE_ENV_AT_THE_END'),	
		string(
            name: 'MANIFEST_V_NUMBER',
            defaultValue: '',
            description: 'Keep it blank if you want to auto populate else specify the Manifest V number as Vx (Note: Existing contents will get replace so be careful)'),	
        string(
            name: 'RELEASE_VERSION',
            defaultValue: '',
            description: 'Specify release version for manifest file. If not specified, job will take it from branch name/version.txt file'),
        string(
            name: 'TSS_VERSION',
            defaultValue: '',
            description: 'TIBCO Spotfire Server version to pick up. If not specified, job will take it from branch name/version.txt file'),
        string(
            name: 'TSS_BUILD_NO',
            defaultValue: 'Latest',
            description: 'Specify build number you can specify specific number as V20 ^V indicates pick up the latest v build number starts with V'),
        string(
            name: 'TD_VERSION', 
            defaultValue: '1.4.0', 
            description: 'TIBCO Drivers version to pick up'),
        string(
            name: 'TD_BUILD_NO',
            defaultValue: '^V',
            description: 'Specify build number you can specify specific number as V20 ^V indicates pick up the latest v build number starts with V'),
        string(
            name: 'CLOUDPORTAL_VERSION',
            defaultValue: '',
            description: 'Cloud portal version to pick up. If not specified, job will take it from branch name/version.txt file'),
        string(
            name: 'CLOUDPORTAL_BUILD_NO',
            defaultValue: '^V',
            description: 'Specify build number you can specify specific number as V20 ^V indicates pick up the latest v build number starts with V'),
        string(
            name: 'TS_VERSION',
            defaultValue: '',
            description: 'TIBCO Spotfire version to pick up. If not specified, job will take it from branch name/version.txt file.'),
        string(
            name: 'TS_BUILD_NO',
            defaultValue: '^V',
            description: 'Specify build number you can specify specific number as V20 ^V indicates pick up the latest v build number starts with V'),
        string(
            name: 'DS_VERSION',
            defaultValue: '',
            description: 'Deployment scripts version to pick up. If not specified, job will take it from branch name/version.txt file'),
        string(
            name: 'DS_BUILD_NO',
            defaultValue: '^V',
            description: 'Specify build number you can specify specific number as V20 ^V indicates pick up the latest v build number starts with V'),
        string(
            name: 'DEPLOYMENT_SCRIPTS_BRANCH',
            defaultValue: '',
            description: 'Specify name of the branch for deployment scripts and products yaml file if not specified it will be same as branch name of job'),
        string(
            name: 'TSCE_MANIFEST_FILE_NAME',
            defaultValue: 'TSCE-manifest.yml',
            description: 'TSCE Manifest file name to generate'),
        string(
            name: 'TSCE_PRODUCTS_YML',
            defaultValue: 'tsce_products.yml',
            description: 'Products yml file to use as input to create manifest file'),
		string(
            name: 'TSCL_MANIFEST_FILE_NAME',
            defaultValue: 'TSCL-manifest.yml',
            description: 'TSCL Manifest file name to generate'),
        string(
            name: 'ONPREM_MANIFEST_FILE_NAME',
            defaultValue: 'ONPREM-manifest.yml',
            description: 'ONPREM Manifest file name to generate'),
        string(
            name: 'SAWS_MANIFEST_FILE_NAME',
            defaultValue: 'SAWS-manifest.yml',
            description: 'SAWS Manifest file name to generate'),
        string(
            name: 'TSC_MANIFEST_FILE_NAME',
            defaultValue: 'TSC-manifest.yml',
            description: 'TSC Manifest file name to generate'),
        string(
            name: 'TSCL_PRODUCTS_YML',
            defaultValue: 'tscl_products.yml',
            description: 'Products yml file to use as input to create manifest file'),
        string(
            name: 'SAWS_PRODUCTS_YML',
            defaultValue: 'saws_products.yml',
            description: 'Products yml file to use as input to create manifest file'),
        string(
            name: 'ONPREM_PRODUCTS_YML',
            defaultValue: 'onprem_products.yml',
            description: 'Products yml file to use as input to create manifest file'),
        string(
            name: 'TSC_PRODUCTS_YML',
            defaultValue: 'tsc_products.yml',
            description: 'Products yml file to use as input to create manifest file')        

    ])
])


try {
  Map<String, String> map1 = new HashMap<String, String>();

  stage('Checkout') {
    node('Gothenburg-Staging') {
      deleteDir()
      checkout scm
	}
  }
  stage('Create manifest') {
    node('Gothenburg-Staging') {
  
     try {
        String BRANCH_VERSION = getBranchVersion()
        String RELEASE_VERSION = getReleaseVersion(BRANCH_VERSION)
        String TSS_VERSION = getTSSVersion(BRANCH_VERSION)
        String CLOUDPORTAL_VERSION = getCloudPortalVersion(BRANCH_VERSION)
        String TS_VERSION = getTSVersion(BRANCH_VERSION)
        String DS_VERSION = getDSVersion(BRANCH_VERSION)
        String DEPLOYMENT_SCRIPTS_BRANCH = getDeploymentScriptsBranch()

        withEnv(['BRANCH_VERSION=' + BRANCH_VERSION,
                 'RELEASE_VERSION=' + RELEASE_VERSION,
                 'CLOUDPORTAL_VERSION=' + CLOUDPORTAL_VERSION,
                 'TSS_VERSION=' + TSS_VERSION,
                 'TS_VERSION=' + TS_VERSION,
                 'DS_VERSION=' + DS_VERSION,
                 'DEPLOYMENT_SCRIPTS_BRANCH=' + DEPLOYMENT_SCRIPTS_BRANCH]) {

          notifyBuildWithMessage(
              'STARTED',
              '#sf_deployinfra',
              'CreateManifestAndDeployTSCE',
              'Job CreateManifestAndDeployTSCE is started')
   
            productsInfo = "\"TIBCO Spotfire Server:" + TSS_VERSION + ":" + env.TSS_BUILD_NO +
                ",TIBCO Drivers:" + env.TD_VERSION + ":" + env.TD_BUILD_NO +
                ",Cloud Portal:" + env.CLOUDPORTAL_VERSION + ":" + env.CLOUDPORTAL_BUILD_NO +
                ",TIBCO Spotfire:" + TS_VERSION + ":" + env.TS_BUILD_NO +
                ",Deployment Scripts:" + DS_VERSION+ ":" + env.DS_BUILD_NO +
                ",Third Party Redistributable Components::\""
            cmdLine = 'sudo -n python ' + env.WORKSPACE + '/Scripts/Python/createManifest.py ' +
                '--release ' + RELEASE_VERSION + ' ' +
                '--products_info ' + productsInfo + ' ' +
                '--manifest_products_directory "TIBCO Spotfire" ' +
                '--manifest_v_number "' + env.MANIFEST_V_NUMBER + '" ' +
                '--tsce_products_yml ' + env.TSCE_PRODUCTS_YML + ' ' +
                '--tscl_products_yml ' + env.TSCL_PRODUCTS_YML + ' ' +
                '--tsc_products_yml ' + env.TSC_PRODUCTS_YML + ' ' +
                '--saws_products_yml ' + env.SAWS_PRODUCTS_YML + ' ' +
                '--onprem_products_yml ' + env.ONPREM_PRODUCTS_YML + ' ' +
                '--tsce_manifest_file ' + env.TSCE_MANIFEST_FILE_NAME + ' ' +
                '--tscl_manifest_file ' + env.TSCL_MANIFEST_FILE_NAME + ' ' +
                '--tsc_manifest_file ' + env.TSC_MANIFEST_FILE_NAME + ' ' +
                '--saws_manifest_file ' + env.SAWS_MANIFEST_FILE_NAME + ' ' +
                '--onprem_manifest_file ' + env.ONPREM_MANIFEST_FILE_NAME + ' ' +
                '--deployment_scripts_branch ' + DEPLOYMENT_SCRIPTS_BRANCH + ' ' +
                '--deployment_scripts_version ' + DS_VERSION + ' '
            
			sh cmdLine
            stash includes: 'manifestFileData.txt', name: 'manifestFile'

			def output = readFile("$WORKSPACE/manifestFileData.txt").trim()
			echo "Read file $WORKSPACE/manifestFileData.txt completed successfully "
			echo output
			def keyvalues = output.split(",")
			print keyvalues.length
			for (int i = 0; i < keyvalues.length; i++) {
			echo keyvalues[i]
			def tokens = keyvalues[i].split("=")
			map1.put(tokens[0], tokens[1])
			}
			
          }
        
      } catch (e) {
        currentBuild.result = "FAILED"
        echo("Issue occured in Create manifest")
        throw e
      }
    }
  }


  stage('Stage to s3') {
    node('Gothenburg-Staging') {
      if ("${env.STAGE_TO_S3}".toBoolean()) {
        echo "from map"
        echo map1.get("MANIFEST_VERSION")
        echo map1.get("ROLES_YAML_BRANCH")
        echo map1.get("ROLES_YAML_VERSION")
        echo map1.get("ROLES_YAML_BUILDNO")
        echo map1.get("MANIFEST_BUILD_NO")

        withCredentials(
            [usernamePassword(
                credentialsId: 'dfe3e01c-7a16-4dd7-84eb-1d84009be10b',
                passwordVariable: 'AWS_SECRET_ACCESS_KEY',
                usernameVariable: 'AWS_ACCESS_KEY_ID')]) {
				        
          cmdLine = 'python2.7 ' + env.WORKSPACE + '/Scripts/Python/stageContentsToS3Sync.py ' +
              ' --manifestVersion ' + map1.get("MANIFEST_VERSION") +
              ' --manifestBuild ' + map1.get("MANIFEST_BUILD_NO") +
              ' --s3bucket s3://spotfire/Content' +
              ' --unc_source_mount_location "//emea-got-filer1.emea.tibco.com/SWFactory" ' +
              ' --unc_domain emea ' +
              ' --unc_user iss-access ' +
              ' --unc_password en0RierL1F ' +
              ' --unc_path_base /mnt/softwarefactory/ ' +
              ' --roles_yml ' + env.WORKSPACE + '/tsc_roles.yml ' +
              ' --manifest_products_directory "TIBCO Spotfire" ' +
              ' --manifest_file "TSC-manifest.yml" '+
			  ' --roles_yml_branch "' + map1.get("ROLES_YAML_BRANCH") + '" ' +
              ' --roles_yml_version "' + map1.get("ROLES_YAML_VERSION") + '" '
          sh cmdLine
        }
		 withCredentials(
            [usernamePassword(
                credentialsId: 'dfe3e01c-7a16-4dd7-84eb-1d84009be10b',
                passwordVariable: 'AWS_SECRET_ACCESS_KEY',
                usernameVariable: 'AWS_ACCESS_KEY_ID')]) {
          cmdLine = 'python ' + env.WORKSPACE + '/Scripts/Python/stageContentsToS3Sync.py ' +
              ' --manifestVersion ' + map1.get("MANIFEST_VERSION") +
              ' --manifestBuild ' + map1.get("MANIFEST_BUILD_NO") +
              ' --s3bucket s3://spotfire/Content' +
              ' --unc_source_mount_location "//emea-got-filer1.emea.tibco.com/SWFactory" ' +
              ' --unc_domain emea ' +
              ' --unc_user iss-access ' +
              ' --unc_password en0RierL1F ' +
              ' --unc_path_base /mnt/softwarefactory/ ' +
              ' --roles_yml ' + env.WORKSPACE + '/tsce_roles.yml ' +
              ' --manifest_products_directory "TIBCO Spotfire" ' +
              ' --manifest_file "TSCE-manifest.yml" '+
			  ' --roles_yml_branch "' + map1.get("ROLES_YAML_BRANCH") + '" ' +
              ' --roles_yml_version "' + map1.get("ROLES_YAML_VERSION") + '" '
          sh cmdLine
        }
      }
    }
  }

  stage('Build TSC deployment pipeline') {
    node('Gothenburg-Staging') {
	def TARGET_ENV=""
	if ("${env.STACK_NAME}".trim().equals(""))
		{
	
			TARGET_ENV="tsc" + env.BRANCH_NAME
		}
		else{
		TARGET_ENV="${env.STACK_NAME}".trim()
		}
		
      if ("${env.DEPLOY_ENVIRONMENT}".toBoolean()) {
        build job: 'TSC-Automation/' + env.BRANCH_NAME, parameters: [
			string(name: 'TARGET_ENVIRONMENT', value: TARGET_ENV), 
			booleanParam(name: 'DEPLOY_NETWORK_STACK',value: Boolean.valueOf(DEPLOY_NETWORK_STACK)),
			booleanParam(name: 'DEPLOY_RDS_STACK',value: Boolean.valueOf(DEPLOY_RDS_STACK)),
			booleanParam(name: 'DEPLOY_APP_STACK',value: Boolean.valueOf(DEPLOY_APP_STACK)),
			booleanParam(name: 'CHECK_SERVICE_INSTANCE_STATUS',value: Boolean.valueOf(CHECK_SERVICE_INSTANCE_STATUS)),
			booleanParam(name: 'EXECUTE_BRC',value: Boolean.valueOf(EXECUTE_BRC)),
			booleanParam(name: 'TERMINATE_ENV_AT_THE_END',value: Boolean.valueOf(TERMINATE_ENV_AT_THE_END)),
			string(name: 'AWS_REGION', value: 'us-west-2'), 
			string(name: 'PRODUCT_VERSION',value: map1.get("MANIFEST_VERSION")), 
			string(name: 'PRODUCT_BUILD',value: map1.get("MANIFEST_BUILD_NO")), 
			string(name: 'TSSA_MINMAXDESIRED', value: '0,2,1'), 
			string(name: 'EDGE_MINMAXDESIRED',value: '0,2,1'), 
			string(name: 'WP_MINMAXDESIRED', value: '0,2,1'), 
			string(name: 'AS_MINMAXDESIRED', value: '0,2,1'), 
			string(name: 'DA_MINMAXDESIRED', value: '0,2,1'), 
			string(name: 'TSS_MINMAXDESIRED',value: '0,2,1'), 
			string(name: 'ST_MINMAXDESIRED', value: '0,2,1'), 
			string(name: 'DEBUG_MODE', value: 'contentdelivery')]
      } else {
        echo "No deployment done."
      }
    }

    notifyBuildWithMessage(
        'SUCCESS',
        '#sf_deployinfra',
        'CreateManifestAndDeployTSCE',
        'Successfully executed job CreateManifestAndDeployTSCE')
  }


} catch (Exception e) {

  echo("Exception occured" + e)
  notifyBuildWithMessage(
      'FAILED',
      '#sf_deployinfra',
      'CreateManifestAndDeployTSCE',
      'Job  CreateManifestAndDeployTSCE failed, error is:' + e)
  error("Exception occured" + e)
}

private String getDeploymentScriptsBranch() {
  DEPLOYMENT_SCRIPTS_BRANCH = (DEPLOYMENT_SCRIPTS_BRANCH.trim().equals(
      "")) ? env.BRANCH_NAME : DEPLOYMENT_SCRIPTS_BRANCH.trim()
  echo("DEPLOYMENT_SCRIPTS_BRANCH value is:" + DEPLOYMENT_SCRIPTS_BRANCH)
  return DEPLOYMENT_SCRIPTS_BRANCH
}

private String getDSVersion(String BRANCH_VERSION) {
  DS_VERSION = (DS_VERSION.trim().equals("")) ? BRANCH_VERSION : DS_VERSION.trim()
  echo("DS_VERSION value is:" + DS_VERSION)
  return DS_VERSION
}

private String getTSVersion(String BRANCH_VERSION) {
  TS_VERSION = (TS_VERSION.trim().equals("")) ? BRANCH_VERSION : TS_VERSION.trim()
  echo("TS_VERSION value is:" + TS_VERSION)
  return TS_VERSION
}

private String getCloudPortalVersion(String BRANCH_VERSION) {
  CLOUDPORTAL_VERSION = (CLOUDPORTAL_VERSION.trim().equals("")) ? BRANCH_VERSION : CLOUDPORTAL_VERSION.trim()
  echo("CLOUDPORTAL_VERSION value is:" + CLOUDPORTAL_VERSION.trim())
  return CLOUDPORTAL_VERSION
}

private String getTSSVersion(String BRANCH_VERSION) {
  TSS_VERSION = (TSS_VERSION.trim().equals("")) ? BRANCH_VERSION : TSS_VERSION.trim()
  echo("TSS_VERSION value is:" + TSS_VERSION)
  return TSS_VERSION
}

private String getReleaseVersion(String BRANCH_VERSION) {
  RELEASE_VERSION = (RELEASE_VERSION.trim().equals("")) ? BRANCH_VERSION : RELEASE_VERSION.trim()
  echo("RELEASE_VERSION value is:" + RELEASE_VERSION)
  assert RELEASE_VERSION =~ /[0-9]+[.][0-9]+[.][0-9]+[[.][0-9]+]*/
  return RELEASE_VERSION
}

private void validateBranchVersion(String BRANCH_VERSION) {
  assert BRANCH_VERSION =~ /[0-9]+[.][0-9]+[.][0-9]+[[.][0-9]+]*/
  echo("Branch Version is:" + BRANCH_VERSION)
}

private String getBranchVersion() {
  String BRANCH_VERSION
  if (env.BRANCH_NAME.startsWith("gen")) {
    echo(
        "Running in branch " + env.BRANCH_NAME +
            " which is a release branch. Setting release version for manifest based on branch name")
    BRANCH_VERSION = env.BRANCH_NAME.replaceFirst('-', '_').replaceAll('-', '.').split('_')[1]
  } else {
    echo(
        "Running in branch " + env.BRANCH_NAME +
            " which is not a release branch. Setting release version for manifest based on version file value");
    BRANCH_VERSION = readFile(env.WORKSPACE + '/version.txt').trim()
  }
  validateBranchVersion(BRANCH_VERSION)
  return BRANCH_VERSION.trim()
}
