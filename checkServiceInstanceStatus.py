#!/usr/bin/env python

""" checkServiceInstanceStatus.py Functionality to compare if the number of different services that is in service
in a Spotfire environment equals the expected number of services."""

import requests
import argparse
import sys

from requests.exceptions import ConnectionError
from requests.packages.urllib3.exceptions import NewConnectionError
from retrying import retry

# TODO: Consider using Python's logger mechanism instead of print statements...'

#
# Global settings:
#
manifest_path = '/spotfire/manifest'
status_list_path = '/spotfire/nodemanager/list/topology/services'  # Where to retrieve instance/service status info from

# String constants for how the services are named when retrieving the status info
tss_key = u'TSS'
wp_key = u'WEB_PLAYER'
as_key = u'AUTOMATION_SERVICES'
da_key = u'DATA_ACCESS'
st_key = u'TERR'

# Time constants for retrying
retry_timeout = 180000  # Retry timeout (30 minutes) to retrieve status if we haven't reached expected state (in ms)
delay_between_retries = 60000  # Delay between consecutive retries (1 min) (in ms)


class ServiceInstanceStatusError(ValueError):
    """Raise when expected state of the target environment does not equal the actual state"""

    def __init__(self, message):
        self.message = message  # without this you may get DeprecationWarning
        # Special attribute you desire with your Error,
        # perhaps the value that caused the error?:
        #        self.value_causing_error = value_causing_error
        # allow users initialize misc. arguments as any other builtin Error
        super(ServiceInstanceStatusError, self).__init__(message)


def check_system_status(environment_url, user, password, expected_tssa, expected_wp, expected_as,
                        expected_da, expected_st):
    """
    Compares expected environment state with actual environment state.
    :rtype: Boolean
    :param environment_url: URL to environment, e g http://tsce7901.spotfire-cloud.com
    :param user: User
    :param password: Password
    :param expected_tssa: Number of expected TSS instances to be online
    :param expected_wp: Number of expected WebPlayer instances to be online
    :param expected_as: Number of expected Automation Services instances to be online
    :param expected_da: Number of expected Data Access instances to be online
    :param expected_st: Number of expected SmarTERR instances to be online
    :return: True if expected state matches actual state. False otherwise.
    """

    expected = {}
    result = False

    # Since actual state only contains info about existing services, any services specified as expected to be zero
    # should not be part of the expected dict.
    if expected_tssa > 0:
        expected[tss_key] = expected_tssa
    if expected_wp > 0:
        expected[wp_key] = expected_wp
    if expected_as > 0:
        expected[as_key] = expected_as
    if expected_da > 0:
        expected[da_key] = expected_da
    if expected_st > 0:
        expected[st_key] = expected_st

    try:
        result = actual_equals_expected(environment_url, user, password, expected)

    except (requests.exceptions.HTTPError, ServiceInstanceStatusError, NewConnectionError, ConnectionError) as err:
        print "Timeout occurred. Issue encountered:"
        print err

    # actual_equals_expected = actual == expected
    #     if not result:
    #         msg = "check_system_status: Expected state does not match actual state.\n" + "\tExpected: " + repr(
    #             expected) + "\n" + "\tActual:   " + repr(actual)
    #         print msg
    return result


# Retry on exceptions for a the set period, waiting between consecutive calls for the set delay
# @retry(stop_max_delay=retry_timeout, wait_fixed=delay_between_retries)
def actual_equals_expected(environment_url, user, password, expected):
    """
    Retrieves actual state from environment
    :rtype: boolean
    :param environment_url: URL to environment, e g http://tsce7901.spotfire-cloud.com
    :param user: Username
    :param password: Password
    :param expected: Expected environment state
    :return: True if actual state equals expected state. False otherwise
    """
    # manifest_path = "/spotfire/manifest"
    # status_list_path = "/spotfire/nodemanager/list/topology/services"
    actual = {}

    # response = requests.get(environment_url + manifest_path)
    # response.raise_for_status()

    try:
        print 'Getting manifest...'
        response = requests.get(environment_url + manifest_path)
    except Exception as e:
        print e
        raise
    response.raise_for_status()  # Raise exception to allow retry in case getting manifest doesn't succeed'

    cookies = response.cookies
    token = response.cookies.get('XSRF-TOKEN')
    headers = {'X-XSRF-TOKEN': token}
    auth = (user, password)
    print 'Comparing actual environment status against expected (', expected, ')...'
    status = requests.get(environment_url + status_list_path, auth=auth, cookies=cookies, headers=headers).json()
    #    status.raise_for_status()

    count = 0
    for item in status:
        if not item['node']['primusCapable']:
            count += 1
            service_num = len(item['services'])
            for service in range(service_num):
                for node in item['services'][service]['serviceInstances']:
                    service_type = node['baseServiceConfig']['capabilities'][0]
                    if service_type in actual.keys():
                        actual[service_type] += 1
                    else:
                        actual[service_type] = 1

    print 'Actual state is (', actual, ')'
    result = actual == expected

    if not result:
        # Raise a ServiceInstanceStatusError exception to allow retry using the retrying package
        raise ServiceInstanceStatusError('Actual state does not equal expected state.')

    return result


def main():
    """
    Main function to check if expected environment state matches actual state.
    """
    parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("hostURL", help="Host URL for the target environment, e.g: http://tscl7901.spotfire-cloud.com")
    parser.add_argument("expectedTSS", type=int, help="Expected number of TSS instances")
    parser.add_argument("expectedWP", type=int, help="Expected number of WP instances")
    parser.add_argument("expectedAS", type=int, help="Expected number of AS instances")
    parser.add_argument("expectedDA", type=int, help="Expected number of DA instances")
    parser.add_argument("expectedST", type=int, help="Expected number of ST instances")
    parser.add_argument("user", help="User")
    parser.add_argument("password", help="Password")
    args = parser.parse_args()

    compare = check_system_status(args.hostURL, args.user, args.password, args.expectedTSS, args.expectedWP,
                                  args.expectedAS, args.expectedDA, args.expectedST)
    if not compare:
        sys.exit(1)


if __name__ == '__main__':
    main()
