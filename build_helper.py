import ssl
import sys
import argparse
import urllib2
import os

import subprocess


def main(argv):
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers()

    parser_download_file = subparsers.add_parser('download-file')
    parser_download_file.add_argument('--to', dest='to_file', help='File to download to', required=True)
    parser_download_file.add_argument('--from', dest='from_url', help='HTTP URL to download from', required=True)
    parser_download_file.set_defaults(func=download_file)

    parser_cat_pipe_process = subparsers.add_parser('cat-pipe-process')
    parser_cat_pipe_process.add_argument('--command', help='The command to run', required=True)
    parser_cat_pipe_process.add_argument('--from', dest='from_file', help='The file to cat into the command', required=True)
    parser_cat_pipe_process.set_defaults(func=cat_pipe_process)

    args = parser.parse_args(argv)

    args.func(args)


def download_file(args):
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE
    f = urllib2.urlopen(args.from_url, context=ctx)
    contents = f.read()
    f.close()

    with open(args.to_file, 'wb') as f:
        f.write(contents)

def cat_pipe_process(args):
    with open(args.from_file) as f:
        contents = f.read()

    sys.stderr.write("PATH: " + os.environ['PATH'] + "\n")
    sys.stderr.write("COMMAND: " + args.command + "\n")
    l = args.command.split()
    p = subprocess.Popen(l, stdin=subprocess.PIPE)
    p.communicate(contents)
    exit(p.wait())


if __name__ == '__main__':
    main(sys.argv[1:])
