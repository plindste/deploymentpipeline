#!groovy
import groovy.transform.Field
@Library('sfAWSLibrary')_

// Globals
String DEFAULT_REGION = 'us-west-2'
String buildUser = 'Internal'

//String prodVersion = '99.0.0.0'
//String prodBuild = 'V06'
String multiAz='No'
String environmentType='Test'
String customerName='TIBCO'
String productCode='TSCE'
String hardwareClass='Reduced'
String vns3InstanceType='m3.medium'
String stackTemplateDefinitionsFile="tsc-config-storage/build-automation/definitions-1608-tsce2.yml"

//RDS DB
String dbClass='db.m4.xlarge'
String DbAdminUsername='tscoadmin'
String InitNewDb='No'
String newDbInitVersion='0'
String newDbInitBuild='0'
String newdbstorageallocation='50'
String networkVersion="v200"

//Application stack variables
String buildJumpHost='Yes'
String keyPair='testing-us-east-1'


//Job properties definition
properties([
        [$class         : 'RebuildSettings',
         autoRebuild    : false,
         rebuildDisabled: false],
        parameters([string(defaultValue: "", description: 'Environment name. E g: acme', name: 'TARGET_ENVIRONMENT'),
					booleanParam(defaultValue: true, description: 'Set this check box if you want to terminate the environment at the end automatically.', name: 'TERMINATE_ENV_AT_THE_END'),
					string(defaultValue: "us-west-2", description: 'Specify AWS region eg.us-west-2 ', name: 'AWS_REGION'),
                    string(defaultValue: "7.9.0.1", description: 'Specify product version eg. 7.9.0.1', name: 'PRODUCT_VERSION'),
                    string(defaultValue: "V02", description: 'Specify product build, for example V14-develop', name: 'PRODUCT_BUILD'),
                    string(defaultValue: "0,6,1", description: 'Min, Max, Desired number of instances of TSSA. E g: 0,6,1', name: 'TSSA_MINMAXDESIRED'),
                    string(defaultValue: "0,6,1", description: 'Min, Max, Desired number of instances of EDGE. E g: 0,6,1 ', name: 'EDGE_MINMAXDESIRED'),
                    string(defaultValue: "0,6,1", description: 'Min, Max, Desired number of instances of WP. E g: 0,6,1  ', name: 'WP_MINMAXDESIRED'),
                    string(defaultValue: "0,6,1", description: 'Min, Max, Desired number of instances of AS. E g: 0,6,1 ', name: 'AS_MINMAXDESIRED'),
                    string(defaultValue: "0,6,0", description: 'Min, Max, Desired number of instances of AS. E g: 0,6,1', name: 'ST_MINMAXDESIRED'),
					//string(defaultValue: "develop", description: 'Branch for deploymentScripts repo', name: 'DEPLOYMENTSCRIPTS_REPO'),
                    string(defaultValue: "tscedevelop", description: 'debug or default?', name: 'DEBUG_MODE')
        ])
		])







node('master') {


try {



		//Stage input validation:
		stage("Input validation"){
			validateTargetEnvironment env.TARGET_ENVIRONMENT
			validateAutoScalingParameters env.TSSA_MINMAXDESIRED
			validateAutoScalingParameters env.EDGE_MINMAXDESIRED
			validateAutoScalingParameters env.WP_MINMAXDESIRED
			validateAutoScalingParameters env.AS_MINMAXDESIRED
			validateAutoScalingParameters env.ST_MINMAXDESIRED
		}
   		//Stage Checkout
		stage("Checkout"){
						checkout()
		}


			stage('Create environment stack') {

			// First, create environment stack

            createEnvironmentStack (buildUser,
							"${env.TARGET_ENVIRONMENT}".toString(),
							"${env.AWS_REGION}".toString(),
                            multiAz,
                           productCode,
                           customerName,
                           environmentType,
                           hardwareClass,
                           vns3InstanceType,
						   stackTemplateDefinitionsFile,
						   networkVersion)
			}

            // TODO: Add updating of jh security rules as separate stage?
//			stage('Update JumpHost security group rules') {
// TODO: Should we encode this into Groovy and convert the below job to a pipeline job instead? Or perhaps refactor it to a library funciton?
// TODO: If so, we're getting to a point where we need to start exploring how to use library functions in Jenkins pipeline and such
//				build 'update-jump-host-security-group-rules'
//			}

// Next, add RDS stack
            stage('Add RDS stack') {
				addOracleRDSStack(buildUser,
                      "${env.TARGET_ENVIRONMENT}".toString(),
                      "${env.AWS_REGION}".toString(),
                      multiAz,
					  dbClass,
					  DbAdminUsername,
					  InitNewDb,
					  newDbInitVersion,
					  newDbInitBuild,
					   newdbstorageallocation,
					   stackTemplateDefinitionsFile,
					   productCode)
			}
            // Next, add app stack.
            // This will initiate the TC role that will perform one-time bootstrapping actions,
            // and on success signal to CF to continue


			stage('Add application stack') {
				addApplicationStack(buildUser,
                        "${env.TARGET_ENVIRONMENT}".toString(),
                        "${env.AWS_REGION}".toString(),
                        "${env.TSSA_MINMAXDESIRED}".toString(),
                        "${env.EDGE_MINMAXDESIRED}".toString(),
                        "${env.WP_MINMAXDESIRED}".toString(),
                        "${env.AS_MINMAXDESIRED}".toString(),
                        "${env.ST_MINMAXDESIRED}".toString(),
                        "${env.PRODUCT_VERSION}".toString(),
                        "${env.PRODUCT_BUILD}".toString(),
						"${env.DEBUG_MODE}".toString(),
						stackTemplateDefinitionsFile,
						productCode,
						buildJumpHost,
						keyPair
						)
            }
            stage('Update Jump Host rules') {
				sleep(time: 5, unit: 'MINUTES') // 5 Minutes sleep before updating jump host rules
				//Do not fail job even if update jump host security group rules fails since we can update the rules afterwards.
				build job: 'update-jump-host-security-group-rules', propagate: false
			}

            // Last, monitor for stack becoming ready to serve requests.
            stage('Wait for completion (Service Instance Status check)') {
                String daSize = "0" // We currently do not have any DA instances in TSCE
				String tssaSize = "0" // We currently do not have any DA instances in TSCE
                retry(60) { // Retry for up to an hour if call do not succeed
                    sleep(time: 1, unit: 'MINUTES') // Wait for one minute between consecutive calls

					//Keeping TSSA Size (first parameter) as zero since I am not sure and I think there won't be any service instance on tssa.	Feel free to modify in future.
                    checkServiceInstanceStatus("${env.TARGET_ENVIRONMENT}".toString(),
                            tssaSize,
                            (env.WP_MINMAXDESIRED.tokenize(','))[2],
                            (env.AS_MINMAXDESIRED.tokenize(','))[2],
                            daSize,
                            (env.ST_MINMAXDESIRED.tokenize(','))[2],
							'root',
							'M3166j7!')
                }
            }
            stage('System test (BRC test)') {
             
				
				build job: 'BRC', parameters: [string(name: 'URL', value: "http://"+"${env.TARGET_ENVIRONMENT}".toString()+".spotfire-cloud.com")]
				
				echo "Get test artifacts from artifact store that relates to this product/build"
                echo "If there are no artifacts - skip further execution abut DO NOT fail the build or mark it as unstable!"
                echo "execute start.bat - fail on error"
                echo "Collect JUnit results. If no results exist - fail"
                echo "Report JUnit results"
            }

			stage('Terminate environment'){

			if ("${env.TERMINATE_ENV_AT_THE_END}".toBoolean()){
			echo "Terminating environment"
			build job: 'environment-control', parameters: [string(name: 'SERVICEID', value: "${env.TARGET_ENVIRONMENT}".toString()), string(name: 'ACTION', value: 'terminate'), string(name: 'CONTROLFILE', value: 'TSCE-1608.yml'), text(name: 'PROTECTED', value: '''launch300
tsclrpt
toolnet
rdsconvert

''')]
			echo "Environment terminated successfully."

			}
			else{
			echo "Keeping the environment since value for parameter TERMINATE_ENV_AT_THE_END is set to false."

			}

			}


    }catch (e) {
        // If there was an exception thrown, the build failed
        currentBuild.result = "FAILED"
        throw e
    } finally {
        // Success or failure, always send notifications
//		notifyBuild(currentBuild.result)
    }
}




def checkout()
{
	try {
        wrap([$class: 'TimestamperBuildWrapper']) {
            // TODO: Figure out why this info is not available and make setting user work!
//			buildUser = env.BUILD_USER
            String buildUser = 'Internal'
            // Checkout
                echo "Checking out..."
                checkout scm
                checkout([$class                           : 'GitSCM',
                          branches                         : [[name: '*/master']],
                          browser                          : [$class : 'FisheyeGitRepositoryBrowser',
                                                              repoUrl: 'http://got-crucible.emea.tibco.com:8060/changelog/tsc-factory-automation'],
                          doGenerateSubmoduleConfigurations: false,
                          extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                               relativeTargetDir: 'tsc-factory-automation']],
                          submoduleCfg                     : [],
                          userRemoteConfigs                : [[credentialsId: '7df2ae0a-55b1-4e8b-9efc-2f9f91a87882',
                                                               url          : 'git@gitlab.iss-resources.com:cloudops/tsc-factory-automation.git']]
                ])
                checkout([$class                           : 'GitSCM',
                          branches                         : [[name: '*/master']],
                          browser                          : [$class : 'FisheyeGitRepositoryBrowser',
                                                              repoUrl: 'http://got-crucible.emea.tibco.com:8060/changelog/tsc-config-storage'],
                          doGenerateSubmoduleConfigurations: false,
                          extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                               relativeTargetDir: 'tsc-config-storage']],
                          submoduleCfg                     : [],
                          userRemoteConfigs                : [[credentialsId: '7df2ae0a-55b1-4e8b-9efc-2f9f91a87882',
                                                               url          : 'git@gitlab.iss-resources.com:cloudops/tsc-config-storage.git']]
                ])



	}
	}
	catch (e) {
        // If there was an exception thrown, the build failed
        currentBuild.result = "FAILED"
		println("Issue occured in checkout")
        throw e
    }
	finally {
        println currentBuild.result
        step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: 'nbhatkan@tibco.com', sendToIndividuals: true])
    }
}
