@Library('sfAWSLibrary') _
@Library('jenkinsCommonLibrary') __


def jobDescription = "" +
    "<p>This job deploys an environment as specified with its name " +
    "(e g tscdev) as a specified type (TSC/TSCE) with a specified manifest " +
    "(defined by version and manifest number). </p>" +
    "<p>By specifying the desired deploy time, a maintenance banner is set " +
    "and the job will wait until the specified deploy time is reached " +
    "before kicking off the deploy actions. If one wish to skip the wait, " +
    "simply tick the <b>SKIP_WAIT</b> box. </p>" +
    "<p>Number of instances of each type, MULTI_AZ, region, db size hw class, " +
    "site URL and automation version is also to be specified.<br/> " +
    "Furthermore, it is possible to deploy from scratch " +
    "(NW stack need to be present) by ticking the <b>NUKE</b> box</p>" +
    "<h3>NOTE! Do NOT manipulate the RDS or App stack(s) " +
    "outside of this job!</h3>" +
    "<p>This job takes care of this."

// TODO: Move this string to a shared resource to be shared across all pipelines that want protection
def protectedStacks = ['launch300', 'launch300-rds1', 'tsclrpt', 'tsclrpt-rds1', 'tsclrpt-rds2', 'toolnet', 'rdsconvert']

def actionsFile = ""
int ordinal
int previousOrdinal
boolean createRdsStack = false
boolean deleteAppStack = false
boolean snapshotRds = false
String python = "/opt/rh/python27/root/usr/bin/python"

// Globals
String BuildUser = 'Internal'
String StackTemplateDefinitionsFile = "tsc-config-storage/build-automation/definitions-1608-tsce2.yml"

//RDS DB
String DbClass = 'db.m4.xlarge'
String DbAdminUsername = 'tscoadmin'
String PlatformVersion = "rev1608platform"
String RdsVersion = "rev1608rds"
String CreateMainDNSRecord = "Yes"
String InitNewDb = "No"
String NewDbInitVersion = '0'
String NewDbInitBuild = '0'

//Application stack variables
String BuildJumpHost = 'Yes'
String KeyPair = 'testing-us-east-1'
String CreateTcRole = 'Yes'

pipeline {
  agent none
  parameters {
    string(name: 'TARGET_ENVIRONMENT',
           description: 'Specify target environment, for example tscdev',
           defaultValue: '')
    choice(name: 'PRODUCT_CODE',
           description: 'TSC is Cloud, TSCE is for Enterprise',
           choices: "TSC\nTSCE")
    string(name: 'PRODUCT_VERSION',
           description: 'Specify the product version, for example 7.12.0',
           defaultValue: '')
    string(name: 'PRODUCT_BUILD',
           description: 'Specify the product build, for example M6',
           defaultValue: '')
    booleanParam(name: 'NUKE',
                 description: 'Remove all addon stacks before deploying True/False',
                 defaultValue: false)
    booleanParam(name: 'SKIP_WAIT',
                 description: 'Skip waiting completely',
                 defaultValue: false)
    string(name: 'DEPLOY_TIME',
           description: 'Time to deploy in. NOW or YYYY-MM-DDTHH:mm:ss+00:00 format',
           defaultValue: 'NOW')
    string(name: 'TSSA_MINMAXDESIRED',
           description: 'Min, Max, Desired number of instances of TSSA. E g: 0,6,1',
           defaultValue: "0,6,1")
    string(name: 'EDGE_MINMAXDESIRED',
           description: 'Min, Max, Desired number of instances of EDGE. E g: 0,6,1 ',
           defaultValue: "0,6,1")
    string(name: 'WP_MINMAXDESIRED',
           description: 'Min, Max, Desired number of instances of WP. E g: 0,6,1  ',
           defaultValue: "0,6,1")
    string(name: 'AS_MINMAXDESIRED',
           description: 'Min, Max, Desired number of instances of DA. E g: 0,6,1. NOTE: For TSCE, desired should be set to 0, e g: 0,6,0.',
           defaultValue: "0,6,0")
    string(name: 'ST_MINMAXDESIRED',
           description: 'Min, Max, Desired number of instances of ST. E g: 0,6,1. NOTE: For TSCE, desired should be set to 0, e g: 0,6,0.',
           defaultValue: "0,6,1")
    string(name: 'TSS_MINMAXDESIRED',
           description: 'Min, Max, Desired number of instances of TSS. E g: 0,6,1. NOTE: For TSCE, desired should be set to 0, e g: 0,6,0.',
           defaultValue: "0,6,1")
    string(name: 'DA_MINMAXDESIRED',
           description: 'Min, Max, Desired number of instances of DA. E g: 0,6,1',
           defaultValue: "0,6,1")
    choice(name: 'MULTI_AZ',
           description: 'Deploy to multiple Availiability Zones?',
           choices: "No\nYes")
    string(name: 'AWS_REGION',
           description: 'Specify target AWS Region, example: us-west-2',
           defaultValue: 'us-west-2')
    string(name: 'DB_SIZE',
           description: 'Specify size of DB disk in GB. Reduced: 50, Full: 100',
           defaultValue: "50")
    choice(name: 'HARDWARE_CLASS',
           description: 'Select appropriate hardware class (Reduced/Full)',
           choices: "Reduced\nFull")
    string(name: 'SITE_URL',
           description: 'Site URL',
           defaultValue: "@SERVICE ID@.spotfire-cloud.com")
    string(name: 'AUTOMATION_VERSION',
           description: 'debug or default?',
           defaultValue: "default")
  }
  options {
    skipDefaultCheckout()
    timestamps()
  }
  stages {
    stage('Check protection') {
      agent {
        label 'master'
      }
      steps {
        setJobDescription(env.JOB_NAME, jobDescription)
        setRunDescription(currentBuild,
                          "${PRODUCT_CODE}: ${PRODUCT_VERSION} " +
                              "${PRODUCT_BUILD} (${TARGET_ENVIRONMENT})")
        script {
          if (protectedStacks.contains(TARGET_ENVIRONMENT)) {
            echo "FATAL: ${TARGET_ENVIRONMENT} is protected"
            currentBuild.result = 'ABORTED'
            error('ABORTED')
          }
        }
      }
    }
    stage('Maintenance banner') {
      agent {
        label 'master'
      }
      steps {
        checkout scm
        echo "Setting maintenance banner"
        sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/maintenanceBanner.py\"" +
               " --time=\"${DEPLOY_TIME}\"" +
               " --vpc=\"${TARGET_ENVIRONMENT}\"" +
               " --region=\"${AWS_REGION}\""

      }
    }
    stage('Wait') {
      agent {
        label 'master'
      }
      steps {
        script {
          outFile = env.WORKSPACE + "/out.txt"
        }
        checkout scm
        echo "Calculating wait time"
        sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/timeCalculator.py\"" +
               " --time=\"${DEPLOY_TIME}\"" +
               " --out=\"${outFile}\""

        script {
          String contents = readFile outFile
          echo "Seconds to wait: ${contents}"
          int waitInSeconds = Integer.valueOf(contents)
          if (0 > waitInSeconds) {
            echo "FATAL: Cannot wait a negative amount of time"
            currentBuild.result = 'ABORTED'
            error('ABORTED')
          }
          if (!Boolean.valueOf(SKIP_WAIT)) {
            echo 'Starting wait...'
            sleep waitInSeconds
          }
        }
      }
    }
    stage('Nuke') {
      agent {
        label 'master'
      }
      when {
        expression {
          Boolean.valueOf(NUKE)
        }
      }
      steps {
        checkout scm
        echo "Terminating all add on stacks"
        sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/nukeAddonStacks.py\"" +
               " --vpc=\"${TARGET_ENVIRONMENT}\"" +
               " --region=\"${AWS_REGION}\""
      }
    }

    stage('Initialize') {
      agent {
        label 'master'
      }
      steps {
        script {
          actionsFile = env.WORKSPACE + "/actions.txt"
        }
        checkout scm //TODO: got-shared-builder AWS credentials?

        echo "Deleting any rolled back stacks..."
        sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/deleteRollbackedStacks.py\"" +
               " --vpc=\"${TARGET_ENVIRONMENT}\"" +
               " --region=\"${AWS_REGION}\""

//        withEnv(
//            ['AWS_ACCESS_KEY_ID=AKIAJTQOAU3HULS6SREA', 'AWS_SECRET_ACCESS_KEY=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']) {
        echo "Determining actions to take..."
        sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/deployActions.py\"" +
               " --stack=\"${TARGET_ENVIRONMENT}\"" +
               " --path=\"${actionsFile}\"" +
               " --region=\"${AWS_REGION}\""
//        }

        script {
          String contents = readFile actionsFile
          echo "Actions: ${contents}"
          List<String> lines = contents.readLines()
          ordinal = Integer.valueOf(lines.get(0))
          previousOrdinal = determinePreviousOrdinal(ordinal)

          if (lines.contains("createRds")) {
            createRdsStack = true
          }
          if (lines.contains("deleteApp")) {
            deleteAppStack = true
          }
          if (shouldSnapshot(deleteAppStack, createRdsStack)) {
            snapshotRds = true
          }
        }

        checkout([$class                           : 'GitSCM',
                  branches                         : [[name: '*/master']],
                  doGenerateSubmoduleConfigurations: false,
                  extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                       relativeTargetDir: 'tsc-config-storage']],
                  submoduleCfg                     : [],
                  userRemoteConfigs                : [[credentialsId: 'got-shared-builder',
                                                       url          : 'git@gitlab.iss-resources.com:cloudops/tsc-config-storage.git']]])
        checkout([$class                           : 'GitSCM',
                  branches                         : [[name: '*/master']],
                  doGenerateSubmoduleConfigurations: false,
                  extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                       relativeTargetDir: 'tsc-factory-automation']],
                  submoduleCfg                     : [],
                  userRemoteConfigs                : [[credentialsId: 'got-shared-builder',
                                                       url          : 'git@gitlab.iss-resources.com:cloudops/tsc-factory-automation.git']]])
        checkout([$class                           : 'GitSCM',
                  branches                         : [[name: '*/master']],
                  doGenerateSubmoduleConfigurations: false,
                  extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                       relativeTargetDir: 'tsc-management-automation']],
                  submoduleCfg                     : [],
                  userRemoteConfigs                : [[credentialsId: 'got-shared-builder',
                                                       url          : 'git@gitlab.iss-resources.com:cloudops/tsc-management-automation.git']]])
        stash includes: 'tsc-config-storage/**, tsc-management-automation/**, tsc-factory-automation/**', name: 'aws-gits'
      }
    }
    stage('Snapshot RDS') {
      agent {
        label 'master'
      }
      when {
        expression {
          snapshotRds
        }
      }
      steps {
        checkout scm
        sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/snapshotRds.py\"" +
               " --vpc \"${TARGET_ENVIRONMENT}\"" +
               " --region \"${AWS_REGION}\"" +
               " --manifest \"${PRODUCT_BUILD}\"" +
               " --product-version \"${PRODUCT_VERSION}\""

      }
    }

    stage('Terminate APP stack') {
      agent {
        label 'master'
      }
      when {
        expression {
          deleteAppStack
        }
      }
      steps {
        // TODO: Replace with in-repo python script
        echo "Terminating APP stack..."
        sh "aws cloudformation delete-stack --stack-name ${TARGET_ENVIRONMENT}-app${previousOrdinal} --region ${AWS_REGION}"
      }
    }

    stage('Create RDS stack') {
      agent {
        label 'master'
      }
      when {
        expression {
          createRdsStack
        }
      }
      steps {
        echo "Creating RDS stack..."
        unstash 'aws-gits'

        // TODO: Replace with in-repo python script
        addOracleRDSStack(BuildUser,
                          TARGET_ENVIRONMENT,
                          AWS_REGION,
                          MULTI_AZ,
                          DbClass,
                          DbAdminUsername,
                          InitNewDb,
                          NewDbInitVersion,
                          NewDbInitBuild,
                          DB_SIZE,
                          StackTemplateDefinitionsFile,
                          PRODUCT_CODE,
                          RdsVersion,
                          CreateMainDNSRecord,
                          ordinal.toString())
      }
    }

    stage('Recreate APP stack') {
      agent {
        label 'master'
      }
      steps {
        // TODO: Replace with in-repo python script
        echo "Recreating APP stack"
        unstash 'aws-gits'
        addApplicationStack(BuildUser,
                            TARGET_ENVIRONMENT,
                            AWS_REGION,
                            TSSA_MINMAXDESIRED,
                            EDGE_MINMAXDESIRED,
                            WP_MINMAXDESIRED,
                            AS_MINMAXDESIRED,
                            ST_MINMAXDESIRED,
                            TSS_MINMAXDESIRED,
                            DA_MINMAXDESIRED,
                            PRODUCT_VERSION,
                            PRODUCT_BUILD,
                            StackTemplateDefinitionsFile,
                            PRODUCT_CODE,
                            BuildJumpHost,
                            KeyPair,
                            PlatformVersion,
                            ordinal.toString(),
                            HARDWARE_CLASS,
                            AUTOMATION_VERSION,
                            '', //Source bukkit
                            SITE_URL,
                            CreateTcRole)
      }
    }
    stage('Clear maintenance banner') {
      agent {
        label 'master'
      }
      steps {
        checkout scm
        echo "Clearing maintenance banner"
        sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/maintenanceBanner.py\"" +
               " --clear" +
               " --vpc=\"${TARGET_ENVIRONMENT}\"" +
               " --region=\"${AWS_REGION}\""

      }
    }
    stage('Finishing') {
      failFast false
      parallel {
        stage('Check status') {
          agent {
            label 'master'
          }
          steps {
            echo "Trying to verify environment status for max 60 minutes"
            script {
              def cacheFile = env.WORKSPACE + "/oauth.txt"
              echo "cacheFile existed: " + new File(cacheFile).delete()
              sleep(time: 5, unit: 'MINUTES')
              retry(55) {
                sleep(time: 1, unit: 'MINUTES')
                dir(env.WORKSPACE) {
                  sh python + " -m Scripts.Python.AWS.verify_nodecount" +
                      " --vpc=\"${TARGET_ENVIRONMENT}\"" +
                      " --region=\"${AWS_REGION}\"" +
                      " --cache-file=\"${cacheFile}\"" +
                      " --sut-url=\"https://${SITE_URL.replace("@SERVICE ID@", TARGET_ENVIRONMENT)}\"" +
                      " --TSS=\"${TSS_MINMAXDESIRED}\"" +
                      " --TSSA=\"${TSSA_MINMAXDESIRED}\"" +
                      " --WP=\"${WP_MINMAXDESIRED}\"" +
                      " --DA=\"${DA_MINMAXDESIRED}\"" +
                      " --ST=\"${ST_MINMAXDESIRED}\"" +
                      " --AS=\"${AS_MINMAXDESIRED}\"" +
                      " --EDGE=\"${EDGE_MINMAXDESIRED}\""
                }
              }
            }
          }
          post {
            aborted {
              // Aborted in this case means timeout, and we want to ensure failure if the status check timed out
              script {
                currentBuild.result = 'FAILED'
              }
            }
          }
        }
        stage('Update JH rules') {
          agent {
            label 'master'
          }
          steps {
            echo "Update Jump Host rules"
            unstash 'aws-gits'
            sleep(time: 5, unit: 'MINUTES')
            script {
              try {
                sh "python -u ${env.WORKSPACE}/tsc-management-automation/aws/sg-whitelist/maintainwhitelist.py " +
                       "${env.WORKSPACE} tsc-config-storage/mgmt-automation/whitelists/jumphosts.yml"
              } catch (ignored) {
                currentBuild.result = 'UNSTABLE'
              }
            }
          }
        }
      }
    }
    /** BRC And smoke tests Commenting the code we can uncomment when we want it.

     stage('Run BRC Test') {agent {label 'master'}steps {// TODO: Replace with in-repo python script
     echo "Trigger BRC tests"

     try {if (TARGET_ENVIRONMENT.equals('tsctest')) {testSignupUrl='https://qa-account.sandbox.cloud.tibco.com/signup/spotfire'}else if(TARGET_ENVIRONMENT.equals('tscdev')){testSignupUrl='https://tropos-account.sandbox.cloud.tibco.com/signup/spotfire'}else if(TARGET_ENVIRONMENT.equals('tscperf')){testSignupUrl='https://tciperfqa-account.sandbox.cloud.tibco.com/signup/spotfire'}build job: 'BRC', parameters: [string(
     name: 'CloudSite_URL',
     value: testSignupUrl)]} catch (ignored) {currentBuild.result = 'UNSTABLE'}}}stage('Run SMOKE Test') {agent {label 'master'}steps {// TODO: Replace with in-repo python script
     echo "Trigger Smoke tests"

     try {build job: 'SMOKE', parameters: [string(
     name: 'CloudSite_URL',
     value: 'http://'+TARGET_ENVIRONMENT+'.spotfire-cloud.com')]} catch (ignored) {currentBuild.result = 'UNSTABLE'}}}**/

  }
}

private java.lang.Boolean shouldSnapshot(boolean deleteAppStack, boolean createRdsStack) {
  return deleteAppStack && !createRdsStack
}

private static int determinePreviousOrdinal(int ordinal) {
  if (ordinal == 1) {
    return 2
  } else {
    return 1
  }
}
