@Library('jenkinsCommonLibrary') _

def jobDescription = "" +
    "<p>This job set up a maintenance banner on the desired environment. " +
    "Message displayed is in the format: <br/>" +
    "<pre>Spotfire will be down for maintenance starting YYYY-MM-DDTHH:mm:ss+00:00</pre> </p>" +
    "<p>By ticking the checkbox CLEAR, the message is cleared</p>"

String python = "/opt/rh/python27/root/usr/bin/python"

pipeline {
  agent none
  parameters {
    string(name: 'TARGET_ENVIRONMENT',
           description: 'Specify target environment, for example tscdev',
           defaultValue: '')
    string(name: 'AWS_REGION',
           description: 'Specify target AWS Region, example: us-west-2',
           defaultValue: 'us-west-2')
    string(name: 'DEPLOY_TIME',
           description: 'Time to deploy in. NOW or YYYY-MM-DDTHH:mm:ss+00:00 format',
           defaultValue: '')
    booleanParam(name: 'CLEAR',
                 description: 'Clear maintenance banner (True/False)',
                 defaultValue: false)

  }
  options {
    skipDefaultCheckout()
    timestamps()
  }
  stages {
    stage('Maintenance banner') {
      agent {
        label 'master'
      }
      steps {
        setJobDescription(env.JOB_NAME, jobDescription)
        setRunDescription(currentBuild, "${TARGET_ENVIRONMENT} - ${DEPLOY_TIME}")
        checkout scm
        echo "Setting maintenance banner"
        script {
          if (Boolean.valueOf(CLEAR)) {
            echo "Clearing maintenance banner"
            sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/maintenanceBanner.py\"" +
                   " --clear" +
                   " --vpc=\"${TARGET_ENVIRONMENT}\"" +
                   " --region=\"${AWS_REGION}\""

          } else {
            sh python + " \"${env.WORKSPACE}/Scripts/Python/AWS/maintenanceBanner.py\"" +
                   " --time=\"${DEPLOY_TIME}\"" +
                   " --vpc=\"${TARGET_ENVIRONMENT}\"" +
                   " --region=\"${AWS_REGION}\""
          }
        }
      }
    }
  }
}
