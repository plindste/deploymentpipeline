import groovy.transform.Field

@Library('jenkinsCommonLibrary') _
//@Library('sfAWSLibrary') _

@Field def runCreateManifest = false;
@Field def runDeployToDev = false;
@Field def runPromoteToTest = false;
@Field def runPromoteToStaging = false;
@Field def runPromoteToProduction = false;
@Field String aborter = "";

@Field String approverOfDevVerification = null
@Field String approverOfDevPromotion = null
@Field String approverOfTestPromotion = null
@Field String prodPromotionInput = null
@Field String targetDevEnv = "tscdev"
@Field String targetTestEnv = "tsctest"
@Field String targetStagingEnv = "FooBar3"
@Field String targetProdEnv = "FooBar4"


@Field final String promoteSlackChannel = "#sf_deliverypipeline"
@Field final String infoSlackChannel = "#tib-subscriber-cloud,#spot-trop-integration,#internal-spot-trop"

@Field final String devSignupUrl = "https://tropos-account.sandbox.cloud.tibco.com/signup/spotfire"
@Field final String testSignupUrl = "https://qa-account.sandbox.cloud.tibco.com/signup/spotfire"

def startStageChoices = ['Create Manifest', 'Deploy to Dev', 'Promote to Test', 'Promote to Stage',
                         'Promote to Production'].join('\n')

pipeline {
  agent none
  parameters {
    booleanParam(name: 'DRY_RUN',
                 description: 'Perform dry-run? (No deployments will be performed)',
                 defaultValue: false)
    booleanParam(name: 'ENABLE_PROMOTE_TO_PRODUCTION',
                 description: 'Enable promotion to production.',
                 defaultValue: false)
    choice(
        name: 'START_STAGE',
        description: '',
        choices: startStageChoices)
    string(
        name: 'PRODUCT_VERSION',
        description: 'Specify product version, for example 7.12.0.0',
        defaultValue: '')
    string(
        name: 'MANIFEST',
        description: 'Specify manifest, for example M123',
        defaultValue: '')
  }
  options {
    skipDefaultCheckout()
    timestamps()
  }
  stages {
    stage('Init') {
      agent any
      steps {
        script {
          currentBuild.description = "${PRODUCT_VERSION} ${MANIFEST}"
        }
        decideStagesToRun(START_STAGE)
      }
    }
    stage('Create manifest, stage to S3 and trigger deploy of tscdev') {
      agent any
      when {
        expression { runCreateManifest }
      }
      steps {
        // Create manifest for latest successful TSS build
        // Should we call these manifests DXY?
        /*
        This is the part of the pipe that would integrate with end of CI pipeline...
         */
        echo "Manifest generation is currently disabled"
//        createManifest()
      }
      post {
        success {
//          notifyGeneralWithMessage(
//              "DISABLED",
//              promoteSlackChannel,
//              "*Manifest generation is currently disabled.*\n")
          echo "Manifest generation is currently disabled"
        }
      }
    }
    stage('Deploy DEV') {
      agent any
      when {
        expression { runDeployToDev }
      }
      steps {
        echo "Deploying ${PRODUCT_VERSION}, ${MANIFEST} to ${targetDevEnv}..."
        notifyGeneralWithMessage(
            'STARTED',
            promoteSlackChannel + "," + infoSlackChannel,
            "*Environment DEV (${targetDevEnv}) is currently being redeployed with ${PRODUCT_VERSION}, ${MANIFEST}.*\n")
        script {
          if (!Boolean.valueOf(DRY_RUN)) {
            def devDeployJob = build job: 'deploy-upgrade',
                                     parameters: [
                                         string(name: 'TARGET_ENVIRONMENT', value: targetDevEnv),
                                         string(name: 'PRODUCT_VERSION', value: PRODUCT_VERSION),
                                         string(name: 'PRODUCT_BUILD', value: MANIFEST),
                                         string(name: 'NUKE', value: 'false'),
                                         string(name: 'SKIP_WAIT', value: 'false'),
                                         string(name: 'DEPLOY_TIME', value: 'NOW'),
                                         string(name: 'TSSA_MINMAXDESIRED', value: '0,6,1'),
                                         string(name: 'EDGE_MINMAXDESIRED', value: '0,6,1'),
                                         string(name: 'WP_MINMAXDESIRED', value: '0,6,1'),
                                         string(name: 'AS_MINMAXDESIRED', value: '0,6,0'),
                                         string(name: 'ST_MINMAXDESIRED', value: '0,6,1'),
                                         string(name: 'TSS_MINMAXDESIRED', value: '0,6,1'),
                                         string(name: 'DA_MINMAXDESIRED', value: '0,6,1'),
                                         string(name: 'MULTI_AZ', value: 'No'),
                                         string(name: 'AWS_REGION', value: 'us-west-2'),
                                         string(name: 'DB_SIZE', value: '50'),
                                         string(name: 'SITE_URL', value: '@SERVICE ID@.spotfire-cloud.com')]
            if (!devDeployJob.result.equals("SUCCESS")) {
              echo "Job deploy-upgrade experienced issues - result is: " + devDeployJob.result
              currentBuild.result = devDeployJob.result
            }
          }
        }
      }
      post {
        failure {
          notifyGeneralWithMessage(
              'FAILED',
              promoteSlackChannel + "," + infoSlackChannel,
              "*Environment DEV (${targetDevEnv}) failed to deploy with ${PRODUCT_VERSION}, ${MANIFEST}.*\n" +
                  "See ${env.BUILD_URL} for more detail.")
        }
      }
    }
    stage('Verify DEV?') {
      agent none
      when {
        expression { runDeployToDev }
      }
      steps {

//                build job: 'BRC', parameters: [string(
//                        name: 'CloudSite_URL',
//                        value: devSignupUrl)]
        notifyGeneralWithMessage(
            'SUCCESS',
            promoteSlackChannel,
            "*Environment DEV (${targetDevEnv}) is redeployed with ${PRODUCT_VERSION}, ${MANIFEST}.*\n" +
                "@here: Please check when environment is up and decide on whether to promote for verification at:\n " +
                "${env.BUILD_URL}input")

        //Input something
        echo "Waiting for input on whether to verify DEV for 60 minutes..."
        script {
          try {
            timeout(time: 60, unit: 'MINUTES') {
              approverOfDevVerification = input id: 'PromoteVerificationProceed',
                                                message: 'Start verification of DEV for promotion to TEST?',
                                                submitterParameter: 'demoPromoter'
            }
          } catch (err) {
            aborter = err.getCauses()[0].getUser().toString()
            echo "Aborted by ${aborter}"
            currentBuild.result = 'ABORTED'
          }
          echo "Submitter: ${approverOfDevVerification}"
        }
      }
      post {
        aborted {
          abort(aborter)
        }
      }
    }
    stage('Promote to TEST?') {
      agent none
      when {
        expression { runPromoteToTest }
      }
      steps {
        notifyPromote("DEV", approverOfDevVerification)
        echo "Promotion testing in progress..."

        script {
          try {
            approverOfDevPromotion = input id: 'verificationPassed',
                                           message: "Proceed and deploy to TEST (${targetTestEnv})?",
                                           submitterParameter: 'devApprover'
          } catch (err) {
            aborter = err.getCauses()[0].getUser().toString()
            echo "Aborted by ${aborter}"
            currentBuild.result = 'ABORTED'
          }
        }
      }
      post {
        aborted {
          abort(aborter)
        }
      }
    }
    stage('Deploy TEST') {
      agent any
      when {
        expression { runPromoteToTest }
      }
      steps {
        echo "Deploy TEST"
        notifyGeneralWithMessage(
            'STARTED',
            infoSlackChannel,
            "*Environment TEST (${targetTestEnv}) is currently being redeployed with ${PRODUCT_VERSION}, ${MANIFEST}.*\n")
        notifyGeneralWithMessage(
            'STARTED',
            promoteSlackChannel,
            "*Environment TEST (${targetTestEnv}) is currently being redeployed with ${PRODUCT_VERSION}, ${MANIFEST}.*\n" +
                "Promoter: ${approverOfDevPromotion}\n" +
                "@here: Please check when environment is up and notify relevant parties.")
        script {
          if (!Boolean.valueOf(DRY_RUN)) {
            def testDeployJob = build job: 'deploy-upgrade',
                                      parameters: [
                                          string(name: 'TARGET_ENVIRONMENT', value: targetTestEnv),
                                          string(name: 'PRODUCT_VERSION', value: PRODUCT_VERSION),
                                          string(name: 'PRODUCT_BUILD', value: MANIFEST),
                                          string(name: 'NUKE', value: 'false'),
                                          string(name: 'SKIP_WAIT', value: 'false'),
                                          string(name: 'DEPLOY_TIME', value: 'NOW'),
                                          string(name: 'TSSA_MINMAXDESIRED', value: '0,6,1'),
                                          string(name: 'EDGE_MINMAXDESIRED', value: '0,6,1'),
                                          string(name: 'WP_MINMAXDESIRED', value: '0,6,1'),
                                          string(name: 'AS_MINMAXDESIRED', value: '0,6,0'),
                                          string(name: 'ST_MINMAXDESIRED', value: '0,6,1'),
                                          string(name: 'TSS_MINMAXDESIRED', value: '0,6,1'),
                                          string(name: 'DA_MINMAXDESIRED', value: '0,6,1'),
                                          string(name: 'MULTI_AZ', value: 'No'),
                                          string(name: 'AWS_REGION', value: 'us-west-2'),
                                          string(name: 'DB_SIZE', value: '50'),
                                          string(name: 'SITE_URL', value: '@SERVICE ID@.spotfire-cloud.com')]
            if (!testDeployJob.result.equals("SUCCESS")) {
              echo "Job deploy-upgrade experienced issues - result is: " + testDeployJob.result
              currentBuild.result = testDeployJob.result
            }
          }
        }
      }
    }
    stage('BRC TEST') {
      agent none
      when {
        expression { runPromoteToTest }
      }
      steps {
        echo "BRC goes here"

//                build job: 'BRC', parameters: [string(
//                        name: 'CloudSite_URL',
//                        value: testSignupUrl)]
        notifyGeneralWithMessage(
            'SUCCESS',
            promoteSlackChannel + "," + infoSlackChannel,
            "*Environment TEST (${targetTestEnv}) is available with ${PRODUCT_VERSION}, ${MANIFEST}.*\n")

      }
    }
//    stage('Promote to STAGING?') {
//      agent none
//      when {
//        expression { runPromoteToStaging }
//      }
//      steps {
////        notifyPromote("TEST", approverOfDevPromotion)
//        echo "Promotion to staging in progress..."
//
//        script {
//          try {
//            approverOfTestPromotion = input id: 'verificationPassed',
//                                            message: "Proceed and deploy to STAGING (${targetStagingEnv})?",
//                                            submitterParameter: 'stagingApprover'
//          } catch (err) {
//            aborter = err.getCauses()[0].getUser().toString()
//            echo "Aborted by ${aborter}"
//            currentBuild.result = 'ABORTED'
//          }
//        }
//      }
//      post {
//        aborted {
//          abort(aborter)
//        }
//      }
//    }
//
//    stage('Deploy STAGING') {
//      agent any
//      steps {
//        echo "Deploy staging"
//        notifyGeneralWithMessage(
//            'STARTED',
//            promoteSlackChannel,
//            "*Environment STAGING (${targetStagingEnv}) is currently being redeployed.*\n" +
//                "Promoter: ${approverOfTestPromotion}\n" +
//                "@here: Please check when environment is up and notify relevant parties.")
////        tearDownTSC(targetStagingEnv)
////        deployTSC(targetStagingEnv, PRODUCT_VERSION, 'M666')
//      }
//    }

    stage('Promote to PRODUCTION?') {
      agent none
      when {
        allOf {
          expression { Boolean.valueOf(ENABLE_PROMOTE_TO_PRODUCTION) }
          expression { runPromoteToProduction }
        }
      }
      steps {
        notifyPromote("STAGING", approverOfDevPromotion)
        echo "Promotion to production in progress..."

        script {
          try {
            prodPromotionInput = input message: 'Proceed and deploy to PRODUCTION',
                                       parameters: [
                                           booleanParam(
                                               defaultValue: false,
                                               description: 'Proceed to deploy to production?',
                                               name: 'deployProduction'),
                                           string(
                                               defaultValue: 'NOW',
                                               description: 'Time when deployment should start. Format: NOW or YYYY-MM-DDTHH:mm:ss+00:00 ',
                                               name: 'deployTime')],
                                       submitter: 'postman',
                                       submitterParameter: 'prodApprover'
          } catch (err) {
            aborter = err.getCauses()[0].getUser().toString()
            echo "Aborted by ${aborter}"
            currentBuild.result = 'ABORTED'
          }
        }
      }
      post {
        aborted {
          abort(aborter)
        }
      }
    }
    stage('Deploy PRODUCTION') {
      agent any
      when {
        allOf {
          expression { Boolean.valueOf(ENABLE_PROMOTE_TO_PRODUCTION) }
          expression { prodPromotionInput['deployProduction'] }
        }
      }
      steps {
        echo "Deploying to PRODUCTION set to ${prodPromotionInput['deployProduction']}.\n" +
                 "Approver: ${prodPromotionInput['prodApprover']}\n " +
                 "Deploy time: ${prodPromotionInput['deployTime']}"
        notifyGeneralWithMessage(
            'STARTED',
            promoteSlackChannel,
            "*PRODUCTION Environment (${targetProdEnv}) is currently being redeployed with ${PRODUCT_VERSION}, ${MANIFEST}.*\n" +
                "Promoter: ${prodPromotionInput['prodApprover']}\n" +
                "Deploy time: ${prodPromotionInput['deployTime']}")
//        build job: 'deploy-environment',
//              parameters: [
//                  string(name: 'TARGET_ENVIRONMENT', value: targetProdEnv),
//                  string(name: 'PRODUCT_VERSION', value: PRODUCT_VERSION),
//                  string(name: 'PRODUCT_BUILD', value: MANIFEST),
//                  string(name: 'NUKE', value: false'),
//                  string(name: 'SKIP_WAIT', value: 'false'),
//                  string(name: 'DEPLOY_TIME', value: prodPromotionInput['deployTime']),
//                  string(name: 'TSSA_MINMAXDESIRED', value: '0,6,1'),
//                  string(name: 'EDGE_MINMAXDESIRED', value: '0,6,1'),
//                  string(name: 'WP_MINMAXDESIRED', value: '0,6,1'),
//                  string(name: 'AS_MINMAXDESIRED', value: '0,6,0'),
//                  string(name: 'ST_MINMAXDESIRED', value: '0,6,1'),
//                  string(name: 'TSS_MINMAXDESIRED', value: '0,6,1'),
//                  string(name: 'DA_MINMAXDESIRED', value: '0,6,1'),
//                  string(name: 'MULTI_AZ', value: 'No'),
//                  string(name: 'AWS_REGION', value: 'us-west-2'),
//                  string(name: 'DB_SIZE', value: '50'),
//                  string(name: 'SITE_URL', value: '@SERVICE ID@.spotfire-cloud.com')]
      }
    }
    stage('PRODUCTION TEST') {
      agent none
      when {
        allOf {
          expression { Boolean.valueOf(ENABLE_PROMOTE_TO_PRODUCTION) }
          expression { prodPromotionInput['deployProduction'] }
        }
      }
      steps {
        echo "Production automatic checks goes here"

//                build job: 'BRC', parameters: [string(
//                        name: 'CloudSite_URL',
//                        value: testSignupUrl)]
        notifyGeneralWithMessage(
            'SUCCESS',
            promoteSlackChannel,
            "*Environment PRODUCTION (${targetProdEnv}) is available with ${PRODUCT_VERSION}, ${MANIFEST}.*\n")

      }
    }
  }
}

private void decideStagesToRun(String stage) {
  switch (stage) {
    case 'Create Manifest':
      runCreateManifest = true;
      runDeployToDev = true;
      runPromoteToTest = true;
      runPromoteToStaging = true;
      runPromoteToProduction = true;
      break;
    case 'Deploy to Dev':
      runDeployToDev = true;
      runPromoteToTest = true;
      runPromoteToStaging = true;
      runPromoteToProduction = true;
      break;
    case 'Promote to Test':
      runPromoteToTest = true;
      runPromoteToStaging = true;
      runPromoteToProduction = true;
      break;
    case 'Promote to Stage':
      runPromoteToStaging = true;
      runPromoteToProduction = true;
      break;
    case 'Promote to Production':
      runCreateManifest = false;
      runDeployToDev = false;
      runPromoteToTest = false;
      runPromoteToStaging = false;
      runPromoteToProduction = true;
      break;
    default:
      break;
  }
}

private void abort(String aborter) {
  def abortInfo = ""
  if (approverOfDevVerification == null) {
    if (aborter == 'SYSTEM') {
      abortInfo = "*Verification for promotion DEV->TEST was aborted due to timeout.*"
    } else {
      abortInfo = "*Verification for promotion DEV->TEST was aborted.*\n" +
          "Aborted by: ${aborter}"
    }
  } else if (approverOfDevPromotion == null) {
    abortInfo = "*Promotion of DEV -> TEST was aborted.*\n" +
        "Aborted by: ${aborter}"
  } else if (approverOfTestPromotion == null) {
    abortInfo = "*Promotion of TEST -> STAGING was aborted.*\n" +
        "Aborted by: ${aborter}"
  }
  echo abortInfo
  notifyGeneralWithMessage(
      'ABORTED',
      promoteSlackChannel,
      abortInfo)

  currentBuild.result = 'ABORTED'
  error('ABORTED')
}

private void notifyPromote(String environment, String promoter) {
  def map = [DEV: "TEST", TEST: "STAGING", STAGING: "PRODUCTION"]
  notifyGeneralWithMessage('SUCCESS', promoteSlackChannel,
                           "*Environment ${environment} is ready for promotion to ${map.get(environment)}.*\n" +
                               "Promoter: ${promoter}\n" +
                               "Please provide promotion input to ${map.get(environment)} environment at:\n " +
                               "${env.BUILD_URL}input")
}


private void createManifest() {
  build job: 'CreateManifestAndDeployEnvironment/refactoring',
        parameters: [string(name: 'STACK_NAME', value: ''),
                     booleanParam(name: 'STAGE_TO_S3', value: true),
                     booleanParam(name: 'DEPLOY_ENVIRONMENT', value: false),
                     booleanParam(name: 'DEPLOY_NETWORK_STACK', value: false),
                     booleanParam(name: 'DEPLOY_RDS_STACK', value: false),
                     booleanParam(name: 'DEPLOY_APP_STACK', value: false),
                     booleanParam(name: 'CHECK_SERVICE_INSTANCE_STATUS', value: false),
                     booleanParam(name: 'EXECUTE_BRC', value: false),
                     booleanParam(name: 'TERMINATE_ENV_AT_THE_END', value: false),
                     string(name: 'MANIFEST_V_NUMBER', value: 'D99'),
                     string(name: 'RELEASE_VERSION', value: ''),
                     string(name: 'TSS_VERSION', value: ''),
                     string(name: 'TSS_BUILD_NO', value: '^V'),
                     string(name: 'TD_VERSION', value: '1.3.0'),
                     string(name: 'TD_BUILD_NO', value: '^V'),
                     string(name: 'CLOUDPORTAL_VERSION', value: ''),
                     string(name: 'CLOUDPORTAL_BUILD_NO', value: '^V'),
                     string(name: 'TS_VERSION', value: ''),
                     string(name: 'TS_BUILD_NO', value: '^V'),
                     string(name: 'DS_VERSION', value: ''),
                     string(name: 'DS_BUILD_NO', value: '^V'),
                     string(name: 'DEPLOYMENT_SCRIPTS_BRANCH', value: ''),
                     string(name: 'TSCE_MANIFEST_FILE_NAME', value: 'TSCE-manifest.yml'),
                     string(name: 'TSCE_PRODUCTS_YML', value: 'tsce_products.yml'),
                     string(name: 'TSCL_MANIFEST_FILE_NAME', value: 'TSCL-manifest.yml'),
                     string(name: 'ONPREM_MANIFEST_FILE_NAME', value: 'ONPREM-manifest.yml'),
                     string(name: 'SAWS_MANIFEST_FILE_NAME', value: 'SAWS-manifest.yml'),
                     string(name: 'TSC_MANIFEST_FILE_NAME', value: 'TSC-manifest.yml'),
                     string(name: 'TSCL_PRODUCTS_YML', value: 'tscl_products.yml'),
                     string(name: 'SAWS_PRODUCTS_YML', value: 'saws_products.yml'),
                     string(name: 'ONPREM_PRODUCTS_YML', value: 'onprem_products.yml'),
                     string(name: 'TSC_PRODUCTS_YML', value: 'tsc_products.yml')]
}


private void tearDownTSC(environment) {
  // Terminate app-stack
  echo "Terminating app stack ${environment}-app1..."
  // Terminate RDS-stack
  echo "Terminating rds stack ${environment}-rds1"
}

private void deployTSC(environment, version, build) {
  echo "Deploying environment: ${environment}, version: ${version}, build: ${build}"
  /*
  Deploying TSC currently involve, in order:
  1. delete existing app stack
  2. delete existing rds stack
  3. create rds stack
  4. create app stack

  Possible speed improvements for new install:
  Assume there is an existing app1 and rds1 as well as an empty rds2 without main DNS record;
  1. delete existing app1
  2. delete existing rds1
  3. update rds2 to have main DNS record
  4. create app2
  5. create rds1

  Next time - switch ordinals

  New install is likely valid for future TSCE deploys?

  After release of 7.11 - all TSC deploys would be upgrade
   */
}


