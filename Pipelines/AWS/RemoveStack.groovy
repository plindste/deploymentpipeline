def regionChoices = ['us-east-1', 'us-west-2', 'us-west-1', 'us-east-2', 'eu-west-1', 'sa-east-1'].join('\n')
def protectedStacks = ['launch300', 'launch300-rds1', 'tsclrpt', 'tsclrpt-rds1', 'tsclrpt-rds2', 'toolnet', 'rdsconvert']

properties([buildDiscarder(logRotator(numToKeepStr: '30'))])

pipeline {
  agent {
    label 'master'
  }
  parameters {
    string(
        name: 'STACK_NAME',
        defaultValue: '',
        description: 'Name of the stack'
    )
    choice(
        name: 'AWS_REGION',
        choices: regionChoices,
        description: 'AWS region where the target stack resides'
    )
  }
  options {
    skipDefaultCheckout()
    timestamps()
  }
  stages {
    stage('Check protection') {
      steps {
        script {
          if (protectedStacks.contains(STACK_NAME)) {
            echo "FATAL: ${STACK_NAME} is protected"
            currentBuild.result = 'ABORTED'
            error('ABORTED')
          }
        }
      }
    }
    stage('Remove stack') {
      steps {
        sh 'aws cloudformation delete-stack --stack-name ${STACK_NAME} --region ${AWS_REGION}'
      }
    }
  }
}