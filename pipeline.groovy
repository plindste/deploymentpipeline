@Library('jenkinsCommonLibrary') _

pipeline {
  options {
    skipDefaultCheckout()
    timestamps()
  }
  agent none
  stages {
    stage('Run gradle') {
      agent any
      steps {
        checkout scm
        script {
          if (isUnix()) {
            sh './gradlew --continue --no-daemon'
          } else {
            bat 'gradlew.bat --continue --no-daemon'
          }
        }
      }
      post {
        always {
          archiveArtifacts 'build/Scripts/** , build/Pipelines/**, build/test_report.log'
        }
      }
    }
  }
  post {
    failure {
      notifyBuildWithMessage('FAILED', '#sf_infra', 'Done',  '')
    }
  }
}
