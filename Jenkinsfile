#!groovy​

// Globals
def userInput
def propertiesFileName = 'pipeline.properties'
def props
//def TARGET_ENVIRONMENT = "tscldev"
def AWS_REGION = 'us-east-1'
def tsclDefaults = [tscl_vpc:'', tscl_version:'7.9.0.0', tscl_vNumber:'V01', tscl_scriptBranch:'master']
//def vNumber = 'V01'
def productCode = 'TSCL30'

// Input stage to allow user to provide initial input

stage 'Input'
	try {
		timeout(time: 30, unit: 'SECONDS') { // Wait for manual input for 5 seconds before proceeding
			userInput = input(
				id: 'userInput', message: 'Provide input:', parameters: [
//				[$class: 'TextParameterDefinition', defaultValue: '', description: 'E g tscl9200', name: 'TARGET_ENVIRONMENT'], 
//				[$class: 'TextParameterDefinition', defaultValue: '', description: 'E g 7.6.0', name: 'PRODUCT_VERSION'], 
				[$class: 'TextParameterDefinition', defaultValue: 'V01', description: '''Optional, E g V02 or LatestOfficial''', name: 'BUILD_NUMBER'], 
//				[$class: 'TextParameterDefinition', defaultValue: '', description: '''Examples: master, develop, gen-7-7-0-elephant, gen-7-7-0''', name: 'SCRIPT_BRANCH']
			])
			// TODO: Should we allow manual setting of product version and environment here??? Shouldn't that always be governed by either branch or properties file?
			// If we need the ability to deploy any version from a job, the we should probably have a separate job for that?
			// Or perhaps configure a separate "swimlane" for manual input? Probably need to set build name according to what was deployed and where?
		}
	} catch(err) { // timeout reached
		echo err.toString()
		echo "TIMEOUT"
	}


node('master') {
	try {
		wrap([$class: 'TimestamperBuildWrapper']) {
			// TODO: Figure out why this info is not available!
//			buildUser = env.BUILD_USER
			buildUser = "Internal"
			// Checkout
			stage('Checkout') { 
				try {
					checkout scm

					echo "Processing properties..."
					// Figure out a reasonable default VPC name and version from the branch name if we're not running from master:
					// (If we're not running from master, then VPC is generated as: tscl[xyzm], where xyzm is generated from branch name (without dashes))
					// TODO: Should handle if we're running from any non-version branch (e g feature branches)
					if (env.BRANCH_NAME.startsWith("gen")) {
						echo("Running in branch " + env.BRANCH_NAME + " which is a release branch. Setting default VPC name and default product version based on branch name")
						def tsclVPCFromBranch = "tscl" + env.BRANCH_NAME.replaceFirst('-', '_').replaceAll('-', '').split('_')[1]
						def tsclVersionFromBranch = env.BRANCH_NAME.replaceFirst('-', '_').replaceAll('-', '.').split('_')[1]
						tsclDefaults.tscl_vpc = tsclVPCFromBranch
						tsclDefaults.tscl_version = tsclVersionFromBranch
					}
					// If we're not running from a branch with a name beginning with 'gen', then use the branch name as is.
					// this will ensure that master branch result in using the branch name as script branch
					// TODO: Clarify above comment - current wording is confusing...
					tsclDefaults.tscl_scriptBranch = env.BRANCH_NAME 
					tsclDefaults.tscl_vNumber = userInput

					// Check if a properly named properties file exists, if not - bail out (we should require a correctly named properties file as part of the contract for the pipe)
					if (!fileExists(propertiesFileName)) {
						currentBuild.result = 'FAILURE'
						def exceptionMessage = "Cannot find file " + propertiesFileName
						echo exceptionMessage
						throw new RuntimeException(exceptionMessage) // TODO: Cannot use FileNotFoundException since running in sandbox - need to figure out how to avoid running in sandbox
					}

					echo("Default properties: " + tsclDefaults)
					echo("Properties file content:")
					def cmdLine = "cat " + propertiesFileName
					sh "${cmdLine}"
					
					props = readProperties defaults: tsclDefaults, file: propertiesFileName // Read properties file, if any properties are omitted in the file, use the defaults obtained here.
					
					// TODO: Fix so that manual input overrides properties from file or branch name
					
					echo "Actual properties: " + props
					
					checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout'], [$class: 'RelativeTargetDirectory', relativeTargetDir: 'tsc-factory-automation']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/tsc-factory-automation.git']]])
					checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout'], [$class: 'RelativeTargetDirectory', relativeTargetDir: 'tsc-config-storage']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/tsc-config-storage.git']]])

				} catch (ex) {
					currentBuild.result = 'FAILURE'
					echo "Exception: " + ex.toString()
					echo "Message: " + ex.getMessage()
					throw ex
				}
			}

			// First, terminate instances
			stage('Terminate Instances') {
				terminateInstances(props.tscl_vpc, AWS_REGION)
			}
			// Next, stage artifacts and create instances in parallel
			stage('Stage Artifacts/Create Instances') {
				echo "Parallel stuff"
				parallel(
					'Stage deployment scripts artifacts': {
						node('master') {
							stageDeploymentScriptsArtifacts(props.tscl_version, props.tscl_vNumber, productCode, props.tscl_scriptBranch)
						}
					}, 
					'Stage manifest artifacts': {
						node('Gothenburg-Staging') {
							stageManifestArtifacts(props.tscl_version, props.tscl_vNumber)
						}
					}, 
					'Upload Cloud Analyst Client': {
						node('Gothenburg-Staging') {
							uploadCloudAnalystClient(props.tscl_version, props.tscl_vNumber)
						}
					}, 
					'Switch RDS snapshots': {
						node('master') {
							switchRDS(props.tscl_vpc, AWS_REGION)
						}
					},
					'Create Instances': {
						node('master') {
							createInstances(props.tscl_vpc, buildUser)
						}
					},
					failFast: true
				)
			}

			// Deploy
			stage('Deploy') {
				node('master') {
					echo "Deploying..."
					doDeploy(props.tscl_version, props.tscl_vNumber, props.tscl_vpc, AWS_REGION)
				}
			}
		}
	} catch (e) {
		// If there was an exception thrown, the build failed
		currentBuild.result = "FAILED"
		throw e
	} finally {
		// Success or failure, always send notifications
		notifyBuild(currentBuild.result)
	}
}

/*
	Termnate instances in given vpc/region
*/
def terminateInstances(vpc, AWS_REGION) {
	echo ("Terminating instances in target VPC: " + vpc + ", region: " + AWS_REGION)
	withEnv([
		'TGTENV='+ vpc, 
		'AWS_REGION=' + AWS_REGION, 
		'PYTHONPATH=' + env.PYTHONPATH + ":./tsc-factory-automation/factory/infra/3.00:./tsc-factory-automation/sw-automation/vns3:/opt/rh/python27/root/usr/lib/python2.7/site-packages", 
		'PYTHONDEBUG=1',
		'PYTHONUNBUFFERED=1',
		'jenkins_user_password=jenkins'
		]) {	
			sh '''
				set -x
				python -u ./tsc-factory-automation/factory/infra/3.00/terminateVpcInstances.py ${AWS_REGION} ${TGTENV}
			'''
		}
}

def stageDeploymentScriptsArtifacts(version, vNumber, productCode, scriptBranch) {
	echo ("Staging deployment scripts artifacts for version: " + version + ", build: " + vNumber + ", product code: " + productCode + ", script branch: " + scriptBranch)
// TODO: Refactor to not call external job but rather capture all code for this here
/*
	build job: 
		'stage-deployment-artifacts-gen7801', 
		parameters: 
		[
			[$class: 'StringParameterValue', name: 'PRODUCT_VERSION', value: version], 
			[$class: 'StringParameterValue', name: 'BUILD_VERSION', value: vNumber], 
			[$class: 'StringParameterValue', name: 'PRODUCT_CODE', value: productCode]
		]
*/
	checkout([$class: 'GitSCM', branches: [[name: '*/' + scriptBranch]], browser: [$class: 'FisheyeGitRepositoryBrowser', repoUrl: 'http://got-crucible.emea.tibco.com:8060/changelog/DeploymentScripts'], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'cc628ba4-b7ae-4149-a9c4-64e92ce6b599', url: 'git@got-git.emea.tibco.com:DeploymentScripts']]])
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], browser: [$class: 'FisheyeGitRepositoryBrowser', repoUrl: 'http://got-crucible.emea.tibco.com:8060/changelog/tsc-factory-automation'], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'tsc-factory-automation']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '7fbf3cab-8add-41de-9e10-3a09443cd8e4', url: 'git@gitlab.iss-resources.com:cloudops/tsc-factory-automation.git']]])
	withCredentials([usernamePassword(credentialsId: 'dfe3e01c-7a16-4dd7-84eb-1d84009be10b', passwordVariable: 'AWS_SECRET_ACCESS_KEY', usernameVariable: 'AWS_ACCESS_KEY_ID')]) {
		sh '''
			aws s3 sync roles/tss s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/tss/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync roles/tssa s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/tssa/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync roles/cms s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/cms/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync roles/da s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/da/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync roles/nm s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/nm/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync roles/nm s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/wp/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync roles/nm s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/st/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync roles/edge s3://tsco-automation/get-content/machine-roots/${PRODUCT_CODE}/edge/${PRODUCT_VERSION}-${BUILD_VERSION}
			aws s3 sync tsc-factory-automation/helper-scripts/content s3://tsco-automation/get-content	
		'''
	}
}

def stageManifestArtifacts(version, vNumber) {
	echo ("Staging manifest scripts artifacts for version: " + version + ", build: " + vNumber)
// TODO: Refactor to not call external job but rather capture all code for this here
/*
	build job: 
		'spotfire-cloud-manifest-upload', 
		parameters: 
		[
			[$class: 'StringParameterValue', name: 'PRODUCT_VERSION', value: version], 
			[$class: 'StringParameterValue', name: 'BUILD_VERSION', value: vNumber], 
			[$class: 'StringParameterValue', name: 'manifestFile', value: 'using_jenkins_values'], 
			[$class: 'StringParameterValue', name: 's3bucket', value: 's3://tscl/manifest_installers'], 
			[$class: 'StringParameterValue', name: 'swfactory_mount', value: '/mnt/swfactory/']
		]
*/
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], browser: [$class: 'FisheyeGitRepositoryBrowser', repoUrl: 'http://got-crucible.emea.tibco.com:8060/changelog/spotfire-eng-deployment'], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'spotfire-eng-deployment'], [$class: 'CleanBeforeCheckout'], [$class: 'CheckoutOption', timeout: 45], [$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: true, timeout: 45]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/spotfire-eng-deployment.git']]])
	withEnv(['UNC_DOMAIN=emea']) {
		withCredentials([usernamePassword(credentialsId: 'dfe3e01c-7a16-4dd7-84eb-1d84009be10b', passwordVariable: 'AWS_SECRET_ACCESS_KEY', usernameVariable: 'AWS_ACCESS_KEY_ID'), usernamePassword(credentialsId: '8dc621f0-b739-491f-a3bb-97afb35d2974', passwordVariable: 'UNC_PASSWORD', usernameVariable: 'UNC_USER')]) {
			def uncDomain = 'emea'
			def manifestFile = 'using_jenkins_values'
			def s3bucket = 's3://tscl/manifest_installers'
			def swfactoryMount = '/mnt/swfactory/'
			def cmdLine = "python2.7 -u " + env.WORKSPACE + "/spotfire-eng-deployment/stage_s3.py  --manifestFile " + manifestFile + " --manifestVersion " + version + " --manifestBuild " + vNumber + " --s3bucket " + s3bucket + " --unc_domain " + uncDomain + " --unc_user " + env.UNC_USER + " --unc_password " + env.UNC_PASSWORD + " --unc_path_base " + swfactoryMount

			echo("Calling shell script with command line: " + cmdLine)			
			sh "${cmdLine}"
		}
	}			

}

/*
	Uploads the Spotfire Cloud Analyst client to the S3 bucket used by the CDN. 
	Once uploaded it will issue a cloudfront invalidation request to refresh the CDN from the S3 bucket.
*/
def uploadCloudAnalystClient(version, vNumber) {
	echo ("Uploading Cloud Analyst Client for version: " + version + ", build: " + vNumber)
// TODO: Refactor to not call external job but rather capture all code for this here
/*
	build job: 
		'upload-cloud-analyst-client',
		parameters:
		[
			[$class: 'StringParameterValue', name: 'version', value: version], 
			[$class: 'StringParameterValue', name: 'build', value: vNumber], 
			[$class: 'StringParameterValue', name: 'type', value: 'Stage'], 
			[$class: 'StringParameterValue', name: 's3bucket', value: 's3://spotfirecdn.cloud.tibco.com'], 
			[$class: 'StringParameterValue', name: 'swfactory_mount', value: '/mnt/swfactory']
		]
*/
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], browser: [$class: 'FisheyeGitRepositoryBrowser', repoUrl: 'http://got-crucible.emea.tibco.com:8060/changelog/spotfire-eng-deployment'], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'spotfire-eng-deployment'], [$class: 'CleanBeforeCheckout'], [$class: 'CheckoutOption', timeout: 45], [$class: 'CloneOption', depth: 0, noTags: false, reference: '', shallow: true, timeout: 45]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/spotfire-eng-deployment.git']]])
	withEnv(['UNC_DOMAIN=emea']) {
		withCredentials([usernamePassword(credentialsId: 'dfe3e01c-7a16-4dd7-84eb-1d84009be10b', passwordVariable: 'AWS_SECRET_ACCESS_KEY', usernameVariable: 'AWS_ACCESS_KEY_ID'), usernamePassword(credentialsId: '8dc621f0-b739-491f-a3bb-97afb35d2974', passwordVariable: 'UNC_PASSWORD', usernameVariable: 'UNC_USER')]) {
			def uncDomain = 'emea'
			def type = 'Stage'
			def s3bucket = 's3://tscl/manifest_installers'
			def swfactoryMount = '/mnt/swfactory'
			def cmdLine = "python2.7 -u " + env.WORKSPACE + "/spotfire-eng-deployment/upload_analyst_client.py  --deploytype " + type + " --manifestVersion " + version + " --manifestBuild " + vNumber + " --s3bucket " + s3bucket + " --unc_domain " + uncDomain + " --unc_user " + env.UNC_USER + " --unc_password " + env.UNC_PASSWORD + " --unc_path_base " + swfactoryMount

			echo("Calling shell script with command line: " + cmdLine)			
			sh "${cmdLine}"
		}
	}			

}

def switchRDS(vpc, AWS_REGION) {
	echo ("Switching RDS snapshots...")
// TODO: Refactor to not call external job but rather capture all code for this here
/*
	build job: 
		'switch-rds-db-snapshots', 
		parameters: 
			[
				string(name: 'RDS_STACK_NAME', value: vpc + '-rds1'), 
				string(name: 'AWS_REGION', value: AWS_REGION), 
				string(name: 'SNAPSHOT_INITIAL', value: 'prod790-0525-01'), 
				string(name: 'SNAPSHOT_SECONDARY', value: 'prod790-0525-02')
			]
*/
	withEnv(['RDS_STACK_NAME='+ vpc + '-rds1', 'AWS_REGION=' + AWS_REGION, 'SNAPSHOT_INITIAL=prod770-1025-00', 'SNAPSHOT_SECONDARY=prod770-1025-01']) {
//TODO: extract to external shell script:
		sh '''
			echo $PATH
			which aws

			QUERY='Stacks[0].Parameters[?ParameterKey==`Snapshot`].ParameterValue'
			CURRENTSNAPSHOT=`aws --region us-east-1 cloudformation describe-stacks --stack-name ${RDS_STACK_NAME} --query $QUERY --output text`

			if [[ "$CURRENTSNAPSHOT" == "${SNAPSHOT_INITIAL}" ]]; then
				echo Switching to secondary snapshot: ${SNAPSHOT_SECONDARY}
				NEXTSNAPSHOT=${SNAPSHOT_SECONDARY}
			else
				echo Loaded snapshot is not the initial ${INITIAL_SNAPSHOT} - switching to it.
				NEXTSNAPSHOT=${SNAPSHOT_INITIAL}
			fi

			echo Triggering stack update...
			aws --region ${AWS_REGION} cloudformation update-stack --stack-name ${RDS_STACK_NAME} --use-previous-template --parameters ParameterKey=DbAdminPassword,UsePreviousValue=True \
			ParameterKey=MultiAz,UsePreviousValue=True ParameterKey=Ordinal,UsePreviousValue=True ParameterKey=Creator,UsePreviousValue=True ParameterKey=StorageType,UsePreviousValue=True \
			ParameterKey=DbClass,UsePreviousValue=True ParameterKey=AttachmentTargetStack,UsePreviousValue=True ParameterKey=DbStorageAllocation,UsePreviousValue=True  \
			ParameterKey=DbAdminUsername,UsePreviousValue=True ParameterKey=CreateMainDnsRecord,UsePreviousValue=True ParameterKey=IopsRequested,UsePreviousValue=True \
			ParameterKey=Snapshot,ParameterValue=$NEXTSNAPSHOT --capabilities CAPABILITY_IAM --notification-arns arn:aws:sns:us-east-1:820740087625:rds-rebuild-events

			sleep 15

			echo Verifying that stack update has started...
			STACKSTATUS=`aws --region ${AWS_REGION} cloudformation describe-stacks --stack-name ${RDS_STACK_NAME} --query "Stacks[0].StackStatus" --output text`

			if [[ "$STACKSTATUS" != "UPDATE_IN_PROGRESS" ]]; then
				echo "Stack update was not initiated - job failed."
					exit -999
			else
				while [ "$STACKSTATUS" != "UPDATE_COMPLETE" ]
					do
						sleep 60
							STACKSTATUS=`aws --region ${AWS_REGION} cloudformation describe-stacks --stack-name ${RDS_STACK_NAME} --query "Stacks[0].StackStatus" --output text`
							if [[ "$STACKSTATUS" != "UPDATE_COMPLETE" ]]; then
								echo Stack status is now $STACKSTATUS.  Still waiting.
							else
								echo Stack status is now $STACKSTATUS - update is complete.
							fi
					done
			fi
		'''
	}

}

def createInstances(vpc, user) {
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout'], [$class: 'RelativeTargetDirectory', relativeTargetDir: 'tsc-factory-automation']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/tsc-factory-automation.git']]])
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout'], [$class: 'RelativeTargetDirectory', relativeTargetDir: 'tsc-config-storage']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/tsc-config-storage.git']]])
// TODO Refactor to use withEnv instead
/*
	env.TGTENV = vpc
	env.INSTANCELIST_PROD = "TSCL:v770:JUMP:0,TSCL:v770:EDGE:1,TSCL:v770:DB:0,TSCL:v770:TSSA:1,TSCL:v770:TSS:1,TSCL:v770:CMS:1,TSCL:v770:NM:1,TSCL:v770:DA:1"
	env.INSTANCELIST_TEST = "TSCL:v770:JUMP:0,TSCL:v770:EDGE:1,TSCL:v770:DB:0,TSCL:v770:TSSA:1,TSCL:v770:TSS:1,TSCL:v770:CMS:1,TSCL:v770:NM:1,TSCL:v770:DA:1"
*/
	withEnv([
		'TGTENV=' + vpc,
		'PYTHONPATH=' + env.PYTHONPATH + ':./tsc-factory-automation/factory/infra/3.00:./tsc-factory-automation/sw-automation/vns3:/opt/rh/python27/root/usr/lib/python2.7/site-packages',
		'PYTHONDEBUG=1',
		'PYTHONUNBUFFERED=1',
		'BUILD_USER=' + user,
		'INSTANCELIST_PROD=TSCL:v770:JUMP:0,TSCL:v770:EDGE:1,TSCL:v770:DB:0,TSCL:v770:TSSA:1,TSCL:v770:TSS:1,TSCL:v770:CMS:1,TSCL:v770:NM:1,TSCL:v770:DA:1',
		'INSTANCELIST_TEST=TSCL:v770:JUMP:0,TSCL:v770:EDGE:1,TSCL:v770:DB:0,TSCL:v770:TSSA:1,TSCL:v770:TSS:1,TSCL:v770:CMS:1,TSCL:v770:NM:1,TSCL:v770:DA:1'
		]) {
			echo ("Creating instances in target VPC: " + env.TGTENV)
			sh '''
				set -x              
				python -u ./tsc-factory-automation/factory/infra/3.00/createInstance.py . ./tsc-config-storage/build-automation/definitions.yml 'us-east-1' ${TGTENV} ${INSTANCELIST_TEST} testing-us-east-1 "51314" "${BUILD_USER}" --saveInstanceList --verbose                
				echo Copying instance list to job workspace...
				cp /tmp/${BUILD_TAG}.txt .
			'''
		}
}

def doDeploy(version, vNumber, vpc, awsRegion) {
	echo "Deploy"
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout'], [$class: 'RelativeTargetDirectory', relativeTargetDir: 'tsc-factory-automation']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/tsc-factory-automation.git']]])
	checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CleanBeforeCheckout'], [$class: 'RelativeTargetDirectory', relativeTargetDir: 'tsc-config-storage']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '06ab93e4-7072-4938-9cf7-05cac044b2dc', url: 'git@gitlab.iss-resources.com:cloudops/tsc-config-storage.git']]])
// TODO Refactor to use withEnv instead
/*
	env.PRODUCT_VERSION = version
	env.BUILD_VERSION = vNumber
	env.TGTENV = vpc
	env.AWS_REGION = awsRegion
	env.PYTHONPATH = env.PYTHONPATH + ":./tsc-factory-automation/factory/infra/3.00:./tsc-factory-automation/sw-automation/vns3:/opt/rh/python27/root/usr/lib/python2.7/site-packages"
	env.PYTHONDEBUG=1
	env.PYTHONUNBUFFERED=1
*/
	withEnv([
		'PRODUCT_VERSION=' + version,
		'BUILD_VERSION=' + vNumber,
		'TGTENV=' + vpc,
		'AWS_REGION=' + awsRegion,
		'PYTHONPATH=' + env.PYTHONPATH + ':./tsc-factory-automation/factory/infra/3.00:./tsc-factory-automation/sw-automation/vns3:/opt/rh/python27/root/usr/lib/python2.7/site-packages',
		'PYTHONDEBUG=1',
		'PYTHONUNBUFFERED=1',
		'jenkins_user_password=jenkins'
		]) {	
			wrap([$class: 'AnsiColorBuildWrapper', colorMapName: 'xterm']) {
				sh '''
					echo Initiating orchestration...
					set -x
					python -u ./tsc-factory-automation/factory/infra/3.00/SaltOrchestrate.py
				'''
			}
	}
}

def notifyBuild(String buildStatus = 'STARTED', String slackChannel = '#sf_deployinfra') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"
  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#ffd700'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#2fbb0e'
  } else {
    color = 'RED'
    colorCode = '#d62d20'
  }

  // Send notifications
  slackSend (color: colorCode, message: summary, channel: slackChannel)
}
