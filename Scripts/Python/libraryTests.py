import unittest
import library
import os
import shutil

TESTS = "C:/temp/StageTests"

class LibraryTests(unittest.TestCase):
    def testNaturalSort(self):
        self.assertEqual(library.natural_sort(['10', '2', '1']), ['1', '2', '10'])


    def testCopyRolesFile(self):
        if os.path.exists(TESTS):
            shutil.rmtree(TESTS)
        os.makedirs(TESTS + '/V01/ContentDelivery')
        f =  os.open(TESTS + '/V01/ContentDelivery/tsc_roles.yml', os.O_CREAT)
        os.close(f)
        library.copyContentDeliveryFile(('%s' % TESTS), 'V01', 'tsc_roles.yml')
        self.assertTrue(os.path.exists("./tsc_roles.yml"))
        os.remove('./tsc_roles.yml')
