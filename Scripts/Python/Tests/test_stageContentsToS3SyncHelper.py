import unittest
import mock
from Scripts.Python.stageContentsToS3SyncHelper import *
from mock import Mock


class MyClass:
    def read(self):
        return "hello"


class TestStageContentsToS3SyncHelper(unittest.TestCase):
    '''
    This class contains unit tests related to StageContentsToS3SyncHelper class
    '''
    manifest_yaml = yaml.load(open('Scripts/Python/Tests/TSC-manifest.testdata.yml'))
    roles_yml_file = 'Scripts/Python/Tests/tsc_roles.testdata.yml'
    md5file = ""
    unc_path_base = "//uncpathbase"
    s3_bucket_name = "unittests3bucket"
    target_path = "/test_target_path"
    manifest_version = "7.11.0"
    manifest_build = "D01"

    # Tests for function download file
    # def test_download_file(self):
    #     '''
    #     Unit tests for download_file function. +Ve scenario
    #     :return:
    #     '''
    #     with mock.patch('stageContentsToS3SyncHelper.get_web_file_data') as mockGetWebFileData:
    #         mockGetWebFileData.return_value = "testData"
    #         with mock.patch('stageContentsToS3SyncHelper.write_data_to_file') as mockWriteDataToFile:
    #             mockWriteDataToFile.return_value = True
    #             self.assertTrue(download_file("http://test/testdata.xml", "c:/temp"))

    # Test for function get_manifest_file
    def test_get_manifest_file(self):
        '''
        Unit test for get_manifest_file function +ve test scenario
        :return:
        '''
        arguments = Mock(unc_path_base='\\uncpathbase', manifest_products_directory='Manifest_Prod_Dir',
                         manifestVersion='manifest_version', manifestBuild='manifest_build')
        with mock.patch('os.chdir') as mockChdir:
            mockChdir.return_value = "done"
            with mock.patch('os.listdir') as mockListDir:
                mockListDir.return_value = ['manifest.txt']
                manifest_file = get_manifest_file(arguments)
                self.assertEqual(
                    '\\uncpathbase\Manifests\Manifest_Prod_Dir\manifest_version\manifest_build\manifest.txt',
                    manifest_file)

    # Test for function get_web_file_data
    # def test_get_web_file_data_validURL_WithoutHTTP(self):
    #     '''
    #     Unit test for  get_web_file_data -ve test scenario with invalid url without http
    #     :return:
    #     '''
    #     with self.assertRaises(ValueError):
    #         get_web_file_data("INVALID_URL")

    def test_md5_FileNotFoundError(self):
        '''
        Unit test for md5 function with invalid file
        :return:
        '''
        with self.assertRaises(IOError):
            md5('invalidFile')

    # Tests for function upload_products

    # +ve test Successfully sync directory test
    def test_upload_products_directory(self):
        '''
        Unit test for function upload_products_directory +ve test scenario for a directory
        :return:
        '''
        print("Curr working directory:")
        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.download_file') as mock_downloadFile:
            mock_downloadFile.return_value = "done"
            with mock.patch('os.path.getsize') as mock_getsize:
                mock_getsize.return_value = 600
                with mock.patch('os.path.isdir') as mock_isdir:
                    mock_isdir.return_value = True
                    with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.sync_dir_to_s3') as mock_sync_dir_to_s3:
                        mock_sync_dir_to_s3.return_value = "success"
                        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.sync_file_to_s3') as mock_sync_file_to_s3:
                            mock_sync_file_to_s3.return_value = "success"
                            status = upload_products(self.manifest_yaml, self.roles_yml_file, self.md5file,
                                                     self.unc_path_base, self.s3_bucket_name, self.target_path,
                                                     self.manifest_version, self.manifest_build)
                            self.assertEqual("Upload completed successfully", status)

    # +ve test Successfully sync file test
    def test_upload_products_file(self):
        '''
        Unit test for function upload_products_directory +ve test scenario for a file
        :return:
        '''
        print("Curr working directory:")
        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.download_file') as mock_downloadFile:
            mock_downloadFile.return_value = "done"
            with mock.patch('os.path.getsize') as mock_getsize:
                mock_getsize.return_value = 600
                with mock.patch('os.path.isdir') as mock_isdir:
                    mock_isdir.return_value = False
                    with mock.patch(
                            'Scripts.Python.stageContentsToS3SyncHelper.sync_dir_to_s3') as mock_sync_dir_to_s3:
                        mock_sync_dir_to_s3.return_value = "success"
                        with mock.patch(
                                'Scripts.Python.stageContentsToS3SyncHelper.sync_file_to_s3') as mock_sync_file_to_s3:
                            mock_sync_file_to_s3.return_value = "success"
                            status = upload_products(self.manifest_yaml, self.roles_yml_file,
                                                     self.md5file, self.unc_path_base,
                                                     self.s3_bucket_name, self.target_path,
                                                     self.manifest_version, self.manifest_build)

                            self.assertEqual("Upload completed successfully", status)

    # -ve test on upload error
    def test_upload_products_file_fail_upload(self):
        '''
        Unit test for function upload_products_directory -ve test scenario for a error
        :return:
        '''
        print("Curr working directory:")
        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.download_file') as mock_downloadFile:
            mock_downloadFile.return_value = "done"
            with mock.patch('os.path.getsize') as mock_getsize:
                mock_getsize.return_value = 600
                with mock.patch('os.path.isdir') as mock_isdir:
                    mock_isdir.return_value = False
                    with mock.patch(
                            'Scripts.Python.stageContentsToS3SyncHelper.sync_dir_to_s3') as mock_sync_dir_to_s3:
                        mock_sync_dir_to_s3.return_value = "error"
                        with mock.patch(
                                'Scripts.Python.stageContentsToS3SyncHelper.sync_file_to_s3') as mock_sync_file_to_s3:
                            mock_sync_file_to_s3.return_value = "error"

                            self.assertRaises(SystemExit, upload_products, self.manifest_yaml, self.roles_yml_file,
                                              self.md5file, self.unc_path_base,
                                              self.s3_bucket_name, self.target_path,
                                              self.manifest_version, self.manifest_build)

    # -ve test Successfully upload Access denied
    def test_upload_products_file_fail_upload_AccessDenied(self):
        '''
        Unit test for function upload_products_file -ve test for access denied error
        :return:
        '''
        print("Curr working directory:")
        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.download_file') as mock_downloadFile:
            mock_downloadFile.return_value = "done"
            with mock.patch('os.path.getsize') as mock_getsize:
                mock_getsize.return_value = 600
                with mock.patch('os.path.isdir') as mock_isdir:
                    mock_isdir.return_value = False
                    with mock.patch(
                            'Scripts.Python.stageContentsToS3SyncHelper.sync_dir_to_s3') as mock_sync_dir_to_s3:
                        mock_sync_dir_to_s3.return_value = "error"
                        with mock.patch(
                                'Scripts.Python.stageContentsToS3SyncHelper.sync_file_to_s3') as mock_sync_file_to_s3:
                            mock_sync_file_to_s3.return_value = "AccessDenied"

                            self.assertRaises(SystemExit, upload_products, self.manifest_yaml,
                                              self.roles_yml_file,
                                              self.md5file, self.unc_path_base,
                                              self.s3_bucket_name, self.target_path,
                                              self.manifest_version, self.manifest_build)

    # Tests for function upload_downloadables

    def test_upload_downloadables_success(self):
        '''
        +ve test scenario for upload_downloadables
        :return:
        '''
        print("Curr working directory:")
        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.download_file') as mock_downloadFile:
            mock_downloadFile.return_value = "done"
            with mock.patch('os.path.getsize') as mock_getsize:
                mock_getsize.return_value = 600
                with mock.patch('os.path.isdir') as mock_isdir:
                    mock_isdir.return_value = False
                    with mock.patch(
                            'Scripts.Python.stageContentsToS3SyncHelper.sync_dir_to_s3') as mock_sync_dir_to_s3:
                        mock_sync_dir_to_s3.return_value = "Success"
                        with mock.patch(
                                'Scripts.Python.stageContentsToS3SyncHelper.sync_file_to_s3') as mock_sync_file_to_s3:
                            mock_sync_file_to_s3.return_value = "Success"
                            status = upload_downloadables(self.manifest_yaml, self.roles_yml_file, self.md5file,
                                                          self.unc_path_base, self.s3_bucket_name, self.target_path,
                                                          self.manifest_version, self.manifest_build)
                            self.assertEqual("Upload downloadables completed", status)

    def test_upload_downloadables_errorInUpload(self):
        '''
        -ve test scenario for upload_downloadables with error
        :return:
        '''
        print("Curr working directory:")
        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.download_file') as mock_downloadFile:
            mock_downloadFile.return_value = "done"
            with mock.patch('os.path.getsize') as mock_getsize:
                mock_getsize.return_value = 600
                with mock.patch('os.path.isdir') as mock_isdir:
                    mock_isdir.return_value = False
                    with mock.patch(
                            'Scripts.Python.stageContentsToS3SyncHelper.sync_dir_to_s3') as mock_sync_dir_to_s3:
                        mock_sync_dir_to_s3.return_value = "error"
                        with mock.patch(
                                'Scripts.Python.stageContentsToS3SyncHelper.sync_file_to_s3') as mock_sync_file_to_s3:
                            mock_sync_file_to_s3.return_value = "error"

                            self.assertRaises(SystemExit, upload_downloadables, self.manifest_yaml,
                                              self.roles_yml_file,
                                              self.md5file, self.unc_path_base,
                                              self.s3_bucket_name, self.target_path,
                                              self.manifest_version, self.manifest_build)

    def test_upload_downloadables_accessDenied(self):
        '''
        -ve test scenario for upload_downloadables with error
        :return:
        '''
        print("Curr working directory:")
        with mock.patch('Scripts.Python.stageContentsToS3SyncHelper.download_file') as mock_downloadFile:
            mock_downloadFile.return_value = "done"
            with mock.patch('os.path.getsize') as mock_getsize:
                mock_getsize.return_value = 600
                with mock.patch('os.path.isdir') as mock_isdir:
                    mock_isdir.return_value = False
                    with mock.patch(
                            'Scripts.Python.stageContentsToS3SyncHelper.sync_dir_to_s3') as mock_sync_dir_to_s3:
                        mock_sync_dir_to_s3.return_value = "AccessDenied"
                        with mock.patch(
                                'Scripts.Python.stageContentsToS3SyncHelper.sync_file_to_s3') as mock_sync_file_to_s3:
                            mock_sync_file_to_s3.return_value = "AccessDenied"

                            self.assertRaises(SystemExit, upload_downloadables, self.manifest_yaml,
                                              self.roles_yml_file,
                                              self.md5file, self.unc_path_base,
                                              self.s3_bucket_name, self.target_path,
                                              self.manifest_version, self.manifest_build)


if __name__ == '__main__':
    unittest.main()
