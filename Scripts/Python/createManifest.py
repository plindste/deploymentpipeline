import argparse
import yaml
import io
import glob
import os
from library import *

MANIFESTS_DIR = '/mnt/softwarefactory/Manifests/TIBCO Spotfire'

currentWorkingDirectory = os.getcwd()
log = set_logger()


def read_products_create_manifest(products_yml, manifest_file, release, official_deployment, channel, path_base,
                                  mount_directory, products_info):
    '''
    This function creates manifest file based on products.yml.
    :param products_yml: object of products_yml file
    :return: manifest file contents
    '''
    manifest_file_contents = ""
    log.info("manifest file name:" + manifest_file)
    manifest_file_contents += "release: " + release + "\n"
    manifest_file_contents += "official_deployment: " + official_deployment + "\n"
    manifest_file_contents += "channel: " + channel + "\n"
    manifest_file_contents += "path_base: " + path_base + "\n"
    products_directory = mount_directory + "/Products/"

    products = products_info.split(",")

    manifest_file_contents += "platform:" + "\n"
    for product in products:
        product_data = product.split(":")
        for prod_artifact in products_yml['products']:
            if prod_artifact['product_name'] == product_data[0]:
                manifest_file_contents = add_product_contents(manifest_file_contents, prod_artifact, product_data,
                                                              products_directory)

    log.info("-----------------------------------------------------------------")
    log.info("\n" + manifest_file_contents)
    return (manifest_file_contents)


def add_product_contents(manifest_file_contents, prod_artifact, product_data, products_directory):
    manifest_file_contents += "- product_name: " + product_data[0] + "\n"
    separator = ""
    if product_data[1] != "":
        separator = "/"
    manifest_file_contents += "  version: " + product_data[1] + "\n"
    prod_build = product_data[2]
    if prod_build != "" and product_data[1] != "":
        if "^" in prod_build or "*" in prod_build or "?" in prod_build:
            prod_build = (run_command_get_output(
                "ls -t '" + products_directory + prod_artifact['path'] + "/" +
                product_data[1] + "' | grep  " + prod_build + " | head -1")).replace("\n", "")
        else:
            log.info("No regex in the version checking if folder with exact version exists.")
            log.info("Checking path:" + products_directory + prod_artifact['path'] + "/" + product_data[
                1] + "/" + prod_build + "' ")
            run_command_get_output(
                "ls -t '" + products_directory + prod_artifact['path'] + "/" + product_data[
                    1] + "/" + prod_build + "' ")
    manifest_file_contents += "  build: " + prod_build + "\n"
    manifest_file_contents += "  artifacts:" + "\n"
    artifacts = (prod_artifact['includes'].replace("'", "")).split(",")
    for artifact in artifacts:
        if artifact.replace("'", "").startswith('URL:'):
            artifactList = artifact
            manifest_file_contents += "  - artifact: " + artifact.strip() + "\n"
        else:
            if separator == "":
                artifactList = run_command_get_output(
                    "cd \"" + products_directory + prod_artifact[
                        'path'] + "\" && find ./" + artifact.replace("\\", "/") + " -maxdepth 0 ")
            else:
                artifactList = run_command_get_output(
                    "cd \"" + products_directory + prod_artifact['path'] + "/" +
                    product_data[1] + separator + prod_build + "\" && find ./" + artifact.replace("\\",
                                                                                                  "/") + " -maxdepth 0 ")
            if artifactList.__len__() == 0:
                log.info("No artifacts found satisfying regular expression " + artifact.replace("\\",
                                                                                                "/") + " Please check in products yml file.")
                exit(1)
            artifactList = artifactList.split("\n")
            for artifact in artifactList:
                if artifact.strip() != "":
                    if separator == "":
                        manifest_file_contents += "  - artifact: " + artifact.replace(".",
                                                                                      prod_artifact['path'],
                                                                                      1) + "\n"
                    else:
                        manifest_file_contents += "  - artifact: " + artifact.replace(".", prod_artifact[
                            'path'] + "/" + product_data[1] + separator + prod_build, 1) + "\n"
    return manifest_file_contents


def get_official_deployment_number(manifestBuild, unc_path_base, manifestVersion, manifest_products_directory):
    if manifestBuild == "":
        manifest_version_directory = os.path.join(unc_path_base, 'Manifests', manifest_products_directory,
                                                  manifestVersion)
        latest_v_build = run_command_get_output('ls -t "' + manifest_version_directory + '" | grep "^V" | head -1')

        if "No such file or directory" in latest_v_build:
            log.error("Version directory does not exists. please check path:" + manifest_version_directory)
            exit(1)

        if latest_v_build == "":
            log.info("No build available this will be the first build version.")
            next_v_build_no = 1
        else:
            print("Current latest version of V build is :" + latest_v_build)
            next_v_build_no = int(latest_v_build[1:]) + 1

        print("Next v build number is:" + str(next_v_build_no))
        if next_v_build_no < 10:
            next_v_build = "V0" + str(next_v_build_no)
        else:
            next_v_build = "V" + str(next_v_build_no)
    else:
        next_v_build = manifestBuild
    return (next_v_build)


def create_manifest_file(manifest_file_contents, manifestVersion, manifestBuild, unc_path_base, manifest_file_name,
                         manifest_products_directory):
    '''
    This function creates manifest file on shared location in a folder with next build number.
    :param manifest_file_contents:  Manifest file contents in string format
    :param manifestVersion:  Manifest file version
    :param manifestBuild:  Manifest build number
    :param unc_path_base:  Path to mounted shared location
    :return:
    '''
    manifest_dir = os.path.join(unc_path_base, 'Manifests', manifest_products_directory, manifestVersion,
                                manifestBuild)
    # print ("Manifest directory to create :"+manifest_dir)
    run_command_get_output('mkdir -p "' + manifest_dir + '"')
    with io.FileIO(manifest_dir + "/" + manifest_file_name, "w") as file:
        file.write(manifest_file_contents)
    if os.path.isfile("./manifestFileDetails.txt"):
        os.remove("./manifestFileDetails.txt")

    manifestDetails = "MANIFEST_VERSION=" + manifestVersion + "\nMANIFEST_BUILD_NO=" + manifestBuild + "\nMANIFEST_FILE_NAME=" + manifest_file_name
    with io.FileIO("./manifestFileDetails.txt", "w") as file:
        file.write(manifestDetails)


def prepForManifest(release, deployment_scripts_branch, deployment_scripts_version, fileList, manifest_v_number):

    if not os.path.isdir(MANIFESTS_DIR):
        log.error('Path not found: ' + MANIFESTS_DIR)
        exit(1)

    if not os.path.isdir(MANIFESTS_DIR + '/' + release):
        os.mkdir(MANIFESTS_DIR + '/' + release)

    if manifest_v_number.startswith(('m', 'M')):
        log.error("You have specified manifest build number starting with M. Which is not valid please do not specify anything for next manifest build number else specify any other Manifest build number under development.")
        exit(1)

    sourceDir = '/mnt/softwarefactory/Internal_Infra/DeploymentScripts/' + deployment_scripts_branch + '/' + \
                deployment_scripts_version + '/'
    vNumber = ''
    try:
        vNumber = natural_sort(os.listdir(
            sourceDir)).pop()
    except OSError:
        log.error("Path not found! " + sourceDir)
        exit(1)

    for f in fileList:
        copyContentDeliveryFile(sourceDir, vNumber, f)

    current_manifest_v_number = ""
    if manifest_v_number == '':
        if not os.path.exists(MANIFESTS_DIR + '/' + release):
            log.error("Path not found! " + MANIFESTS_DIR + '/' + release)
            exit(1)
        try:
            mversionList=glob.glob(MANIFESTS_DIR + '/' + release + '/M*')
            if mversionList.__len__()>0:
                current_manifest_v_number = os.path.basename(natural_sort(mversionList).pop())
            else:
                current_manifest_v_number=''
        except OSError :
            log.error("Error occured not able to get next M number."+OSError)
            exit(1)

        if current_manifest_v_number == '':
            nextmanifest_v_number = 'M1'
        else:
            log.info("Current M number is " + current_manifest_v_number)
            current_number = current_manifest_v_number[1:]
            log.info("Current number is " + current_number)
            nextmanifest_v_number = int(current_number) + 1
            log.info("Next number is "+ str(nextmanifest_v_number))
            nextmanifest_v_number = "M" + str(nextmanifest_v_number)
    else:
        nextmanifest_v_number=manifest_v_number

    with io.FileIO("./manifestFileData.txt", "w") as file:
        file.write("MANIFEST_VERSION=" + release +
                   ",ROLES_YAML_BRANCH=" + deployment_scripts_branch +
                   ",ROLES_YAML_VERSION=" + deployment_scripts_version +
                   ",ROLES_YAML_BUILDNO=" + vNumber +
                   ",MANIFEST_BUILD_NO=" + nextmanifest_v_number)

    return nextmanifest_v_number


def doManifest(products_yml, manifest_file, release, channel, products_info, manifest_dir, official_deployment,
               mount_directory):
    # Load yaml file
    log.info("products yml file is :" + products_yml)
    products_yml_obj = yaml.load(open(products_yml))

    manifest_file_contents = read_products_create_manifest(products_yml_obj, manifest_file, release,
                                                           official_deployment, channel,
                                                           '//emea-got-filer1.emea.tibco.com/SWFactory/Products',
                                                           mount_directory, products_info)

    # Create manifest file in next version build folder.
    create_manifest_file(manifest_file_contents, release, official_deployment, mount_directory,
                         manifest_file, manifest_dir)


def main():
    '''
    main function to create manifest file.
    How to run the script?
    python createManifest.py  --products_yml  "tscl_products.yml" --manifest_file_name test.yml --release 7.7.0 --channel SAWS --official_deployment V27 --products_info "TIBCO Spotfire Server:7.7.0:^V,TIBCO Drivers:1.2.0:^RC-,Cloud Portal:7.7.0:^V,TIBCO Spotfire:7.7.0:^RC-3" --unc_source_mount_location "//emea-got-filer1.emea.tibco.com/SWFactory" --unc_domain "apac.tibco.com" --unc_user="spotfiretest" --unc_password "T3Redtr1Je" --mount_directory "/mnt/softwarefactory"

    :return:
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--release', help='Specify release number', required=True)
    parser.add_argument('--products_info',
                        help='Specify comma separated list of producst followed by colon followed by Version followed by build number',
                        required=True)
    parser.add_argument('--manifest_products_directory',
                        help='Specify manifest product directory. This is optional argument default value is TIBCO Spotfire Cloud',
                        required=False, default="TIBCO Spotfire Cloud")
    parser.add_argument('--manifest_v_number', required=True)
    parser.add_argument('--tsce_products_yml', required=True)
    parser.add_argument('--tscl_products_yml', required=True)
    parser.add_argument('--tsc_products_yml', required=True)
    parser.add_argument('--saws_products_yml', required=True)
    parser.add_argument('--onprem_products_yml', required=True)
    parser.add_argument('--tsce_manifest_file', required=True)
    parser.add_argument('--tscl_manifest_file', required=True)
    parser.add_argument('--tsc_manifest_file', required=True)
    parser.add_argument('--saws_manifest_file', required=True)
    parser.add_argument('--onprem_manifest_file', required=True)
    parser.add_argument('--deployment_scripts_branch', required=True)
    parser.add_argument('--deployment_scripts_version', required=True)

    unc_source_mount_location = "//emea-got-filer1.emea.tibco.com/SWFactory"
    unc_path_base = "/mnt/softwarefactory"
    unc_user = "got-shared-builder"
    unc_domain = "emea.tibco.com"
    unc_password = "N3KillyL0F"

    args = parser.parse_args()

    # Mount product source
    try:
        mount_unc(unc_source_mount_location, unc_domain, unc_user, unc_password, unc_path_base)
    except Exception:
        log.info("Exception occured. Ignoring exception here. ")

    official_deployment = prepForManifest(args.release, args.deployment_scripts_branch, args.deployment_scripts_version,
                    [args.tsce_products_yml, args.tscl_products_yml, args.tsc_products_yml, args.saws_products_yml,
                     args.onprem_products_yml], args.manifest_v_number)

    doManifest(args.tsce_products_yml, args.tsce_manifest_file, args.release, 'TSCE', args.products_info,
               'TIBCO Spotfire', official_deployment, unc_path_base)
    doManifest(args.tscl_products_yml, args.tscl_manifest_file, args.release, 'TSCL', args.products_info,
               'TIBCO Spotfire', official_deployment, unc_path_base)
    doManifest(args.tsc_products_yml, args.tsc_manifest_file, args.release, 'TSC', args.products_info,
               'TIBCO Spotfire', official_deployment, unc_path_base)
    doManifest(args.saws_products_yml, args.saws_manifest_file, args.release, 'SAWS', args.products_info,
               'TIBCO Spotfire', official_deployment, unc_path_base)
    doManifest(args.onprem_products_yml, args.onprem_manifest_file, args.release, 'ONPREM', args.products_info,
               'TIBCO Spotfire', official_deployment, unc_path_base)


main()
