import hashlib
import urllib2

import boto3
import botocore
import yaml

from library import *
from library import natural_sort, set_logger, copyContentDeliveryFile

currentWorkingDirectory = os.getcwd()
log = set_logger()


def download_file(url, to_directory):
    '''
    This function downloads the file from specified url to the specified directory
    :param url: Url path to the file on WEB
    :param to_directory: Absolute path to directory on machine where you want the file to get downloaded
    :return:
    '''
    log.info("Downloading from url:" + url)
    log.info("Downloading to directory:" + to_directory)
    log.info("target file name is:" + url.split("/")[-1])
    webFile = urllib2.urlopen(url)
    localFile = open(to_directory + "/" + url.split('/')[-1], 'w')
    localFile.write(webFile.read())
    webFile.close()
    localFile.close()


#
# def get_web_file_data(url):
#     '''
#     This function gets data from web file
#     :param url: Web file url
#     :return: data
#     '''
#     with urllib.request.urlopen(url) as url:
#         data = url.read()
#     return data


def get_manifest_file(args):
    '''
    This function returns path of the manifest file
    :param args: args object
    :return: manifest file path object
    '''

    manifest_dir = os.path.join(args.unc_path_base, 'Manifests', args.manifest_products_directory, args.manifestVersion,
                                args.manifestBuild)
    log.info("Getting manifest file from mounted location:" + manifest_dir)
    os.chdir(manifest_dir)
    files = os.listdir(os.getcwd())
    manifest_file = os.path.join(manifest_dir, files[0])
    log.info("Manifest file is:" + manifest_file)
    return manifest_file


# Commenting out this function since we are not using this.
# def get_md5sum(path,is_directory,md5file):
#     '''
#     This function fetches md5 sum of the specified file or files in the specified directory and appends to the specified md5 file.
#     :param path: File or directory path for md5 sum
#     :param is_directory:  True/False
#     :param md5file: Output Md5sum file file
#     :return:
#     '''
#     if is_directory:
#         run_command = "cd \"" + path + "\" && find . -type f -exec md5sum {} +>>" + md5file
#         recursive = " --recursive"
#     else:
#         run_command = '/usr/bin/md5sum "%s" >> "%s"' % (path, md5file)
#         recursive = ""
#     log.info ("Running command :" + run_command)
#     retvalue = os.system(run_command)
#     if retvalue != 0:
#         log.info ('An error occurred while creating md5 checksum. Exiting program.')
#         raise Exception


def upload_products(manifest_yml, roles_yml_file, md5file, unc_path_base, s3_bucket_name, target_path, manifest_version,
                    manifest_build):
    '''
    This function uploads all installers to S3 with respect to manifest.yml and roles.yml file
    :param manifest_yml: path to manifest.yaml file
    :param roles_yml_file: path to roles.yaml file
    :param md5file: Output path of md5 file.
    :param unc_path_base: mount directory base path
    :param s3_bucket_name: path to s3 bucket
    :param target_path: target directory path on s3 where you want to upload
    :param manifest_version: manifest version
    :param manifest_build: manifest build
    :return:
    '''

    roles_yml = yaml.load(open(roles_yml_file))
    channel = manifest_yml['channel']
    for role in roles_yml['roles']:
        log.info('Role is:' + role['role'].lower())
        artifacts_list = role['artifacts'].split(",")
        stage_artifacts(artifacts_list, channel, manifest_build, manifest_version, manifest_yml, role, roles_yml_file,
                        s3_bucket_name, target_path, unc_path_base)

    log.info('Upload complete.')
    return ("Upload completed successfully")


def stage_artifacts(artifacts_list, channel, manifest_build, manifest_version, manifest_yml, role, roles_yml_file,
                    s3_bucket_name, target_path, unc_path_base):
    for role_artifact in artifacts_list:
        artifact_found = False
        for item in manifest_yml['platform']:
            artifact_found = handle_artifact_artifacts(artifact_found, channel, item, manifest_build, manifest_version,
                                                       role, role_artifact, s3_bucket_name, target_path, unc_path_base)

        if not artifact_found:
            log.info("No artifact found matching regular expression: " + role_artifact)
            raise Exception
    sync_output = sync_file_to_s3(s3_bucket_name, roles_yml_file, target_path + '/' + channel + '/' + role[
        'role'].lower() + '/' + manifest_version + '/' + manifest_build + '/' + os.path.basename(roles_yml_file))
    log.info("output: " + sync_output)
    if "error" in sync_output or "AccessDenied" in sync_output:
        log.error("Failed to copy roles yml file.")
        exit(1)


def handle_artifact_artifacts(artifact_found, channel, item, manifest_build, manifest_version, role, role_artifact,
                              s3_bucket_name, target_path, unc_path_base):
    for artifact in item['artifacts']:
        match_result = re.search(role_artifact, artifact['artifact'])
        if match_result is not None:

            if artifact['artifact'].replace("'", "").startswith('URL:'):
                installer = handle_url_download(artifact)
            else:
                installer = (unc_path_base + "/Products/" + artifact['artifact']).replace("//", "/")

            log.info("installer to copy is:" + installer)
            is_directory = False
            if os.path.isdir(installer):
                # Temporary disabling md5 since I think we do not need this
                # get_md5sum(installer,True,md5file)
                if "deploymentscripts" in installer.lower():
                    sync_output = sync_dir_to_s3(s3_bucket_name, installer,
                                                 target_path + '/' + channel + '/' + role[
                                                     'role'].lower() + '/' + manifest_version + '/' + manifest_build + '/DeploymentScripts')
                    log.info("output: " + sync_output)
                    if "error" in sync_output or "AccessDenied" in sync_output:
                        log.error("There exists an error while copy to s3 please check.")
                        exit(1)
                    artifact_found = True

                else:
                    sync_output = sync_dir_to_s3(s3_bucket_name, installer,
                                                 target_path + '/' + 'INSTALLERS' + '/' + manifest_version + '/' + manifest_build + '/' + os.path.basename(
                                                     installer))
                    if "error" in sync_output or "AccessDenied" in sync_output:
                        log.error("There exists an error while copy to s3 please check.")
                        exit(1)
                    artifact_found = True


            else:
                # recursive = ""
                # Not a directory
                # Temporary disabling md5 sum since I think we do not need this.
                # get_md5sum(installer,False,md5file)

                sync_output = sync_file_to_s3(s3_bucket_name, os.path.abspath(installer),
                                              target_path + '/' + 'INSTALLERS' + '/' + manifest_version + '/' + manifest_build + '/' + os.path.basename(
                                                  installer))

                # log.info("Command output:" + sync_output)
                if "error" in sync_output or "AccessDenied" in sync_output:
                    log.error("There exists an error while copy to s3 please check.")
                    exit(1)
                artifact_found = True
    return artifact_found


def handle_url_download(artifact):
    # Handle for URL
    log.info("Artifact starts with URL:" + artifact['artifact'])
    downloadUrl = artifact['artifact'].replace("'", "").replace("URL:", "")
    download_file(downloadUrl, currentWorkingDirectory)
    installer = currentWorkingDirectory + "/" + downloadUrl.split('/')[-1]
    # installer_name = "Product/" + downloadUrl.split('/')[-1]
    file_size = os.path.getsize(installer)
    log.info("Size of file on disk is:" + file_size.__str__())
    if file_size < 500:
        log.error(
            "Download from url " + downloadUrl + " Not completed successfully. size of the file is less than 500 Please verify the url " + downloadUrl)
        exit(1)
    else:
        log.info(
            "Download of file from url " + downloadUrl + " Completed successfully. size of the file is:" + file_size.__str__())
    return installer


# Commenting out since we are not using this
# def configure_for_md5sum(manifest_version,manifest_build):
#     '''
#     Presetup for md5 sum file. This function creates the checksum file directory if it is not available, If md5 sum file already available then it removes that file.
#     :param manifest_version: manifest version
#     :param manifest_build: manifest build
#     :return: Path to md5 sum file
#     '''
#     md5path = 'checksums/'
#     md5dir = os.path.join(os.environ['WORKSPACE'], md5path)
#     if not os.path.isdir(md5dir):
#         os.makedirs(md5dir)
#     else:
#         pass
#     md5filename = manifest_version + manifest_build+ '.md5'
#     md5file = os.path.join(md5dir, md5filename)
#     # Delete if md5 sum file already exists.
#     try:
#         os.remove(md5file)
#     except OSError:
#         log.info ("Failed to remove md5 sum file :" + md5file)
#         pass
#     return md5file


def prepForStaging(roles_yml_branch, roles_yml_version, roles_yml_filepath):
    if not os.path.isdir('/mnt/softwarefactory/Products'):
        sourceMountLocation = '//emea-got-filer1.emea.tibco.com/SWFactory'
        pathBase = '/mnt/softwarefactory'
        uncUser = 'got-shared-builder'
        uncDomain = 'emea.tibco.com'
        uncPass = 'N3KillyL0F'
        os.system(
            'sudo -n mount -t cifs ' + sourceMountLocation + ' ' + pathBase + ' -o username='
            + uncUser + ',domain=' + uncDomain + ',password=' + uncPass)
    sourceDir = '/mnt/softwarefactory/Internal_Infra/DeploymentScripts/' + roles_yml_branch + '/' + \
                roles_yml_version + '/'
    vNumber = ''
    try:
        vNumber = natural_sort(os.listdir(
            sourceDir)).pop()
    except OSError:
        log.error("Path not found! " + sourceDir)
        exit(1)

    # copyContentDeliveryFile(sourceDir, vNumber, 'tsce_roles.yml')

    copyContentDeliveryFile(sourceDir, vNumber, os.path.basename(roles_yml_filepath))


def upload_downloadables(manifest_yml, roles_yml_file, md5file, unc_path_base, s3_bucket_name, target_path,
                         manifestVersion, manifestBuild):
    '''
    This function uploads downloadables to specified bucket to S3 with respect to manifest.yml and roles.yml file
    :param manifest_yml: path to manifest.yaml file
    :param roles_yml_file: path to roles.yaml file
    :param md5file: Output path of md5 file.
    :param unc_path_base: mount directory base path
    :param s3_bucket_name: path to s3 bucket
    :param target_path: target path where to upload
    :param manifestVersion: manifest version
    :param manifestBuild: manifest build
    :return:
    '''

    roles_yml = yaml.load(open(roles_yml_file))
    channel = manifest_yml['channel']
    if not ("downloadables" in roles_yml):
        log.info("WARN: Downloadables not available in the roles.yml skipping this.")
        return None

    for downloadable in roles_yml['downloadables']:
        log.info('Downloadable Artifacts List is:' + downloadable['artifacts'].lower())
        artifacts_list = downloadable['artifacts'].split(",")
        stage_downloadables(artifacts_list, manifestBuild, manifestVersion, manifest_yml, s3_bucket_name, target_path,
                            unc_path_base)
    log.info('Upload complete.')
    return ('Upload downloadables completed')


def stage_downloadables(artifacts_list, manifestBuild, manifestVersion, manifest_yml, s3_bucket_name, target_path,
                        unc_path_base):
    for downloadable_artifact in artifacts_list:
        artifact_found = False
        for item in manifest_yml['platform']:
            artifact_found = handle_downloadble_artifacts(artifact_found, downloadable_artifact, item, manifestBuild,
                                                          manifestVersion, s3_bucket_name, target_path, unc_path_base)

        if artifact_found == False:
            log.info("No artifact found matching regular expression: " + downloadable_artifact)
            raise ("No artifacts found matching regular expression " + downloadable_artifact)


def handle_downloadble_artifacts(artifact_found, downloadable_artifact, item, manifestBuild, manifestVersion,
                                 s3_bucket_name, target_path, unc_path_base):
    for artifact in item['artifacts']:
        match_result = re.search(downloadable_artifact, artifact['artifact'])
        if match_result != None:

            if artifact['artifact'].replace("'", "").startswith('URL:'):
                # Handle for URL
                log.info("Artifact starts with URL:" + artifact['artifact'])
                downloadUrl = artifact['artifact'].replace("'", "").replace("URL:", "")
                download_file(downloadUrl, currentWorkingDirectory)
                installer = currentWorkingDirectory + "/" + downloadUrl.split('/')[-1]
                # installer_name = "Product/" + downloadUrl.split('/')[-1]
                file_size = os.path.getsize(installer)
                log.info("Size of file on disk is:" + file_size.__str__())
                if file_size < 500:
                    log.error(
                        "Download from url " + downloadUrl + " Not completed successfully. size of the file is less than 500 Please verify the url " + downloadUrl)
                    exit(1)
                else:
                    log.info(
                        "Download of file from url " + downloadUrl + " Completed successfully. size of the file is:" + file_size.__str__())
            else:
                installer = (unc_path_base + "/Products/" + artifact['artifact']).replace("//", "/")

            log.info("installer to copy is:" + installer)
            is_directory = False
            if os.path.isdir(installer):
                # Temporary disabling md5 sum
                # get_md5sum(installer,True,md5file)
                sync_output = sync_dir_to_s3(s3_bucket_name, installer,
                                             target_path + '/' + 'cloud-downloads' + '/' + manifestVersion + '/' + manifestBuild + '/' + os.path.basename(
                                                 installer))
                if "error" in sync_output or "AccessDenied" in sync_output:
                    log.error("There exists an error while copy to s3 please check.")
                    exit(1)
                artifact_found = True
            else:
                # Temporary disabling md5 sum
                # get_md5sum(installer,False,md5file)
                sync_output = sync_file_to_s3(s3_bucket_name, os.path.abspath(installer),
                                              target_path + '/' + 'cloud-downloads' + '/' + manifestVersion + '/' + manifestBuild + '/' + os.path.basename(
                                                  installer))

                log.info("Command output:" + sync_output)
                if "error" in sync_output or "AccessDenied" in sync_output:
                    log.error("There exists an error while sync to s3 please check.")
                    exit(1)
                artifact_found = True
    return artifact_found


def s3_md5sum(bucket_name, resource_name):
    '''
    This function returns the md5 sum of a key on s3, If resource does not exist on s3 returns None
    :param bucket_name:  Name of the S3 bucket
    :param resource_name:  Path to the resource or name of the resource
    :return: md5 sum or None
    '''

    try:
        md5sum = boto3.client('s3').head_object(
            Bucket=bucket_name,
            Key=resource_name
        )['ETag'][1:-1]
    except botocore.exceptions.ClientError:
        md5sum = None
    return md5sum


def md5(file_path):
    '''
    This function returns md5 sum of the specified file
    :param file_path:
    :return:
    '''
    hash_md5 = hashlib.md5()
    with open(file_path, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def sync_file_to_s3(bucket_name, source_path, target_path):
    '''
    This function sync specified file to S3 based on change in checksum of source and target file
    :param bucket_name: Name of the S3 bucket
    :param source_path:  absolute path to the source file
    :param target_path: target path on s3
    :return:
    '''
    try:
        s3 = boto3.resource('s3')
        if target_path.startswith('/'):
            target_path = target_path.replace('/', '', 1)
        bucket = s3.Bucket(bucket_name)
        src_md5sum = md5(source_path)
        md5_on_s3 = s3_md5sum(bucket_name, target_path)
        if src_md5sum == md5_on_s3:
            log.info("MD5 sum matches skipping copy for " + source_path + " to " + target_path)
            return "Skipping copy version of file already exists."
        else:
            with open(source_path, 'rb') as data:
                log.info("Copying file to s3 bucket " + bucket_name + "/" + target_path)
                bucket.put_object(Key=target_path, Body=data)
                return "success"
    except Exception as exc:
        log.error("Error occured during stage to s3 " + exc)
        return ("Error occured exception is:" + exc)


def copy_dir_to_s3(bucket_name, source_path, target_path):
    '''
    This function copies specified directory to S3 recursively.
    :param bucket_name: Name of the S3 bucket
    :param source_path: Path to source directory
    :param target_path:  Target path on s3
    :return:
    '''
    s3 = boto3.resource('s3')
    if target_path.startswith('/'):
        target_path = target_path.replace('/', '', 1)

    bucket = s3.Bucket(bucket_name)
    for subdir, dirs, files in os.walk(source_path):
        for file in files:
            full_path = os.path.join(subdir, file)
            with open(full_path, 'rb') as data:
                log.info(
                    'Uploading file to path:' + target_path + '/' + full_path[len(source_path) + 1:].replace('\\', '/'))
                bucket.put_object(Key=target_path + '/' + full_path[len(source_path) + 1:].replace('\\', '/'),
                                  Body=data)


def sync_dir_to_s3(bucket_name, source_path, target_path):
    '''
    This method sync specified directory recursively to S3, It uses md3 sum to check difference in files.
    :param bucket_name: Name of the S3 bucket
    :param source_path:  Path to the source directory
    :param target_path: Target path on S3
    :return:
    '''
    s3 = boto3.resource('s3')
    if target_path.startswith('/'):
        target_path = target_path.replace('/', '', 1)
    bucket = s3.Bucket(bucket_name)
    if not os.path.exists(source_path):
        return "error source direcotry " + source_path + " does not exists"
    for subdir, dirs, files in os.walk(source_path):
        for file in files:
            full_path_src = os.path.join(subdir, file)
            log.info("full path on os:" + full_path_src)
            full_path_src_md5 = md5(full_path_src)
            log.info("full path src md5:" + full_path_src_md5)
            with open(full_path_src, 'rb') as data:
                target_file_s3 = target_path + '/' + full_path_src[len(source_path) + 1:].replace('\\', '/')
                target_file_s3_md5 = s3_md5sum(bucket_name, target_file_s3)
                if full_path_src_md5 == target_file_s3_md5:
                    log.info("MD5 sum matches skipping copy for " + full_path_src_md5 + " to " + target_file_s3)
                else:
                    log.info('putting to path:' + target_file_s3)
                    bucket.put_object(Key=target_file_s3, Body=data)
    return "sync directory completed successfully"
