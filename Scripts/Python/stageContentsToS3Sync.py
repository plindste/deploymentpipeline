import argparse
import yaml
import re
import urllib
import boto3
from library import *
from stageContentsToS3SyncHelper import *
from stageContentsToS3SyncHelper import upload_downloadables

currentWorkingDirectory = os.getcwd()
log = set_logger()


def main():
    '''
      This is a main function
     How to Run?
      python stageContentsToS3.py  --manifest_file /root/manifestfile.yml  --manifestVersion 99.0.0.0 --manifestBuild V01
     --s3bucket s3://pune-auto/Content  --unc_source_mount_location "//emea-got-filer1.emea.tibco.com/SWFactory"
     --unc_domain emea --unc_user iss-access --unc_password en0RierL1F --unc_path_base /mnt/softwarefactory/ --roles_yml roles.yml

    :return:
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--manifestVersion', help='Version of Yaml manifest file', required=True)
    parser.add_argument('--manifestBuild', help='Build of Yaml manifest file', required=True)
    parser.add_argument('--s3bucket', help='Base path of the S3 Bucket', required=True)
    parser.add_argument('--unc_domain', help='Domain of the LDAP service account', required=True)
    parser.add_argument('--unc_user', help='Username of the LDAP service account', required=True)
    parser.add_argument('--unc_password', help='Password for the LDAP service account', required=True)
    parser.add_argument('--unc_path_base', help='Location of the mounted SWFactory share', required=True)
    parser.add_argument('--manifest_file', help='Hard-coded path to manifestFile this is optional if not specified it will automatically take from mount location', required=False, default="")
    parser.add_argument('--unc_source_mount_location', help='Source mount directory', required=True)
    parser.add_argument('--roles_yml', help='Roles yml file path', required=True)
    parser.add_argument('--roles_yml_branch', help='Roles yml branch', required=True)
    parser.add_argument('--roles_yml_version', help='Roles yml version', required=True)
    parser.add_argument('--manifest_products_directory', help='Specify manifest product directory. This is optional argument default value is TIBCO Spotfire Cloud',required=False, default="TIBCO Spotfire Cloud")

    args = parser.parse_args()

    #Fetch S3 bucket name and container path
    if "s3://" in args.s3bucket.lower():
        s3_path=args.s3bucket.replace("s3://","",1)
        s3_bucket_name=s3_path.split("/")[0]
        s3_path=s3_path.replace(s3_bucket_name,"")
        log.info ("S3 bucket is:"+s3_bucket_name)
        log.info("S3 path is: "+s3_path)
    else:
        log.error("Please specify valid s3 bucket path e.g. s3://pune-auto/Content")
        exit(1)

    #mount_unc(args.unc_source_mount_location, args.unc_domain, args.unc_user, args.unc_password, args.unc_path_base)
    prepForStaging(args.roles_yml_branch, args.roles_yml_version,args.roles_yml)

    #Get manifest file from mount
    if args.manifest_file == "":
        manifest_file=get_manifest_file(args)
    else:

        manifest_dir = os.path.join(args.unc_path_base, 'Manifests', args.manifest_products_directory,
                                    args.manifestVersion,
                                    args.manifestBuild)
        manifest_file=manifest_dir+"/"+args.manifest_file
    log.info ("manifest file is :"+manifest_file)

    #Load manifest file
    manifest_yml = yaml.load(open(manifest_file))

    #Pre configuration for md5 sum file
    #md5file=configure_for_md5sum(args.manifestVersion,args.manifestBuild)
    #Since we are not using md5sum for now setting it up to blank for future use if required and passing it on
    md5file=""

    #Read manifest file and upload products to s3
    log.info( 'Uploading products to s3\n')
    upload_products(manifest_yml,args.roles_yml,  md5file,args.unc_path_base,s3_bucket_name,s3_path ,args.manifestVersion ,args.manifestBuild )
    upload_downloadables(manifest_yml,args.roles_yml,  md5file,args.unc_path_base,"signed-url-distribution","" ,args.manifestVersion ,args.manifestBuild )
    # Upload manifest file to s3
    sync_file_to_s3(s3_bucket_name, manifest_file, s3_path+'/INSTALLERS/' + args.manifestVersion + '/' + args.manifestBuild + '/' +os.path.basename(manifest_file)+ '"')

    log.info ("Successfully uploaded manifest file to s3")

    # Upload md5 checksum file to s3
    #s3_md5file = args.s3bucket + '/' + args.manifestVersion+ '/' + args.manifestBuild+ '/' + 'manifest.md5'

main()
