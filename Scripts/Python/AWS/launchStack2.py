import argparse
import logging
import tempfile
import time
import sys
import urllib2
import boto3
import yaml


class FailedStackCreation(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ParseFault(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


log = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')
log.setLevel(level=logging.INFO)


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--manifest", help="Path to manifest on local drive", required=True)
    parser.add_argument("--productcode", help="TSCE, TSCL, TSC", required=True)
    parser.add_argument("--type", help="rev1608network, rev1608platform, rev1608rds", required=True)
    parser.add_argument("--environment-name", help="tscdev, tsctest, tscperf, ...", required=True)
    parser.add_argument("--aws-region", help="us-west2, us-east1, ...", required=True)
    parser.add_argument("--verbose", help="Enable debug output", action='store_true')
    parser.add_argument("--disable-rollback", help="Disable automatic rollback of launched stack", action='store_true')

    opts, extra = parser.parse_known_args(argv)

    if opts.verbose:
        log.setLevel(level=logging.DEBUG)

    log.info("Starting...")

    try:
        manifest_data = yaml.load(open(opts.manifest))
        definitions_url = get_definitions_url(manifest_data)
        opts.s3_template_url = get_template_url(manifest_data)
        definitions = yaml.load(load_file(definitions_url))
        log.debug(definitions)

        validate_type_and_productcode(definitions, definitions_url, opts)

        environment = definitions['Networks'][opts.productcode][opts.type]
        definitions_parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        for param in environment['Template']['RequiredInput']:
            definitions_parser.add_argument('--' + param, required=True)

        for param_with_default in environment['Template']['Parameters']:
            key, value = param_with_default.popitem()
            definitions_parser.add_argument('--' + key, default=value, help=key)

        template_options = definitions_parser.parse_args(extra)
        opts.parameter_list = []

        for key, value in vars(template_options).iteritems():
            opts.parameter_list.append({'ParameterKey': key, 'ParameterValue': value})

        log.info('Launching stack: {}'.format(opts.s3_template_url))
        log.debug('Paramters: {}'.format(opts))
        create_stack(opts.environment_name, opts.aws_region, opts, boto3)
    except Exception, e:
        log.critical('{} failed: {}\n{}'.format(__file__, e.__class__.__name__, e))
        exit(1)
    exit(0)


def get_definitions_url(yaml_content):
    return get_cloudinfra_url(yaml_content, 'definitions')


def get_template_url(yaml_content):
    return get_cloudinfra_url(yaml_content, 'cf-template')


def get_cloudinfra_url(yaml_content, artifact):
    log.debug(yaml_content)
    cloud_infra_list = [x['artifacts'] for x in yaml_content['platform'] if x['product_name'] == 'Cloud Infrastructure']
    if not cloud_infra_list:
        raise ParseFault('Cloud Infrastructure not found in manifest')
    definition_list = [y['artifact'] for y in cloud_infra_list.pop() if artifact == y['name']]
    if not definition_list:
        raise ParseFault('{} file missing in manifest'.format(artifact))
    return definition_list.pop()


def load_file(manifest_artifact):
    manifest_artifact = manifest_artifact.replace("'", "")
    if is_manifest_url(manifest_artifact):
        try:
            manifest_artifact = manifest_artifact.replace('URL:', '')
            log.debug("Downloading from url: {}".format(manifest_artifact))
            web_file = urllib2.urlopen(manifest_artifact)
            contents = web_file.read()
            web_file.close()
            return contents
        except Exception, e:
            raise FailedStackCreation('Failed to download file. Nested: {}'.format(e))
    else:
        raise FailedStackCreation('Non-URL manifest entries are not supported: {}'.format(manifest_artifact))


def is_manifest_url(possible_url):
    return possible_url.startswith('URL:')


def validate_type_and_productcode(definitions, definitions_url, opts):
    if opts.productcode not in definitions['Networks']:
        raise ParseFault("Productcode '{}' not found in {}".format(opts.productcode, definitions_url))
    elif opts.type not in definitions['Networks'][opts.productcode]:
        raise ParseFault('Type "{}" not found in {}'.format(opts.type, opts.productcode))


def create_stack(name, region, opts, boto):
    try:
        cf = boto.client('cloudformation', region_name=region)
    except Exception, e:
        raise FailedStackCreation("Region {} does not exist. boto exception: {}".format(region, e))

    stack_exists = False
    try:
        cf.describe_stacks(name)
        stack_exists = True
    except Exception:
        pass  # We want to get an exception here
    if stack_exists:
        raise FailedStackCreation("Stack {} already exists".format(name))

    log.info('Starting stack creation')

    try:
        create_response = cf.create_stack(
            StackName=name,
            TemplateURL=opts.s3_template_url,
            Parameters=opts.parameter_list,
            DisableRollback=opts.disable_rollback,
            TimeoutInMinutes=120,
            Capabilities=["CAPABILITY_IAM"])
    except Exception, e:
        raise FailedStackCreation("Call to cloudformation failed. boto exception: {}".format(e))

    log.info('Waiting for creation')

    waiter = cf.get_waiter('stack_create_complete')
    waiter.wait(StackName=create_response['StackId'])

    log.info('Stack creation done')


if __name__ == '__main__':
    main(sys.argv[1:])
