import copy
import unittest

import yaml
from Scripts.Python.AWS.launchStack2 import *

import mockBoto


class MicroMock(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class LaunchStack2Tests(unittest.TestCase):
    manifest = {}
    definitions = {}

    @classmethod
    def setUpClass(cls):
        with open('Scripts/Python/AWS/Tests/TSC-manifest.testdata.yml') as defFile:
            LaunchStack2Tests.manifest = yaml.load(defFile)
        with open('Scripts/Python/AWS/Tests/definitions.testdata.yml') as defFile:
            LaunchStack2Tests.definitions = yaml.load(defFile)

    def setUp(self):
        self.options = MicroMock(s3_template_url="s3://url",
                                 parameter_list=[{'ParameterKey': 'value', 'ParameterList': 'value'}],
                                 disable_rollback=True)
        self.client = mockBoto.MockBoto().client('cloudformation', region_name='exists')
        self.manifest = copy.deepcopy(LaunchStack2Tests.manifest)
        self.definitions = copy.deepcopy(LaunchStack2Tests.definitions)

    # create_stack
    def test_create_stack_raises_on_region_nonexistant(self):
        with self.assertRaises(FailedStackCreation):
            create_stack(name='does_not_exist', region='does_not_exist',
                         opts=self.options,
                         boto=mockBoto.MockBoto())

    def test_create_stack_raises_on_stack_exists(self):
        with self.assertRaises(FailedStackCreation):
            create_stack(name='exists', region='exists', opts=self.options,
                         boto=mockBoto.MockBoto())

    def test_create_stack_does_not_raise_if_ok(self):
        create_stack(name='does_not_exist', region='exists', opts=self.options,
                     boto=mockBoto.MockBoto())

    # get_definitions_url
    def test_get_definitions_url(self):
        self.assertEqual(get_definitions_url(self.manifest), 'definitions.yml')

    def test_get_definitions_url_raises_on_missing_cloud_infra(self):
        del self.manifest['platform'][5]
        with self.assertRaises(ParseFault):
            get_definitions_url(self.manifest)

    def test_get_definitions_url_raises_on_missing_artifact(self):
        del self.manifest['platform'][5]['artifacts'][0]
        with self.assertRaises(ParseFault):
            get_definitions_url(self.manifest)

    # get_template_url
    def test_get_template_url(self):
        self.assertEqual(get_template_url(self.manifest), 'cf-template.template')

    # load_file
    def test_download_file_raises_on_non_url(self):
        with self.assertRaises(FailedStackCreation):
            load_file('../Internal_Infra/definitions.yml')

    # validate_type_and_productcode
    def test_validate_type_pc_raises_on_missing_pc(self):
        opts = MicroMock(productcode='nonexistent')
        with self.assertRaises(ParseFault):
            validate_type_and_productcode(self.definitions, 'url', opts)

    def test_validate_type_pc_raises_on_missing_type(self):
        opts = MicroMock(productcode='TSC', type='nonexistent')
        with self.assertRaises(ParseFault):
            validate_type_and_productcode(self.definitions, 'url', opts)

    def test_validate_type_pc_does_not_raise_normally(self):
        opts = MicroMock(productcode='TSC', type='rev1608platform')
        validate_type_and_productcode(self.definitions, 'url', opts)


if __name__ == '__main__':
    unittest.main()
