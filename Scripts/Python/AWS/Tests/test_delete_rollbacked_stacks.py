import unittest
from datetime import datetime

from Scripts.Python.AWS.deleteRollbackedStacks import stacks_to_delete


class DeleteRollbackedStacksTests(unittest.TestCase):
    def setUp(self):
        self.stacks = {
            'Stacks': [
                {
                    'StackId': 'string',
                    'StackName': 'app1',
                    'ChangeSetId': 'string',
                    'Description': 'string',
                    'Parameters': [
                        {
                            'ParameterKey': 'string',
                            'ParameterValue': 'string',
                            'UsePreviousValue': False
                        },
                    ],
                    'CreationTime': datetime(2015, 1, 1),
                    'DeletionTime': datetime(2015, 1, 1),
                    'LastUpdatedTime': datetime(2015, 1, 1),
                    'RollbackConfiguration': {
                        'RollbackTriggers': [
                            {
                                'Arn': 'string',
                                'Type': 'string'
                            },
                        ],
                        'MonitoringTimeInMinutes': 123
                    },
                    'StackStatus': 'CREATE_IN_PROGRESS',
                    'StackStatusReason': 'string',
                    'DisableRollback': False,
                    'NotificationARNs': [
                        'string',
                    ],
                    'TimeoutInMinutes': 123,
                    'Capabilities': [
                        'CAPABILITY_IAM',
                    ],
                    'Outputs': [
                        {
                            'OutputKey': 'string',
                            'OutputValue': 'string',
                            'Description': 'string',
                            'ExportName': 'string'
                        },
                    ],
                    'RoleARN': 'string',
                    'Tags': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ],
                    'EnableTerminationProtection': False,
                    'ParentId': 'string',
                    'RootId': 'string'
                },
                {
                    'StackId': 'string',
                    'StackName': 'app2',
                    'ChangeSetId': 'string',
                    'Description': 'string',
                    'Parameters': [
                        {
                            'ParameterKey': 'string',
                            'ParameterValue': 'string',
                            'UsePreviousValue': False
                        },
                    ],
                    'CreationTime': datetime(2015, 1, 1),
                    'DeletionTime': datetime(2015, 1, 1),
                    'LastUpdatedTime': datetime(2015, 1, 1),
                    'RollbackConfiguration': {
                        'RollbackTriggers': [
                            {
                                'Arn': 'string',
                                'Type': 'string'
                            },
                        ],
                        'MonitoringTimeInMinutes': 123
                    },
                    'StackStatus': 'CREATE_IN_PROGRESS',
                    'StackStatusReason': 'string',
                    'DisableRollback': False,
                    'NotificationARNs': [
                        'string',
                    ],
                    'TimeoutInMinutes': 123,
                    'Capabilities': [
                        'CAPABILITY_IAM',
                    ],
                    'Outputs': [
                        {
                            'OutputKey': 'string',
                            'OutputValue': 'string',
                            'Description': 'string',
                            'ExportName': 'string'
                        },
                    ],
                    'RoleARN': 'string',
                    'Tags': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ],
                    'EnableTerminationProtection': False,
                    'ParentId': 'string',
                    'RootId': 'string'
                },
            ],
            'NextToken': 'string'
        }

    def test_no_stacks_returns_empty(self):
        self.assertEqual(stacks_to_delete({'Stacks': []}), [])

    def test_1_rolledback_stack_1_in_progress_returns_rolledback_stackname(self):
        self.stacks['Stacks'][0]['StackStatus'] = 'ROLLBACK_COMPLETE'
        self.assertEqual(stacks_to_delete(self.stacks), ["app1"])

    def test_in_progress_stacks_returns_empty(self):
        self.assertEqual(stacks_to_delete(self.stacks), [])

    def test_2_rolledback_stacks_returns_both_stacknames(self):
        self.stacks['Stacks'][0]['StackStatus'] = 'ROLLBACK_COMPLETE'
        self.stacks['Stacks'][1]['StackStatus'] = 'ROLLBACK_COMPLETE'
        self.assertEqual(stacks_to_delete(self.stacks), ["app1", "app2"])


if __name__ == '__main__':
    unittest.main()
