import unittest
import copy
from datetime import datetime

from Scripts.Python.AWS.awsHelpers import *


class AwsHelpersTests(unittest.TestCase):
    def setUp(self):
        self.stacks = {
            'Stacks': [
                {
                    'StackId': 'id1',
                    'StackName': 'app1',
                    'ChangeSetId': 'string',
                    'Description': 'string',
                    'Parameters': [
                        {
                            'ParameterKey': 'string',
                            'ParameterValue': 'string',
                            'UsePreviousValue': False
                        },
                    ],
                    'CreationTime': datetime(2015, 1, 1),
                    'DeletionTime': datetime(2015, 1, 1),
                    'LastUpdatedTime': datetime(2015, 1, 1),
                    'RollbackConfiguration': {
                        'RollbackTriggers': [
                            {
                                'Arn': 'string',
                                'Type': 'string'
                            },
                        ],
                        'MonitoringTimeInMinutes': 123
                    },
                    'StackStatus': 'CREATE_IN_PROGRESS',
                    'StackStatusReason': 'string',
                    'DisableRollback': False,
                    'NotificationARNs': [
                        'string',
                    ],
                    'TimeoutInMinutes': 123,
                    'Capabilities': [
                        'CAPABILITY_IAM',
                    ],
                    'Outputs': [
                        {
                            'OutputKey': 'string',
                            'OutputValue': 'string',
                            'Description': 'string',
                            'ExportName': 'string'
                        },
                    ],
                    'RoleARN': 'string',
                    'Tags': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ],
                    'EnableTerminationProtection': False,
                    'ParentId': 'string',
                    'RootId': 'string'
                },
                {
                    'StackId': 'id2',
                    'StackName': 'app2',
                    'ChangeSetId': 'string',
                    'Description': 'string',
                    'Parameters': [
                        {
                            'ParameterKey': 'string',
                            'ParameterValue': 'string',
                            'UsePreviousValue': False
                        },
                    ],
                    'CreationTime': datetime(2015, 1, 1),
                    'DeletionTime': datetime(2015, 1, 1),
                    'LastUpdatedTime': datetime(2015, 1, 1),
                    'RollbackConfiguration': {
                        'RollbackTriggers': [
                            {
                                'Arn': 'string',
                                'Type': 'string'
                            },
                        ],
                        'MonitoringTimeInMinutes': 123
                    },
                    'StackStatus': 'CREATE_IN_PROGRESS',
                    'StackStatusReason': 'string',
                    'DisableRollback': False,
                    'NotificationARNs': [
                        'string',
                    ],
                    'TimeoutInMinutes': 123,
                    'Capabilities': [
                        'CAPABILITY_IAM',
                    ],
                    'Outputs': [
                        {
                            'OutputKey': 'Wtf',
                            'OutputValue': '!',
                            'Description': 'string',
                            'ExportName': 'string'
                        },
                        {
                            'OutputKey': 'PARENTSTACK',
                            'OutputValue': 'app1',
                            'Description': 'string',
                            'ExportName': 'string'
                        },
                    ],
                    'RoleARN': 'string',
                    'Tags': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ],
                    'EnableTerminationProtection': False,
                    'ParentId': 'string',
                    'RootId': 'string'
                },
            ],
            'NextToken': 'string'
        }
        self.rdses = {
            'Marker': 'string',
            'DBInstances': [
                {
                    'DBInstanceIdentifier': 'rds-id',
                    'DBInstanceClass': 'string',
                    'Engine': 'string',
                    'DBInstanceStatus': 'string',
                    'MasterUsername': 'string',
                    'DBName': 'string',
                    'Endpoint': {
                        'Address': 'string',
                        'Port': 123,
                        'HostedZoneId': 'string'
                    },
                    'AllocatedStorage': 123,
                    'InstanceCreateTime': datetime(2015, 1, 1),
                    'PreferredBackupWindow': 'string',
                    'BackupRetentionPeriod': 123,
                    'DBSecurityGroups': [
                        {
                            'DBSecurityGroupName': 'string',
                            'Status': 'string'
                        },
                    ],
                    'VpcSecurityGroups': [
                        {
                            'VpcSecurityGroupId': 'string',
                            'Status': 'string'
                        },
                    ],
                    'DBParameterGroups': [
                        {
                            'DBParameterGroupName': 'string',
                            'ParameterApplyStatus': 'string'
                        },
                    ],
                    'AvailabilityZone': 'string',
                    'DBSubnetGroup': {
                        'DBSubnetGroupName': 'string',
                        'DBSubnetGroupDescription': 'string',
                        'VpcId': 'vpc-id',
                        'SubnetGroupStatus': 'string',
                        'Subnets': [
                            {
                                'SubnetIdentifier': 'string',
                                'SubnetAvailabilityZone': {
                                    'Name': 'string'
                                },
                                'SubnetStatus': 'string'
                            },
                        ],
                        'DBSubnetGroupArn': 'string'
                    },
                    'PreferredMaintenanceWindow': 'string',
                    'PendingModifiedValues': {
                        'DBInstanceClass': 'string',
                        'AllocatedStorage': 123,
                        'MasterUserPassword': 'string',
                        'Port': 123,
                        'BackupRetentionPeriod': 123,
                        'MultiAZ': True,
                        'EngineVersion': 'string',
                        'LicenseModel': 'string',
                        'Iops': 123,
                        'DBInstanceIdentifier': 'string',
                        'StorageType': 'string',
                        'CACertificateIdentifier': 'string',
                        'DBSubnetGroupName': 'string'
                    },
                    'LatestRestorableTime': datetime(2015, 1, 1),
                    'MultiAZ': True,
                    'EngineVersion': 'string',
                    'AutoMinorVersionUpgrade': True,
                    'ReadReplicaSourceDBInstanceIdentifier': 'string',
                    'ReadReplicaDBInstanceIdentifiers': [
                        'string',
                    ],
                    'ReadReplicaDBClusterIdentifiers': [
                        'string',
                    ],
                    'LicenseModel': 'string',
                    'Iops': 123,
                    'OptionGroupMemberships': [
                        {
                            'OptionGroupName': 'string',
                            'Status': 'string'
                        },
                    ],
                    'CharacterSetName': 'string',
                    'SecondaryAvailabilityZone': 'string',
                    'PubliclyAccessible': True,
                    'StatusInfos': [
                        {
                            'StatusType': 'string',
                            'Normal': True,
                            'Status': 'string',
                            'Message': 'string'
                        },
                    ],
                    'StorageType': 'string',
                    'TdeCredentialArn': 'string',
                    'DbInstancePort': 123,
                    'DBClusterIdentifier': 'string',
                    'StorageEncrypted': True,
                    'KmsKeyId': 'string',
                    'DbiResourceId': 'string',
                    'CACertificateIdentifier': 'string',
                    'DomainMemberships': [
                        {
                            'Domain': 'string',
                            'Status': 'string',
                            'FQDN': 'string',
                            'IAMRoleName': 'string'
                        },
                    ],
                    'CopyTagsToSnapshot': True,
                    'MonitoringInterval': 123,
                    'EnhancedMonitoringResourceArn': 'string',
                    'MonitoringRoleArn': 'string',
                    'PromotionTier': 123,
                    'DBInstanceArn': 'string',
                    'Timezone': 'string',
                    'IAMDatabaseAuthenticationEnabled': True,
                    'PerformanceInsightsEnabled': True,
                    'PerformanceInsightsKMSKeyId': 'string'
                },
            ]
        }
        self.vpcs = {
            'Vpcs': [
                {
                    'CidrBlock': 'string',
                    'DhcpOptionsId': 'string',
                    'State': 'available',
                    'VpcId': 'vpc1-id',
                    'InstanceTenancy': 'default',
                    'Ipv6CidrBlockAssociationSet': [
                        {
                            'AssociationId': 'string',
                            'Ipv6CidrBlock': 'string',
                            'Ipv6CidrBlockState': {
                                'State': 'associated',
                                'StatusMessage': 'string'
                            }
                        },
                    ],
                    'CidrBlockAssociationSet': [
                        {
                            'AssociationId': 'string',
                            'CidrBlock': 'string',
                            'CidrBlockState': {
                                'State': 'associated',
                                'StatusMessage': 'string'
                            }
                        },
                    ],
                    'IsDefault': True,
                    'Tags': [
                        {
                            'Key': 'Name',
                            'Value': 'vpc1'
                        },
                    ]
                },
            ]
        }

    # getStacks
    def test_0_stacks_returns_empty(self):
        self.assertEqual(get_stacks('app', {'Stacks': []}), {'Stacks': []})

    def test_1_match_returns_match(self):
        self.stacks['Stacks'][1]['StackName'] = 'rds1'
        self.assertEqual(get_stacks('app', self.stacks), {'Stacks': [self.stacks['Stacks'][0]]})

    def test_2_match_returns_both(self):
        self.assertEqual(get_stacks('app', self.stacks), {'Stacks': self.stacks['Stacks']})

    # get_addon_stacks
    def test_addon_0_stacks_returns_empty(self):
        self.assertEqual(get_addon_stacks({'Stacks': []}), {'Stacks': []})

    def test_nw_stack_returns_empty(self):
        del self.stacks['Stacks'][1]
        self.assertEqual(get_addon_stacks(self.stacks), {'Stacks': []})

    def test_1addon_returns_the_addon(self):
        del self.stacks['Stacks'][0]
        self.assertEqual(get_addon_stacks(self.stacks), {'Stacks': self.stacks['Stacks']})

    def test_1_addon_nw_returns_the_addon(self):
        self.assertEqual(get_addon_stacks(self.stacks), {'Stacks': [self.stacks['Stacks'][1]]})

    # get_stack_names
    def test_names_0_stacks_returns_empty(self):
        self.assertEqual(get_stack_names({'Stacks': []}), [])

    def test_1_stack_returns_its_name(self):
        del self.stacks['Stacks'][1]
        self.assertEqual(get_stack_names(self.stacks), ["app1"])

    def test_2_stacks_returns_both_names(self):
        self.assertEqual(get_stack_names(self.stacks), ["app1", "app2"])

    # filter_wait_list
    def test_filter_empty_returns_empty(self):
        self.assertEqual(filter_wait_dict({}), {})

    def test_filter_complete_returns_empty(self):
        self.assertEqual(filter_wait_dict({'tscdev-app1': 'DELETE_COMPLETE'}), {})

    def test_filter_in_progress_returns_in_progress(self):
        self.assertEqual(filter_wait_dict({'tscdev-app1': 'DELETE_IN_PROGRESS'}), {'tscdev-app1': 'DELETE_IN_PROGRESS'})

    def test_filter_mixed_returns_in_progress(self):
        self.assertEqual(filter_wait_dict({'tscdev-app1': 'DELETE_IN_PROGRESS', 'tscdev-app2': 'DELETE_COMPLETE'}),
                         {'tscdev-app1': 'DELETE_IN_PROGRESS'})

    # get_stack_ids
    def test_ids_0_stacks_returns_empty(self):
        self.assertEqual(get_stack_ids({'Stacks': []}), [])

    def test_ids_1_stack_returns_its_id(self):
        del self.stacks['Stacks'][1]
        self.assertEqual(get_stack_ids(self.stacks), ["id1"])

    def test_ids_2_stacks_returns_both_ids(self):
        self.assertEqual(get_stack_ids(self.stacks), ["id1", "id2"])

    # get_vpc_id
    def test_vpc_0_vpcs_returns_none(self):
        self.assertEqual(get_vpc_id('vpc1', { 'Vpcs' : [] }), None)

    def test_vpc_0_matches_returns_none(self):
        self.assertEqual(get_vpc_id('vpc-does-not-exist', self.vpcs), None)

    def test_vpc_1_match_returns_match(self):
        self.assertEqual(get_vpc_id('vpc1', self.vpcs), 'vpc1-id')

    def test_vpc_1_match_1_no_match_returns_match(self):
        self.vpcs['Vpcs'].append(copy.deepcopy(self.vpcs['Vpcs'][0]))
        self.vpcs['Vpcs'][0]['VpcId'] = 'vpc2-id'
        self.vpcs['Vpcs'][0]['Tags'][0]['Value'] = 'vpc2'
        self.assertEqual(get_vpc_id('vpc1', self.vpcs), 'vpc1-id')

    def test_vpc_2_matches_returns_first(self):
        self.vpcs['Vpcs'].append(dict(self.vpcs['Vpcs'][0]))
        self.vpcs['Vpcs'][1]['VpcId'] = 'vpc1-id2'
        self.assertEqual(get_vpc_id('vpc1', self.vpcs), 'vpc1-id')

    # get_rdses
    def test_rds_0_returns_empty(self):
        self.assertEqual(get_rdses('vpc-id', {'DBInstances': []}), {'DBInstances': []})

    def test_rds_0_matches_returns_empty(self):
        expected = dict(self.rdses)
        expected['DBInstances'] = []
        self.assertEqual(get_rdses('vpc-id-does-not-exist', self.rdses), expected)

    def test_rds_1_match_returns_match(self):
        self.assertEqual(get_rdses('vpc-id', self.rdses), self.rdses)

    def test_rds_2_matches_returns_both(self):
        self.rdses['DBInstances'].append(self.rdses['DBInstances'][0])
        self.assertEqual(get_rdses('vpc-id', self.rdses), self.rdses)

    def test_rds_1_match_1_no_match_returns_match(self):
        expected = copy.deepcopy(self.rdses)
        self.rdses['DBInstances'].append(copy.deepcopy(self.rdses['DBInstances'][0]))
        self.rdses['DBInstances'][0]['DBSubnetGroup']['VpcId'] = 'vpc-id2'
        self.assertEqual(get_rdses('vpc-id', self.rdses), expected)

    # get_rds_ids
    def test_rdsids_0_returns_empty(self):
        self.assertEqual(get_rds_ids({'DBInstances': []}), [])

    def test_rdsids_2_returns_ids(self):
        self.rdses['DBInstances'].append(self.rdses['DBInstances'][0])
        self.assertEqual(get_rds_ids(self.rdses), ['rds-id', 'rds-id'])


if __name__ == '__main__':
    unittest.main()
