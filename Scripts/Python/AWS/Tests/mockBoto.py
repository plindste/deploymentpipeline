from datetime import datetime

default_stacks = {
    'Stacks': [
        {
            'StackId': 'string',
            'StackName': 'string',
            'ChangeSetId': 'string',
            'Description': 'string',
            'Parameters': [
                {
                    'ParameterKey': 'string',
                    'ParameterValue': 'string',
                    'UsePreviousValue': True
                },
            ],
            'CreationTime': datetime(2015, 1, 1),
            'DeletionTime': datetime(2015, 1, 1),
            'LastUpdatedTime': datetime(2015, 1, 1),
            'RollbackConfiguration': {
                'RollbackTriggers': [
                    {
                        'Arn': 'string',
                        'Type': 'string'
                    },
                ],
                'MonitoringTimeInMinutes': 123
            },
            'StackStatus': 'CREATE_COMPLETE',
            'StackStatusReason': 'string',
            'DisableRollback': False,
            'NotificationARNs': [
                'string',
            ],
            'TimeoutInMinutes': 123,
            'Capabilities': [
                'CAPABILITY_IAM',
            ],
            'Outputs': [
                {
                    'OutputKey': 'string',
                    'OutputValue': 'string',
                    'Description': 'string',
                    'ExportName': 'string'
                },
            ],
            'RoleARN': 'string',
            'Tags': [
                {
                    'Key': 'string',
                    'Value': 'string'
                },
            ],
            'EnableTerminationProtection': False,
            'ParentId': 'string',
            'RootId': 'string'
        },
    ],
    'NextToken': 'string'
}


class MockBoto(object):
    def __init__(self):
        self.mock_cf = None

    def client(self, type, region_name=None):
        if 'does_not_exist' == region_name:
            raise Exception
        if 'cloudformation' == type:
            self.mock_cf = MockCF(region_name)
            return self.mock_cf
        elif 'rds' == type:
            self.mock_rds = MockRDS(region_name)
            return self.mock_rds
        else:
            raise Exception('Unknown client type')


class MockCF(object):
    def __init__(self, region):
        self.region = region
        self.count = 0

    def describe_stacks(self, string):
        self.count += 1
        if 'does_not_exist' == string:
            raise Exception
        default_stacks['Stacks'][0]['StackStatus'] = 'CREATE_COMPLETE'
        return default_stacks

    def create_stack(self, StackName, TemplateURL, Parameters, DisableRollback,
                     TimeoutInMinutes, Capabilities):
        return {'StackId': 'stack-id'}

    def get_waiter(self, string):
        return Waiter()


class Waiter():
    def wait(self, StackName):
        pass

class MockRDS(object):
    def __init__(self, region):
        self.region = region

    def list_tags_for_resource(self, ResourceName):
        return {'ResponseMetadata': {'HTTPStatusCode': 200},
                'TagList': [{
                    'Key': 'da:Type',
                    'Value': 'Test'
                }]}
