import copy
import unittest

from datetime import datetime

import mockBoto
from Scripts.Python.AWS.snapshotRds import filter_snapshot_deletion_candidates, \
    fetch_tags_for_snapshots, select_for_deletion


class SnapshotTests(unittest.TestCase):

    def setUp(self):
        self.snapshots = {
            'Marker': 'string',
            'DBSnapshots': [
                {
                    'DBSnapshotIdentifier': 'tsctest-7-13-0-m1-sadf7432fd',
                    'DBInstanceIdentifier': 'sadf7432fd',
                    'SnapshotCreateTime': datetime(2015, 1, 1),
                    'Engine': 'oracle-se1',
                    'AllocatedStorage': 123,
                    'Status': 'string',
                    'Port': 123,
                    'AvailabilityZone': 'string',
                    'VpcId': 'vpc-806824e6',
                    'InstanceCreateTime': datetime(2015, 1, 1),
                    'MasterUsername': 'string',
                    'EngineVersion': 'string',
                    'LicenseModel': 'string',
                    'SnapshotType': 'string',
                    'Iops': 123,
                    'OptionGroupName': 'string',
                    'PercentProgress': 123,
                    'SourceRegion': 'string',
                    'SourceDBSnapshotIdentifier': 'string',
                    'StorageType': 'string',
                    'TdeCredentialArn': 'string',
                    'Encrypted': False,
                    'KmsKeyId': 'string',
                    'DBSnapshotArn': 'string',
                    'Timezone': 'string',
                    'IAMDatabaseAuthenticationEnabled': False
                },
            ]
        }
        self.tags = {
            'ResponseMetadata': {
                'RetryAttempts': 0,
                'HTTPStatusCode': 200,
                'RequestId': '0c3d39f2-ea77-46a6-b17f-f1e984e892ed',
                'HTTPHeaders': {
                    'x-amzn-requestid': '0c3d39f2-ea77-46a6-b17f-f1e984e892ed',
                    'date': 'Fri, 12 Jan 2018 13:43:13 GMT',
                    'content-length': '387',
                    'content-type': 'text / xml'}},
            'TagList': [
                {'Value': 'Test', 'Key': 'da:Type'}
            ]
        }
        self.rds = mockBoto.MockBoto().client('rds')
        self.tagged_id = [{
            'DBSnapshotIdentifier': 'a',
            'TagList': [{
                'Key': 'Rawr',
                'Value': 'Wat'
            }, {
                'Key': 'da:Type',
                'Value': 'Test'
            }]}]

        # Tests for listing snapshots to delete

    def test_list_empty_returns_empty(self):
        del self.snapshots['DBSnapshots'][0]
        self.assertEquals(
            filter_snapshot_deletion_candidates("tscdev", self.snapshots), [])

    def test_list_nomatch_returns_empty(self):
        self.assertEquals(
            filter_snapshot_deletion_candidates("tscdev", self.snapshots), [])

    def test_list_1match_returns_1_id(self):
        self.snapshots['DBSnapshots'].append(
            copy.deepcopy(self.snapshots['DBSnapshots'][0]))
        self.snapshots['DBSnapshots'][1]['DBSnapshotIdentifier'] = \
            'tscdev-7-13-0-m1-sadf7432fd'
        self.assertEquals(
            filter_snapshot_deletion_candidates("tscdev", self.snapshots),
            ['tscdev-7-13-0-m1-sadf7432fd'])

    def test_list_matches_vpc_only_at_start_of_id(self):
        self.snapshots['DBSnapshots'].append(
            copy.deepcopy(self.snapshots['DBSnapshots'][0]))
        self.snapshots['DBSnapshots'][1]['DBSnapshotIdentifier'] = \
            'tscdev-7-13-0-m1-tsctest'
        self.assertEquals(
            filter_snapshot_deletion_candidates('tsctest', self.snapshots),
            ['tsctest-7-13-0-m1-sadf7432fd'])

    def test_tags_empty_returns_empty(self):
        self.assertEquals(
            fetch_tags_for_snapshots('1', [], 'us-west-2', self.rds), [])

    def test_tags_match_returns_tags(self):
        del self.tagged_id[0]['TagList'][0]
        self.assertEquals(
            fetch_tags_for_snapshots('1', ['a'], 'us-west-2', self.rds),
            self.tagged_id)

    def test_select_empty_returns_empty(self):
        self.assertEquals(select_for_deletion([]), [])

    def test_select_match_returns_match(self):
        self.assertEquals(
            select_for_deletion(self.tagged_id), ['a'])

    def test_select_mixed_returns_match(self):
        self.tagged_id.append(copy.deepcopy(self.tagged_id[0]))
        self.tagged_id[1]['DBSnapshotIdentifier'] = 'b'
        self.tagged_id[1]['TagList'][1]['Value'] = 'Production'
        self.assertEquals(
            select_for_deletion(self.tagged_id), ['a'])

    def test_select_nomatch_returns_empty(self):
        del self.tagged_id[0]['TagList'][1]
        self.assertEquals(select_for_deletion(self.tagged_id), [])


if __name__ == '__main__':
    unittest.main()
