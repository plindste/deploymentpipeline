import unittest
from datetime import datetime

from Scripts.Python.AWS.deployActions import validate_stack_state, determine_ordinal, InvalidStackState, determine_actions, \
    check_network_stack, check_in_progress


class DeployActionsTests(unittest.TestCase):
    def setUp(self):
        self.stacks = {
            'Stacks': [
                {
                    'StackId': 'string',
                    'StackName': '1',
                    'ChangeSetId': 'string',
                    'Description': 'string',
                    'Parameters': [
                        {
                            'ParameterKey': 'string',
                            'ParameterValue': 'string',
                            'UsePreviousValue': False
                        },
                    ],
                    'CreationTime': datetime(2015, 1, 1),
                    'DeletionTime': datetime(2015, 1, 1),
                    'LastUpdatedTime': datetime(2015, 1, 1),
                    'RollbackConfiguration': {
                        'RollbackTriggers': [
                            {
                                'Arn': 'string',
                                'Type': 'string'
                            },
                        ],
                        'MonitoringTimeInMinutes': 123
                    },
                    'StackStatus': 'CREATE_IN_PROGRESS',
                    'StackStatusReason': 'string',
                    'DisableRollback': False,
                    'NotificationARNs': [
                        'string',
                    ],
                    'TimeoutInMinutes': 123,
                    'Capabilities': [
                        'CAPABILITY_IAM',
                    ],
                    'Outputs': [
                        {
                            'OutputKey': 'string',
                            'OutputValue': 'string',
                            'Description': 'string',
                            'ExportName': 'string'
                        },
                    ],
                    'RoleARN': 'string',
                    'Tags': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ],
                    'EnableTerminationProtection': False,
                    'ParentId': 'string',
                    'RootId': 'string'
                },
                {
                    'StackId': 'string',
                    'StackName': '2',
                    'ChangeSetId': 'string',
                    'Description': 'string',
                    'Parameters': [
                        {
                            'ParameterKey': 'string',
                            'ParameterValue': 'string',
                            'UsePreviousValue': False
                        },
                    ],
                    'CreationTime': datetime(2015, 1, 1),
                    'DeletionTime': datetime(2015, 1, 1),
                    'LastUpdatedTime': datetime(2015, 1, 1),
                    'RollbackConfiguration': {
                        'RollbackTriggers': [
                            {
                                'Arn': 'string',
                                'Type': 'string'
                            },
                        ],
                        'MonitoringTimeInMinutes': 123
                    },
                    'StackStatus': 'CREATE_IN_PROGRESS',
                    'StackStatusReason': 'string',
                    'DisableRollback': False,
                    'NotificationARNs': [
                        'string',
                    ],
                    'TimeoutInMinutes': 123,
                    'Capabilities': [
                        'CAPABILITY_IAM',
                    ],
                    'Outputs': [
                        {
                            'OutputKey': 'string',
                            'OutputValue': 'string',
                            'Description': 'string',
                            'ExportName': 'string'
                        },
                    ],
                    'RoleARN': 'string',
                    'Tags': [
                        {
                            'Key': 'string',
                            'Value': 'string'
                        },
                    ],
                    'EnableTerminationProtection': False,
                    'ParentId': 'string',
                    'RootId': 'string'
                },
            ],
            'NextToken': 'string'
        }

    def test_4_stacks_are_invalid(self):
        with self.assertRaises(InvalidStackState):
            validate_stack_state([0, 0, 0, 0])

    def test_3_rds_stacks_are_invalid(self):
        with self.assertRaises(InvalidStackState):
            validate_stack_state(["tscdev-rds1", "tscdev-rds12", "tscdev-rds123"])

    def test_2_app_stacks_are_invalid(self):
        with self.assertRaises(InvalidStackState):
            validate_stack_state(["tscdev-app1", "tscdev-app1"])

    def test_1_app_2_rds_stacks_are_invalid(self):
        with self.assertRaises(InvalidStackState):
            validate_stack_state(["tscdev-app1", "tscdev-rds3", "tscdev-rds1"])

    def test_1_app_1_rds_stacks_are_valid(self):
        validate_stack_state(["tscdev-app1", "tscdev-rds1"])

    def test_0_stacks_are_valid(self):
        validate_stack_state([])

    def test_0_app_1_rds_stacks_are_valid(self):
        validate_stack_state(["tscdev-rds2"])

    def test_2_rds_stacks_are_invalid(self):
        with self.assertRaises(InvalidStackState):
            validate_stack_state(["tscdev-rds1", "tscdev-rds2"])

    def test_1_app_0_rds_stacks_are_invalid(self):
        with self.assertRaises(InvalidStackState):
            validate_stack_state(["tscdev-app1"])

    def test_ordinal_3_is_invalid(self):
        with self.assertRaises(InvalidStackState):
            determine_ordinal(["tscdev-rds1", "tscdev-rds2", "tscdev-app3"])

    def test_lack_of_ordinal_is_invalid(self):
        with self.assertRaises(InvalidStackState):
            determine_ordinal(["tscdev"])

    def test_app1_returns_2(self):
        self.assertEqual(determine_ordinal(["tscdev-app1"]), 2)

    def test_app2_returns_1(self):
        self.assertEqual(determine_ordinal(["tscdev-app2"]), 1)

    def test_app1_rds2_returns_2(self):
        self.assertEqual(determine_ordinal(["tscdev-app1", "tscdev-rds2"]), 2)

    def test_app2_rds1_rds2_returns_1(self):
        self.assertEqual(determine_ordinal(["tscdev-app2", "tscdev-rds2", "tscdev-rds1"]), 1)

    def test_empty_return_1(self):
        self.assertEqual(determine_ordinal([]), 1)

    def test_rds1_return_1(self):
        self.assertEqual(determine_ordinal(["tscdev-rds1"]), 1)

    def test_empty_stacks_returns_createRds(self):
        self.assertEqual(determine_actions([], "createRds", "deleteApp"), {"createRds"})

    def test_rdsStack_returns_empty(self):
        self.assertEqual(determine_actions(["tscdev-rds1"], "createRds", "deleteApp"), set())

    def test_app_returns_deleteApp_createRds(self):
        self.assertEqual(determine_actions(["tscdev-app1"], "createRds", "deleteApp"),
                         {"createRds", "deleteApp"})

    def test_1rds_1app_returns_deleteApp(self):
        self.assertEqual(
            determine_actions(["tscdev-app1", "tscdev-rds2"], "createRds", "deleteApp"), {"deleteApp"})

    def test_missing_network_stack_is_invalid(self):
        with self.assertRaises(InvalidStackState):
            check_network_stack("tscdev", [])

    def test_0_in_progress_does_not_raise(self):
        check_in_progress([])

    def test_1_in_progress_raises(self):
        with self.assertRaises(InvalidStackState):
            check_in_progress(self.stacks['Stacks'])


if __name__ == '__main__':
    unittest.main()
