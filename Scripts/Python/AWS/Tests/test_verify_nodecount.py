import copy
import unittest

from Scripts.Python.AWS.verify_nodecount import get_oauth, InvalidData, \
    extract_desired, verify_nodecount, InvalidState


class GetOauthCheckStatusTest(unittest.TestCase):
    static_testdata = None

    @classmethod
    def setUpClass(cls):
        with open('Scripts/Python/AWS/Tests/oauth.testdata') as f:
            GetOauthCheckStatusTest.static_testdata = f.readlines()

    def setUp(self):
        self.testdata = copy.copy(GetOauthCheckStatusTest.static_testdata)

    def test_get_oauth_empty_raises(self):
        with self.assertRaises(InvalidData):
            get_oauth([])

    def test_get_oauth_returns_clientid_and_secret(self):
        self.assertEqual(get_oauth(self.testdata),
                         {
                             'client_id': '3ed04af164bdfec14faae20878123da1.oauth-clients.spotfire.tibco.com',
                             'client_secret': '628ac98a0d130da01680ca837937f75de9ba8d09b6b2d59633adac4bc1d0e0c1'})

    def test_get_oauth_no_match_raises(self):
        self.testdata = self.testdata[:-2]
        with self.assertRaises(InvalidData):
            get_oauth(self.testdata)

    def test_get_oauth_partial_match_raises(self):
        self.testdata = self.testdata[:-1]
        with self.assertRaises(InvalidData):
            get_oauth(self.testdata)

    def test_extract_desired_returns_0_on_none(self):
        self.assertEqual(extract_desired(None), 0)

    def test_extract_desired_returns_0_on_empty(self):
        self.assertEqual(extract_desired(''), 0)

    def test_extract_desired_returns_1_on_161(self):
        self.assertEqual(extract_desired('1,6,1'), 1)

    def test_extract_desired_returns_1_on_161space(self):
        self.assertEqual(extract_desired('1,6,1 '), 1)

    def test_extract_desired_raises_on_invalid_format(self):
        with self.assertRaises(InvalidData):
            extract_desired('a,z')

    def test_verify_nodecount_returns_true_on_identical(self):
        self.assertEqual(verify_nodecount({'TSS': 1}, {'TSS': 1}), True)

    def test_verify_nodecount_returns_true_on_differing_order(self):
        self.assertEqual(verify_nodecount({'TSS': 1, 'WEB_PLAYER': 2},
                                          {'WEB_PLAYER': 2, 'TSS': 1}), True)

    def test_verify_nodecount_returns_true_on_0_count_missing(self):
        self.assertEqual(verify_nodecount({'WEB_PLAYER': 0}, {}), True)

    def test_verify_nodecount_returns_true_on_0_count_and_other_matches(self):
        self.assertEqual(verify_nodecount({'TSS': 1, 'WEB_PLAYER': 0},
                                          {'TSS': 1}), True)

    def test_verify_nodecount_raises_on_1_count_missing(self):
        with self.assertRaises(InvalidState):
            verify_nodecount({'TSS': 1}, {})

    def test_verify_nodecount_raises_on_too_few_nodes(self):
        with self.assertRaises(InvalidState):
            verify_nodecount({'TSS': 2}, {'TSS': 1})

    def test_verify_nodecount_raises_on_too_many_nodes(self):
        with self.assertRaises(InvalidState):
            verify_nodecount({'TSS': 1}, {'TSS': 2})

    def test_verify_nodecount_raises_on_0_count_missing_and_too_few(self):
        with self.assertRaises(InvalidState):
            verify_nodecount({'TSS': 1, 'DATA_ACCESS': 0}, {'TSS': 0})


if __name__ == '__main__':
    unittest.main()
