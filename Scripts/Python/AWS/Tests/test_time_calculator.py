import unittest

import datetime

from Scripts.Python.AWS.timeCalculator import calculate_time_until


class TimeCalculatorTests(unittest.TestCase):

    def test_NOW_returns_60(self):
        self.assertEqual(calculate_time_until('NOW'), 60)

    def test_full_iso8601_returns_proper(self):
        self.assertEqual(calculate_time_until('1970-01-01T01:00:00+00:00', datetime.datetime(1970, 1, 1)), 3600)

    def test_negative_tz_returns_proper(self):
        self.assertEqual(calculate_time_until('1970-01-01T01:00:00-01:01', datetime.datetime(1970, 1, 1)), -60)

    def test_IST_returns_proper(self):
        self.assertEqual(calculate_time_until('1970-01-01T01:00:00+04:30', datetime.datetime(1970, 1, 1)), int(3600*5.5))

if __name__ == '__main__':
    unittest.main()
