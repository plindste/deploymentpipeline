import argparse
import sys

import boto3

from awsHelpers import get_stacks


def main(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("--vpc", help="Name of the vpc to clean rollbacked stacks from")
    parser.add_argument("--region", help="Name of the region to search in")

    parsed = parser.parse_args(args)

    region = parsed.region
    vpc = parsed.vpc

    cf = boto3.client('cloudformation', region_name=region)
    allStacks = cf.describe_stacks()
    stacks = get_stacks(vpc, allStacks)
    to_be_deleted = stacks_to_delete(stacks)
    deleteStacks(to_be_deleted, cf)


def stacks_to_delete(stacks):
    return map(lambda x: x['StackName'], filter(lambda x: x['StackStatus'] == 'ROLLBACK_COMPLETE', stacks['Stacks']))


def deleteStacks(to_be_deleted, cf):
    for stack_name in to_be_deleted:
        cf.delete_stack(StackName=stack_name)


if __name__ == '__main__':
    main(sys.argv[1:])
