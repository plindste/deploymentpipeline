import argparse
import sys
import re

import requests
import logging
import boto3
import Scripts.Python.current_status as current_status
from Scripts.Python import library

log = logging.getLogger(__name__)


class InvalidData(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class InvalidState(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


# TODO: Implement edge node detection
# TODO: Perhaps accept counts between desired and max
def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--vpc',
                        help='The environment to check on. Example: tscdev',
                        required=True)
    parser.add_argument('--region',
                        help='The aws region to look in. Example: us-west-2',
                        required=True)
    parser.add_argument('--cache-file',
                        help='The file to cache the oauth creds in. Example: c.txt',
                        required=True)
    parser.add_argument('--sut-url',
                        help='URL for SUT. Example: https://tscdev.spotfirecloud.com',
                        required=True)
    parser.add_argument('--TSS',
                        help='MIN/MAX/DESIRED for TSS. Example: 0,6,1')
    parser.add_argument('--TSSA',
                        help='MIN/MAX/DESIRED for TSSA. Example: 0,6,1')
    parser.add_argument('--EDGE',
                        help='MIN/MAX/DESIRED for EDGE. Example: 0,6,1')
    parser.add_argument('--WP', help='MIN/MAX/DESIRED for WP. Example: 0,6,1')
    parser.add_argument('--DA', help='MIN/MAX/DESIRED for DA. Example: 0,6,1')
    parser.add_argument('--ST', help='MIN/MAX/DESIRED for ST. Example: 0,6,1')
    parser.add_argument('--AS', help='MIN/MAX/DESIRED for AS. Example: 0,6,1')
    args = parser.parse_args(argv)

    desired = {'TSS': extract_desired(args.TSS) + extract_desired(args.TSSA),
               'WEB_PLAYER': extract_desired(args.WP),
               'DATA_ACCESS': extract_desired(args.DA),
               'TERR': extract_desired(args.ST),
               'AUTOMATION_SERVICES': extract_desired(args.AS)}

    content = ""
    try:
        s3 = boto3.client('s3', region_name=args.region)
        f = s3.get_object(Bucket='tscx-{}'.format(args.vpc),
                          Key='oauth.txt')['Body']
        content = f.read()
        content = content.split('\r\n')
        f.close()
        with open(args.cache_file, mode='w') as f2:
            for line in content:
                f2.write("{}\n".format(line))

    except Exception, e:
        log.critical('Failed to get oauth from s3: {}'.format(str(e)))
        exit(1)

    oauths = get_oauth(content)
    log.debug("Oauths: {}".format(oauths))

    try:
        nodes = current_status.fetch_node_count(
            {'user': oauths['client_id'], 'password': oauths['client_secret'],
             'oauth': True, 'server': args.sut_url})

    except current_status.HTTPstatusError, e:
        log.error("Invalid HTTP Status code! Expected 200 but received {}"
                  .format(e.code))
        exit(3)

    except requests.exceptions.Timeout:
        log.exception("Request timed out! Is tss available?\n")
        exit(4)

    except requests.exceptions.ConnectionError:
        log.exception("A networking error was encountered!\n")
        exit(5)

    except Exception, e:
        log.error("Exception encountered: {}".format(e))
        exit(6)

    try:
        verify_nodecount(desired, nodes)
    except InvalidState, e:
        log.error(e)
        exit(7)

    exit(0)


def get_oauth(data):
    ret = {}
    try:
        client_id_line = get_single_matching(data, 'Client ID: ')
        ret['client_id'] = extract_after_colon(client_id_line)
        client_secret_line = get_single_matching(data, 'Client secret: ')
        ret['client_secret'] = extract_after_colon(client_secret_line)
    except InvalidData:
        raise InvalidData('Could not find oauth credentials in: ' + str(data))
    return ret


def get_single_matching(list, string):
    matches = filter(lambda x: string in x, list)
    if matches:
        return matches.pop()
    raise InvalidData('No match')


def extract_after_colon(string):
    match = re.search(".*: (.*)$", string)
    if match:
        return match.group(1).strip()
    raise InvalidData('No match')


def extract_desired(min_max_desired):
    if not min_max_desired:
        return 0
    min_max_desired = min_max_desired.strip()
    match = re.search("^\d+,\d+,(\d+)$", min_max_desired)
    if match:
        return int(match.group(1))
    raise InvalidData('No match')


def verify_nodecount(expected, actual):
    if expected == actual:
        return True
    added, removed, modified, same = library.dict_compare(expected, actual)
    if added and not removed and not modified:
        for r in added:
            if not expected[r] == 0:
                raise InvalidState(
                    'Nonzero nodecount was missing from target state,'
                    ' expected: {}, actual: {}'.format(expected, actual))
        return True

    raise InvalidState('Nodecount does not match target state,'
                       ' expected: {}, actual: {}'.format(expected, actual))


if __name__ == '__main__':
    log.setLevel(logging.DEBUG)
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    log.addHandler(stdout_handler)
    log.propagate = False
    main(sys.argv[1:])
