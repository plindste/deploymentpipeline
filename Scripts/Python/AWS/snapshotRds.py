import argparse

import boto3
import botocore.exceptions
import logging

import sys

import awsHelpers

log = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--vpc",
                        help="The vpc to snapshot the rds stack from",
                        required=True)
    parser.add_argument("--region",
                        help="The AWS region to connect to",
                        required=True)
    parser.add_argument("--manifest",
                        help="The manifest number. Example: M97",
                        required=True)
    parser.add_argument("--product-version",
                        help="The prodcut version number. Example: 7.12.0",
                        required=True)

    args = parser.parse_args()

    rds = boto3.client('rds', region_name=args.region)
    ec2 = boto3.client('ec2', region_name=args.region)
    vpc_object = ec2.describe_vpcs(Filters=[{'Name': 'tag:Name',
                                             'Values': [args.vpc]}])
    env_type = map(lambda x: x['Value'],
                   filter(lambda x: x['Key'] == 'Environment Type',
                          vpc_object['Vpcs'][0]['Tags']))
    vpc_id = awsHelpers.get_vpc_id(args.vpc, vpc_object)

    log.debug('VPC ID: {}'.format(vpc_id))
    vpc_rdses = awsHelpers.get_rdses(vpc_id, rds.describe_db_instances())

    log.debug('RDS Instances: {}'.format(vpc_rdses))
    rds_ids = awsHelpers.get_rds_ids(vpc_rdses)
    if len(rds_ids) != 1:
        log.fatal('Wrong number of RDS instances: {}. Bailing'.format(rds_ids))
        exit(1)

    log.debug('Deleting da:Type = Test snapshots')
    snapshots = fetch_all_snapshots(rds)
    possible_ids = filter_snapshot_deletion_candidates(args.vpc, snapshots)
    account_no = boto3.client('sts').get_caller_identity()['Account']
    tagged_snapshots = fetch_tags_for_snapshots(account_no,
                                                possible_ids, args.region, rds)
    snapshots_to_delete = select_for_deletion(tagged_snapshots)
    for snapshot in snapshots_to_delete:
        rds.delete_db_snapshot(DBSnapshotIdentifier=snapshot)

    snapshot_id = "{}-{}-{}-{}".format(args.vpc,
                                       args.product_version.replace('.', '-'),
                                       args.manifest, rds_ids[0])

    log.info('Creating snapshot')
    log.debug(rds.create_db_snapshot(DBSnapshotIdentifier=snapshot_id,
                                     DBInstanceIdentifier=rds_ids[0],
                                     Tags=[{'Key': 'da:Type',
                                            'Value': env_type.pop()},
                                           {'Key': 'da:Vpc',
                                            'Value': args.vpc}]))
    snapshot_waiter = rds.get_waiter('db_snapshot_completed')
    snapshot_waiter.wait(DBSnapshotIdentifier=snapshot_id)

    log.info('Done creating snapshot')


def fetch_all_snapshots(rds):
    paginator = rds.get_paginator('describe_db_snapshots')
    page_iterator = paginator.paginate()
    ret = {'DBSnapshots': []}
    for page in page_iterator:
        ret['DBSnapshots'].extend(page['DBSnapshots'])
    return ret


def filter_snapshot_deletion_candidates(vpc, snapshots):
    return map(lambda x: x['DBSnapshotIdentifier'],
               filter(lambda x: x['DBSnapshotIdentifier'].startswith(vpc),
                      snapshots['DBSnapshots']))


def fetch_tags_for_snapshots(account_no, possible_ids, region, rds):
    return map(lambda id: {
        'DBSnapshotIdentifier': id,
        'TagList': rds.list_tags_for_resource(
            ResourceName='arn:aws:rds:{}:{}:snapshot:{}'
                .format(region, account_no, id))['TagList']
    }, possible_ids)


def select_for_deletion(tagged_ids):
    return map(lambda snapshot: snapshot['DBSnapshotIdentifier'],
               filter(lambda x: 'Test' == get_tag_value(x, 'da:Type'),
                      tagged_ids))


def get_tag_value(item, tag):
    ret = get_tag(item, tag)
    if ret:
        return ret['Value']
    return None


def get_tag(item, tag_key):
    ret = filter(lambda tag: tag['Key'] == tag_key, item['TagList'])
    if ret:
        return ret.pop()
    return None


def delete_snapshot(rds, snapshot_id):
    try:
        rds.delete_db_snapshot(DBSnapshotIdentifier=snapshot_id)
        log.info('Snapshot deleted')
    except botocore.exceptions.ClientError, e:
        if e.response['Error']['Code'] == 'DBSnapshotNotFound':
            log.info('Snapshot not found')
        else:
            log.fatal('Other error: {}'.format(e.response))
            exit(1)


if __name__ == '__main__':
    log.setLevel(logging.INFO)
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.INFO)
    stdout_handler.setFormatter(
        logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    log.addHandler(stdout_handler)
    main()
