import boto3
import sys
import argparse
import logging

logger = logging.getLogger(__name__)


class InvalidStackState(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def validate_state_determine_ordinal(addOnStacks):
    validate_stack_state(addOnStacks)
    return determine_ordinal(addOnStacks)


def get_addon_stacks(baseStack, region):
    client = boto3.client('cloudformation', region_name=region)
    stacks = client.describe_stacks().get('Stacks')
    check_network_stack(baseStack, stacks)
    addOnStacks = filter(lambda x: baseStack + '-' in x.get('StackName'), stacks)
    check_in_progress(addOnStacks)
    addOnStackNames = map(lambda x: x.get('StackName'), addOnStacks)
    return addOnStackNames


def check_network_stack(baseStack, stacks):
    if len(filter(lambda x: baseStack in x.get('StackName'), stacks)) < 1:
        raise InvalidStackState('Network stack does not exist')


def check_in_progress(stacks):
    count = len(filter(lambda x: 'PROGRESS' in x['StackStatus'], stacks))
    if count > 0:
        raise InvalidStackState('{} stacks are in progress, will not proceed'.format(count))


def rds_count(addOnStacks):
    return len(filter(lambda x: '-rds' in x, addOnStacks))


def app_count(addOnStacks):
    return len(filter(lambda x: '-app' in x, addOnStacks))


def determine_actions(addOnStacks, createRds, deleteApp):
    ret = set()
    if rds_count(addOnStacks) < 1:
        ret.add(createRds)
    if app_count(addOnStacks) == 1:
        ret.add(deleteApp)
    return ret


def validate_stack_state(addOnStacks):
    if len(addOnStacks) > 3:
        raise InvalidStackState('Too many add-on stacks')
    if app_count(addOnStacks) > 1:
        raise InvalidStackState('Too many app stacks')
    if rds_count(addOnStacks) > 1:
        raise InvalidStackState('Too many rds stacks')
    if app_count(addOnStacks) > 0 and rds_count(addOnStacks) < 1:
        raise InvalidStackState('No RDS stacks, but APP stacks present')


def validate_ordinal(ordinal):
    return ordinal == "1" or ordinal == "2"


def determine_ordinal(addOnStacks):
    ordinals = set(map(lambda x: x[-1:], addOnStacks))
    if not (reduce(lambda x, y: validate_ordinal(y) and x, ordinals, True)):
        raise InvalidStackState('Invalid ordinals deployed')

    if not ordinals:
        nextOrdinal = 1
    else:
        if app_count(addOnStacks) == 0:
            stack = filter(lambda x: "-rds" in x, addOnStacks).pop()
            nextOrdinal = int(stack[-1:])
        else:
            stack = filter(lambda x: "-app" in x, addOnStacks).pop()
            currentOrdinal = stack[-1:]
            if currentOrdinal == "1":
                nextOrdinal = 2
            else:
                nextOrdinal = 1
    return nextOrdinal


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--stack', help='Name of the Network Stack. Ex: tscdev')
    parser.add_argument('--path',
                        help='Path including filename to desired output. Ex: /var/lib/jenkins/workspace/job@tmp/output')
    parser.add_argument('--region', help='AWS Region to search in')

    args = parser.parse_args()
    netStack = args.stack
    path = args.path
    region = args.region

    try:
        addOnStacks = get_addon_stacks(netStack, region)
        ordinal = validate_state_determine_ordinal(addOnStacks)
        actions = determine_actions(addOnStacks, "createRds", "deleteApp")

        with open(path, 'w') as f:
            f.write(str(ordinal) + '\n')
            f.write('\n'.join(actions))

    except Exception as e:
        print e
        sys.exit(1)
    sys.exit(0)


if __name__ == "__main__":
    main()
