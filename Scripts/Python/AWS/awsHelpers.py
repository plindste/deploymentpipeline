def get_stacks(vpc, all_stacks):
    return {'Stacks': filter(lambda x: vpc in x['StackName'], all_stacks['Stacks'])}


def get_addon_stacks(stacks):
    return {
        'Stacks': filter(lambda x: filter(lambda y: "PARENTSTACK" in y['OutputKey'], x['Outputs']), stacks['Stacks'])}


def get_stack_names(stacks):
    return map(lambda x: x['StackName'], stacks['Stacks'])


def filter_wait_dict(wait_dict):
    return {x: wait_dict[x] for x in wait_dict if wait_dict[x] == "DELETE_IN_PROGRESS"}


def get_stack_ids(stacks):
    return map(lambda x: x['StackId'], stacks['Stacks'])


def get_rdses(vpc_id, rdses):
    ret = dict(rdses)
    ret['DBInstances'] = filter(lambda x: x['DBSubnetGroup']['VpcId'] == vpc_id, rdses['DBInstances'])
    return ret

def get_rds_ids(rdses):
    return map(lambda x: x['DBInstanceIdentifier'], rdses['DBInstances'])


def get_vpc_id(vpc, vpcs):
    raw_list = vpcs['Vpcs']
    filtered = filter(lambda x: filter(lambda y: y['Key'] == 'Name' and vpc == y['Value'], x['Tags']), raw_list)
    if (filtered):
        return filtered[0]['VpcId']
    return None
