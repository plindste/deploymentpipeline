import argparse
import datetime

import sys


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--time', help='The timestamp to parse. NOW (60 seconds) or YYYY-MM-DDTHH:mm:ss+00:00', required=True)
    parser.add_argument('--out', help='The file to write the output to', required=True)
    args = parser.parse_args(argv)

    seconds_left = calculate_time_until(args.time)

    with open(args.out, 'w') as f:
        f.write(str(seconds_left))


def calculate_time_until(timestamp, now=datetime.datetime.utcnow()):
    if timestamp == 'NOW':
        ret = 60
    else:
        if (timestamp[-3] == ':'):
            timestamp = rreplace(timestamp, ':', '', 1)
        if timestamp[-5] in '+-':
            tz_string = timestamp[-5:]
            timestamp = timestamp[:-5]
        hour_part = int(tz_string[:3])
        minute_part = int(tz_string[0] + tz_string[-2:])
        tz_delta = datetime.timedelta(hours=hour_part, minutes=minute_part)
        parsed_time = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')
        utc_time = parsed_time + tz_delta
        ret = (utc_time - now).total_seconds()

    return ret


def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)


if __name__ == '__main__':
    main(sys.argv[1:])
