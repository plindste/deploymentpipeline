import argparse
import time

import boto3
import awsHelpers


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--vpc", help="The vpc to clean up")
    parser.add_argument("--region", help="The region to use")
    args = parser.parse_args()

    cf = boto3.client('cloudformation', region_name=args.region)
    stacks = awsHelpers.get_stacks(args.vpc, cf.describe_stacks())
    addon_stacks = awsHelpers.get_addon_stacks(stacks)
    addon_stack_ids = awsHelpers.get_stack_ids(addon_stacks)

    map(lambda x: cf.delete_stack(StackName=x), addon_stack_ids)

    wait_dict = True

    while wait_dict:
        time.sleep(30)

        wait_dict = dict(
            map(lambda x: (x, cf.describe_stacks(StackName=x)['Stacks'][0]['StackStatus']), addon_stack_ids))

        print "Wait list: {}".format(wait_dict)

        wait_dict = awsHelpers.filter_wait_dict(wait_dict)


if __name__ == '__main__':
    main()
