import requests
import json
import time
import logging

log = logging.getLogger(__name__)

# The complete provisioning sequence looks like this:
# 1. Enable functionality on the device by uploading a signed license key from the manufacturer (vns3api.vns3_license_upload)
# 2. Specify the desired configuration for the enabled functionality (either build json on the fly or upload a pre-built file) (vns3api.vns3_config_license)
# 2a. Device reboot (vns3api.vns3_config_license waits for this to complete)
# 3. Generate a new encryption keyset on the device, or tell the device to retrieve one from an existing manager in the topology (vns3api.vns3_configure_keyset)
# 4. Make the new instance the master of the topology, or peer it with an already-existing master (vns3api.vns3_set_peering)
#
# Optionally:
# 5. Set firewall rules (vns3api.vns3_upload_firewall_rules to transfer a pre-built file)
# 6. Reset the password for the built-in "api" user (vns3api.vns3_set_api_password)
# 7. Set the credentials for the UI (vns3api.vns3_set_ui_credentials)


def vns3_wait_for_appliance_online(target_host, api_uid, api_pwd, wait_time, target_port=8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/config"

    waited = 0

    log.info("Waiting up to " + str(wait_time) + "s for device at " + target_host + " to come online (api port " + str(target_port) + ")...")

    while (waited <= wait_time):

        try:
            response = requests.get(url, auth=(api_uid, api_pwd), verify=False)
            log.info("HTTP response: " + str(response.status_code))
        except Exception, exc: # (requests.ConnectionError, requests.HTTPError):
            log.debug(exc.message)
            log.info("Device still initializing (API connection failed)...")

        else:

            message = response.json()

            if message.get("response"):
                log.info("Appliance is now online with configuration: " + json.dumps(message))
                break

        time.sleep(10)
        waited = waited + 10

    else:
        log.error("Still no response from device after " + str(waited) + "s wait")
        raise UserWarning("Still no response from device after " + str(waited) + "s wait")


# Uploads the contents of the specified license file to the VNS3 API
def vns3_license_upload(target_host, api_uid, api_pwd, license_file, target_port=8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/license"
    headers = {'content-type': 'text/plain'}
    license_data = open(license_file, 'r').read()

    response = requests.put(url, auth = (api_uid, api_pwd), data = license_data, headers = headers, verify = False)
    log.info(json.dumps(response.json()))
    response.raise_for_status()
    log.info("License upload complete.")


# Restores a configuration snapshot - NOT YET TESTED
#def vns3_import_snapshot(target_host, api_uid, api_pwd, snapshot_file, target_port=8000):
#
#    url = "https://" + target_host + ":" + str(target_port) + "/api/snapshots/running_config"
#    headers = {"content-type" : "application/octet-stream"}
#    files = {'file': open(snapshot_file, 'rb')}
#
#    response = requests.post(url, auth = (api_uid, api_pwd), headers = headers, files = files)
#    response.raise_for_status()
#    print("Snapshot import successful.")


# Uploads a json-formatted configuration file, and wait for device to finish configuration/reboot cycle
def vns3_config_license(target_host, api_uid, api_pwd, license_param_file, target_port=8000, timeout=300):

    url = "https://" + target_host + ":" + str(target_port) + "/api/license/parameters"
    headers = {'content-type': 'application/json'}
    license_params = open(license_param_file, 'r').read()

    response = requests.put(url, auth=(api_uid, api_pwd), headers=headers, data=license_params, verify=False)
    log.info(json.dumps(response.json()))
    response.raise_for_status()
    log.info("Configuration upload complete - waiting for reboot...")

    check_url = "https://" + target_host + ":" + str(target_port) + "/api/license"

    waited = 0

    while(waited <= timeout):
        time.sleep(5)
        waited = waited + 5

        try:
            response = requests.get(check_url, auth=(api_uid, api_pwd), verify=False)
        except: # (requests.ConnectionError, requests.HTTPError):
            log.debug("Device still rebooting (API connection failed)...")
            pass
        else:
            message = response.json()

            if message.get("response"):
                log.debug(json.dumps(message))
                break
            elif message.get("error"):
                log.debug("Shutdown still in progress - " + json.dumps(message["error"]))

    else:
        log.error("Still unable to re-establish contact after " + str(waited) + "s wait")
        raise UserWarning("Still unable to re-establish contact after " + str(waited) + "s wait")


# Queries the API for configuration information
def vns3_describe_config(target_host, api_uid, api_pwd, target_port=8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/config"
    response = requests.get(url, auth = (api_uid, api_pwd), verify = False)
    response.raise_for_status()
    return response.json()


# Queries the API for license parameters
def vns3_describe_license(target_host, api_uid, api_pwd, target_port=8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/license"
    response = requests.get(url, auth = (api_uid, api_pwd), verify = False)
    response.raise_for_status()
    return response.json()


# Either create a new topology name with the specified security token, or use the specified token to connect to an existing master and retrieve keys
def vns3_configure_keyset(target_host, api_uid, api_pwd, topology_token, topology_name=None, key_master=None, target_port=8000):

    if ((topology_name is None and key_master is None) or (topology_name is not None and key_master is not None)):
        raise UserWarning("Required: exactly one of topology_name or key_master")

    url = "https://" + target_host + ":" + str(target_port) + "/api/keyset"
    headers = {"content-type": "application/json"}
    keyset_json = '{"token":"' + topology_token + '",'

    if (topology_name is not None):
        keyset_json = keyset_json + '"topology_name":"' + topology_name + '"}'
    else:
        keyset_json = keyset_json + '"source":"' + key_master + '"}'

    response = requests.put(url, auth = (api_uid, api_pwd), headers = headers, data = keyset_json, verify = False)
    log.debug(json.dumps(response.json()))
    response.raise_for_status()
    log.info("Submitted key setup request.")

    waited = 0

    while (waited <= 300):
        time.sleep(5)
        waited = waited + 5
        response = requests.get(url, auth=(api_uid, api_pwd), verify=False)
        if (response.json()["response"]["keyset_present"]):
            log.debug(json.dumps(response.json()))
            break
    else:
        raise UserWarning("Didn't receive keygen confirmation after 5m wait: response " + json.dumps(response.json()))

    log.info("Keyset operation complete.")


# Grants this instance the specified peer number in the topology (#1 for a single master)
def vns3_set_peering(target_host, api_uid, api_pwd, mgr_ordinal, target_port=8000):

    headers = {'content-type': 'application/json'}
    peering_json = '{"id":"' + str(mgr_ordinal) + '"}'
    response = requests.put("https://" + target_host + ":" + str(target_port) + "/api/peering/self", auth=(api_uid, api_pwd), headers=headers, data=peering_json, verify=False)
    response.raise_for_status()
    log.info("Peering configured.")


# Uploads the contents of a file containing VNS3 firewall rules to the device
def vns3_upload_firewall_rules(target_host, api_uid, api_pwd, rules_file, target_port = 8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/firewall/rules"
    headers = {"content-type" : "application/json"}
    rules_list = open(rules_file, 'r')

    for rule in rules_list:
        rule_json = '{"rule":"' + rule.rstrip('\r\n') + '"}'
        response = requests.post(url, auth = (api_uid, api_pwd), headers = headers, data = rule_json, verify = False)
        response.raise_for_status()
        log.info("Added rule '" + rule.rstrip('\r\n') + "'...")

    log.info("Done.")


# Clears all firewall rules on the appliance
def vns3_purge_firewall_rules(target_host, api_uid, api_pwd, target_port = 8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/firewall/rules"

    log.info("Getting rules list...")
    response = requests.get(url, auth = (api_uid, api_pwd), verify = False)
    response.raise_for_status()

    log.info("Purging all rules...")
    for fw_rule in response.json()['response']:
        response = requests.delete(url + "/0", auth = (api_uid, api_pwd), verify = False)
        response.raise_for_status()
        log.info(".")
    log.info("Done.")


# Lists the current firewall rules set on the appliance
def vns3_list_firewall_rules(target_host, api_uid, api_pwd, target_port = 8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/firewall/rules"

    log.info("Getting rules list...")
    response = requests.get(url, auth = (api_uid, api_pwd), verify = False)
    response.raise_for_status()
    for fw_rule in response.json()['response']:
        log.info(json.dumps(fw_rule))


# Changes the username and/or password used to access the appliance UI console
def vns3_set_ui_credentials(target_host, api_uid, api_pwd, new_uid, new_pwd, target_port = 8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/admin_ui"
    headers = {"content-type" : "application/json"}
    req_json = '{"admin_username" : "' + new_uid + '", "admin_password" : "' + new_pwd + '", "enabled" : true}'

    response = requests.put(url, auth = (api_uid, api_pwd), headers = headers, data = req_json, verify = False)
    response.raise_for_status()
    log.debug(json.dumps(response.json()))


# Changes the password for the built-in "api" account
def vns3_set_api_password(target_host, api_uid, api_pwd, new_pwd, target_port = 8000):

    url = "https://" + target_host + ":" + str(target_port) + "/api/api_password"
    headers = {"content-type" : "application/json"}
    req_json = '{"password" : "' + new_pwd + '"}'

    response = requests.put(url, auth = (api_uid, api_pwd), headers = headers, data = req_json, verify = False)
    print(json.dumps(response.json()))
    response.raise_for_status()


def vns3_provision_new_instance(target_host, api_uid, api_pwd, license_file_path, cfg_file_path, target_topology_token, target_topology_name, manager_number, firewall_file_path, new_api_pwd, new_ui_uid, new_ui_pwd, timeout=300):

    log.info("Beginning provisioning of VNS3 instance at " + target_host + "...")

    #Run the various provisioning steps
    vns3_license_upload(target_host, api_uid, api_pwd, license_file_path)
    vns3_config_license(target_host, api_uid, api_pwd, cfg_file_path)
    vns3_configure_keyset(target_host, api_uid, api_pwd, target_topology_token, topology_name=target_topology_name)
    vns3_set_peering(target_host, api_uid, api_pwd, manager_number)
    vns3_purge_firewall_rules(target_host, api_uid, api_pwd)
    vns3_upload_firewall_rules(target_host, api_uid, api_pwd, firewall_file_path)

    vns3_set_api_password(target_host, api_uid, api_pwd, new_api_pwd)
    api_pwd = new_api_pwd
    vns3_set_ui_credentials(target_host, api_uid, api_pwd, new_ui_uid, new_ui_pwd)

    log.info("Provisioning complete.")
