__author__ = 'TSCO'

import sys, os
import boto.cloudformation
import boto.ec2
import boto.ec2.elb
import boto.rds
import boto.vpc
import boto.route53.connection
import time
import argparse
import configureVnsInstance
import yaml
import logging

log = logging.getLogger(__name__)

global stack_id, stackName, stack_exists, tabooZones, stackAttrs, ec2Conn


def createStack(gitMountPoint, templateURL, cFstackName, region, templateParamList):
    sleep_time = 15
    stackName = cFstackName

    try:
        connCF = boto.cloudformation.connect_to_region(region)

    except Exception, e:

        print "Could not create the connection", sys.exc_info()[0]
        print "Boto Exception is : ", e
        raise

    if connCF is None:
        print "Region name parameter string incorrect"
        sys.exit()

    try:
        stack_exists = connCF.describe_stacks(stackName)

    except Exception, exc:

        # do nothing. Trap the exception to continue with the program.
        # This confirms the stack has not been created.
        pass

    try:

        ec2Conn = boto.ec2.connect_to_region(region)

    except Exception, e:

        print "Could not get an ec2 connection to region: ", region, sys.exc_info()[0]
        print "Boto Exception is : ", e
        raise

    if 'stack_exists' in locals():

        log.error("Stacks must have a unique name, please enter a different stack name.")
        raise RuntimeError("Environment %s already exists - cannot continue.", cFstackName)

    else:

        log.info("Starting build...")

        try:
            stack_id = connCF.create_stack(
                cFstackName,
                template_url=templateURL,
                parameters=templateParamList,
                notification_arns=[],
                disable_rollback=False,
                timeout_in_minutes=120,
                capabilities=["CAPABILITY_IAM"],
                tags=None
            )

        except Exception, e:

            log.error("Could not create a stack : ", region, sys.exc_info()[0])
            log.error("Boto Exception is : ", e)
            raise

    log.info("waiting for the stack to build...")

    if not wait_for_stack_ready(connCF, stack_id):
        raise RuntimeError("Cloud Formation stack creation failed - cannot continue.")
    else:
        log.info("Stack launch complete")
        return True


def configureVNS3Instances(region, cFstackName, gitMountPoint):
    ec2Conn = boto.ec2.connect_to_region(region)

    stackAttrs = getStackAttrs(region, cFstackName)

    for resource in stackAttrs:

        if ("VNS" in resource.logical_resource_id) and (resource.resource_type == 'AWS::EC2::Instance'):

            try:
                modAttSuccess = ec2Conn.modify_instance_attribute(instance_id=resource.physical_resource_id,
                                                                  attribute="sourceDestCheck", value="False")
                if modAttSuccess:
                    log.info(
                        "Source/Destination Check disabled for the VNS instance '" + resource.physical_resource_id + "'.")

            except Exception, e:

                print "Could not modify the source/Dest Check attribute", sys.exc_info()[0]
                print "Boto Exception is : ", e
                raise

            if wait_for_valid_instance(ec2Conn, resource.physical_resource_id, 300):
                ## get the public ip
                instanceObj = ec2Conn.get_all_instance_status([resource.physical_resource_id],
                                                              filters={'instance-state-name': 'running'})
                publicIp = ec2Conn.get_all_addresses(filters={'instance-id': instanceObj[0].id})
                configureVnsInstance.configureVnsInstance(resource.physical_resource_id, publicIp[0].public_ip, region,
                                                          gitMountPoint)


def getStackAttrs(region, stack2Check):
    """

    :rtype : Dictionary
    """
    try:
        connCF = boto.cloudformation.connect_to_region(region)

    except Exception, exc:
        print ("Could not create the Connection ", exc.message)
        raise

    try:
        stackResourceDict = {}

        stackResourceDict = connCF.list_stack_resources(stack_name_or_id=stack2Check)
    except:
        print ("Could not find stack ", stack2Check)
        raise sys.exit()

    return stackResourceDict


"""
  Get a boto.vpc.VPCConnection type
"""


def getVPCConnection(region):
    connVPC = None
    try:
        regionInfoObj = boto.ec2.get_region(region)

        connVPC = boto.vpc.VPCConnection(region=regionInfoObj)

    except Exception, exc:
        log.critical("Could not create the VPC Connection ", exc.message)
        raise

    return connVPC


"""
 Return the id from a VPC name
"""


def getVPCid(region, vpcName):
    """

    :rtype : List
    """

    vpcIdList = []

    connHandle = getVPCConnection(region)
    try:

        vpcIdList = connHandle.get_all_vpcs(filters={"tag-key": "Name", "tag-value": vpcName})
    except:
        log.critical("Could not retrieve VPC list.")
        raise RuntimeError("Unable to retrieve VPC list.")

    if len(vpcIdList) < 1:
        log.critical("VPC " + vpcName + " not found in region " + region)
        raise RuntimeError("No VPC by that name in selected region", vpcName, region)

    return vpcIdList[0].id


def getVPCSubnets(regionName, vpcName):
    vpcId = None
    vpcSubNets = None
    vpcIdList = []

    try:

        vid = getVPCid(regionName, vpcName)

        connHandle = getVPCConnection(regionName)
        vpcSubNets = connHandle.get_all_subnets(filters={"vpcId": vid})

    except Exception, e:
        print ("Could not find subnets for VPC : ", vpcName)
        print "Boto Exception is : ", e
        raise

    return vpcSubNets


def getVPCTags(regionName, vpcName):
    vpcId = None
    vpcSubNets = None
    vpcIdList = []

    try:

        vid = getVPCid(regionName, vpcName)

        connHandle = getVPCConnection(regionName)
        vpcObj = connHandle.get_all_vpcs(vpc_ids=[vid])

    except Exception, exc:
        log.critical("Could not find VPC object : ", vpcName)
        log.critical("Boto Exception is : ", exc.message)
        raise

    return vpcObj[0].tags


def makeReadableStackEvent(stackEvent):
    result = "Resource " + stackEvent.logical_resource_id + " (" + stackEvent.resource_type + "): " + stackEvent.resource_status
    if stackEvent.resource_status_reason is not None:
        result += " - " + stackEvent.resource_status_reason
    return result


def wait_for_stack_ready(cfConn, stackName, timeout=0):
    try:
        stackList = cfConn.describe_stacks(stackName)
    except Exception, exc:
        raise RuntimeError(
            "Unable to retrieve any information for stack " + stackName + " (" + exc.message + ").  Cannot continue.")

    cfStack = stackList[0]

    if timeout > 0:
        deadline = time.time() + timeout
    else:
        deadline = None

    buildComplete = False
    pastEvents = cfStack.describe_events()
    if len(pastEvents) > 1:
        pastEvents = set(map(makeReadableStackEvent, pastEvents))
        log.debug("Skipping {e} existing events.".format(e=len(pastEvents) - 1))
    else:
        pastEvents = set()

    while (not buildComplete and ((time.time() < deadline) or (deadline == None))):

        stackStatus = cfStack.stack_status

        eventList = set(map(makeReadableStackEvent, cfStack.describe_events()))
        cfStack.update()

        newEvents = eventList.difference(pastEvents)
        pastEvents = eventList

        for event in newEvents:
            log.debug("New stack event: " + event)

        if stackStatus == "CREATE_IN_PROGRESS":
            log.info("Still waiting for stack creation.")
            time.sleep(15)
        elif stackStatus == "CREATE_COMPLETE":
            log.info("Stack creation complete!")
            buildComplete = True

        else:
            log.error("Stack is in an unexpected state - \"%s\".  Launch failed!", stackStatus)
            raise RuntimeError("Stack creation failed - Cloud Formation reports stack status \"%s\".", stackStatus)

    if (timeout > 0) and (time.time() > deadline):
        log.error("Wait time (%ss) exceeded - aborting.", timeout)
        return False

    return True


def wait_for_valid_instance(ec2Conn, iid, timeout):
    instanceReady = False
    runTime = 0

    log.debug("\tWaiting for instance %s to enter running state...", iid)

    while instanceReady is False:
        # as of 2015 05 05, boto get_all_instance_status doesn't support the include_all_instances parameter described in the documentation
        # so getting instance status info actually tells us less about an instance that's not yet up and running
        instanceInfo = ec2Conn.get_only_instances(instance_ids=[iid])
        if instanceInfo:
            if ('running' in instanceInfo[0].state):
                log.debug("\t\tInstance exists and is running.")
                instanceReady = True
            else:
                log.debug("\t\tInstance id %s is not operational yet (state: \"%s\".) Waiting...", iid,
                          instanceInfo[0].state)
                time.sleep(10)
                runTime += 10
        else:
            log.debug("\t\tNo instances found for iid %s - waiting and retrying.", iid)
            time.sleep(10)
            runTime += 10

    if runTime > timeout:
        log.error("Exceeded time limit of " + str(
            timeout) + " seconds waiting for instance " + iid + " to enter running state.")
        raise RuntimeError("Time limit exceeded waiting for instance " + iid + " to enter running state.")

    return True


def getContentFromYaml(filePath):
    if (not os.path.isfile(filePath)):
        msg = "File " + filePath + " not found - cannot continue."
        raise IOError(msg)

    try:
        stream = file(filePath, 'r')
        yamlData = yaml.load(stream)

    except yaml.YAMLError, exc:

        log.critical("Could not parse YAML file '" + filePath + "' - " + exc.problem)
        if hasattr(exc, 'problem_mark'):
            mark = exc.problem_mark
            log.critical("Error position: line %s, character %s" % (mark.line + 1, mark.column + 1))
        raise RuntimeError("Parsing failure loading YAML - cannot continue.")
    except Exception, exc:
        log.critical("Load of YAML file '" + filePath + "' failed - " + exc.message)
        raise RuntimeError("Environment definition load/parse failed - cannot continue.")

    return yamlData


if __name__ == '__main__':

    initParser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    initParser.add_argument("gitMountPoint", help="Directory root for locally-stored files")
    initParser.add_argument("configfile", help="repo path to YAML file containing environment/system defs")
    initParser.add_argument("network", help="Type of network to launch")
    initParser.add_argument("version", help="Version of network to launch")
    initParser.add_argument("--verbose", help="Output debug progress messages", action="store_true")
    initParser.add_argument("--validateOnly", help="Validate definitions file only, then exit.", action="store_true")
    initParser.add_argument("--skipVNS3", help="Skip VNS3 search/provision step", action="store_true")
    initOptions, initArgs = initParser.parse_known_args()

    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("vns3api").setLevel(logging.DEBUG)
    log = logging.getLogger(__name__)

    boto.set_stream_logger('boto', level=60)
    logging.getLogger('requests').setLevel(level=logging.ERROR)

    if initOptions.verbose:
        log.setLevel(level=logging.DEBUG)
        log.info("Logging set to debug level.")
        # boto.set_stream_logger('boto', level=logging.DEBUG)
    else:
        log.setLevel(level=logging.INFO)
        log.info("Standard logging configured.")

    log.info("Starting up.")

    # Open defs file
    yamlFilePath = os.path.join(initOptions.gitMountPoint, initOptions.configfile)
    yamlData = getContentFromYaml(yamlFilePath)

    if initOptions.validateOnly:
        log.info("Definitions file parsed successfully - exiting.")
        sys.exit(0)

    # Verify that the specified network name exists
    if initOptions.network not in yamlData["Networks"]:
        raise RuntimeError("Network type '" + initOptions.network + "' not found in " + initOptions.configfile)
    # Verify that the specified version has been defined
    elif initOptions.version not in yamlData["Networks"][initOptions.network]:
        raise RuntimeError("Version '" + initOptions.version + "' not found in " + initOptions.configfile)
    # Otherwise grab just the target environment definition
    else:
        envDef = yamlData["Networks"][initOptions.network][initOptions.version]

    mainParser = argparse.ArgumentParser(add_help=True, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # Add the parameters that are required but not passed to the template
    log.debug("Loading non-template parameters:")
    for requiredInput in envDef["RequiredInput"]:
        log.debug("\t--" + requiredInput)
        mainParser.add_argument("--" + requiredInput, required=True)

    # Add the parameters that the template requires but don't have defaults in the config file
    log.debug("Loading template parameters for which values must be provided:")
    requiredParams = envDef["Template"]["RequiredInput"]
    for param in requiredParams:
        log.debug("\t--" + param)
        mainParser.add_argument("--" + param, required=True)

    # Add the parameters that have pre-configured defaults in the config file
    log.debug("Loading parameters for which default values are specified in definitions file:")
    for defaultedParam in envDef["Template"]["Parameters"]:
        for key, value in defaultedParam.items():
            log.debug("\t--" + key + " (default " + str(value) + " - type " + str(type(value)) + ")")
            mainParser.add_argument("--" + key, default=value, help=key)

    options = mainParser.parse_args(args=initArgs)

    templateParamList = []

    log.debug("Building key/value pair list to pass to Cloud Formation...")
    for key, value in vars(options).iteritems():
        if key not in envDef["RequiredInput"]:
            if key in templateParamList:
                log.debug(
                    "\tFound override for default template parameter " + key + " - new value is " + str(value) + ".")
                templateParamList[key] = value
            else:
                log.debug("\tAdding template parameter: " + key + " (" + str(value) + ")")
                templateParamList.append((key, value))
        else:
            log.debug("\tParameter " + key + " isn't a template parameter - not adding it.")

    log.debug("Passing list: " + templateParamList.__str__())

    log.debug("Launching template " + envDef["Template"]["FilePath"])
    log.debug("Stack name: " + options.EnvironmentName + ", region: " + options.AWSRegion)

    createStack(initOptions.gitMountPoint, envDef["Template"]["FilePath"], options.EnvironmentName, options.AWSRegion,
                templateParamList)

    if not initOptions.skipVNS3:
        log.info("Finding and configuring VNS3 instances...")
        configureVNS3Instances(options.AWSRegion, options.EnvironmentName, initOptions.gitMountPoint)

    exit(0)
