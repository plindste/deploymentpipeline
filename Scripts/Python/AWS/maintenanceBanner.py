import argparse
import boto3
import uuid
import sys


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--region', help='AWS Region to connect to. Example: us-west-2', required=True)
    parser.add_argument('--vpc', help='Name of environment to set banner for. Example: tscdev', required=True)
    parser.add_argument('--time', help='Time to display in banner')
    parser.add_argument('--clear', help='Clears the banner, takes precedence over --time', action='store_true')

    args = parser.parse_args(argv)

    key = 'banners/{}-maintenance-message.js'.format(args.vpc)
    bucket = 'spotfirecdn.cloud.tibco.com'

    s3 = boto3.client('s3')
    # link: "https://www.tibco.com/", linkText: "Read more",
    newUUID = uuid.uuid4()
    contents = 'window.maintenanceMessage = ' \
               '[ {{ text: "Spotfire will be down for maintenance starting {}",' \
               '  id: "{}" }} ]; '.format(args.time, newUUID)

    if args.clear:
        contents = ''

    s3.put_object(ACL='public-read', Bucket=bucket, Key=key, Body=contents, CacheControl='max-age:5,public')


if __name__ == '__main__':
    main(sys.argv[1:])
