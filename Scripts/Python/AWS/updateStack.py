__author__ = 'aseidl'

import argparse
import logging
import tscolibrary.aws
import boto3
import botocore
import time

if __name__ == '__main__':
    initParser = argparse.ArgumentParser(add_help = False, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    initParser.add_argument("stackName", help="Name of target CF stack")
    initParser.add_argument("--wait", help="Wait for stack update to complete before exiting", action="store_true")
    initParser.add_argument("--ignoreFail", help="Don't generate an exception if stack update fails (requires --wait)", action="store_true")
    initParser.add_argument("--listParamsOnly", help="Locate the target stack and print valid parameter names, then exit", action="store_true")
    initParser.add_argument("--verbose", help="Output debug progress messages", action="store_true")
    initOptions, initArgs = initParser.parse_known_args()

    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("vns3api").setLevel(logging.DEBUG)
    log = logging.getLogger(__name__)

    boto3.set_stream_logger('boto3', level=60)
    logging.getLogger('requests').setLevel(level=logging.ERROR)

    if initOptions.verbose:
        log.setLevel(level=logging.DEBUG)
        logging.getLogger("aws").setLevel(level=logging.DEBUG)
        log.info("Logging set to debug level.")
        #boto.set_stream_logger('boto', level=logging.DEBUG)
    else:
        log.setLevel(level=logging.INFO)
        log.info("Standard logging configured.")

    log.info("Starting up.")

    log.info("Finding target stack...")
    region = tscolibrary.aws.find_cf_stack_region(initOptions.stackName)

    log.info("Stack '" + initOptions.stackName + "' found in region " + region + ".")

    log.debug("Getting parameters and initializing main parser...")
    cfConn = boto3.client('cloudformation', region_name=region)

    stackParams = (cfConn.describe_stacks(StackName=initOptions.stackName))["Stacks"][0]["Parameters"]

    if initOptions.listParamsOnly:
        log.info("Valid parameters for stack '" + initOptions.stackName + "':")
        for param in stackParams:
            log.info("\t" + param["ParameterKey"] + " (current value: '" + str(param["ParameterValue"]) + "')")
        exit(0)

    mainParser = argparse.ArgumentParser(add_help=True, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    for param in stackParams:
        log.debug("Adding stack parameter " + param["ParameterKey"])
        mainParser.add_argument("--" + param["ParameterKey"], default="USEPREV")
    parameterValues = mainParser.parse_args(args=initArgs)

    stackParameterList = []
    for value in vars(parameterValues):
        paramValue = getattr(parameterValues, value)
        if paramValue == "USEPREV" :
            paramValuePair = { "UsePreviousValue" : bool("True") }
        else:
            paramValuePair = { "ParameterValue" : paramValue }
        entry = { "ParameterKey" : value }
        entry.update(paramValuePair)
        stackParameterList.append(entry)

    log.info("Triggering stack update.")
    log.debug("Parameter values: " + str(stackParameterList))

    updateRunning = False

    try:
        response = cfConn.update_stack(StackName=initOptions.stackName,UsePreviousTemplate=True,Capabilities=['CAPABILITY_IAM'],Parameters=stackParameterList)
        log.debug(response)

        stackStatus = (cfConn.describe_stacks(StackName=initOptions.stackName))["Stacks"][0]["StackStatus"]

        while stackStatus != "UPDATE_IN_PROGRESS":
            time.sleep(1)
            stackStatus = (cfConn.describe_stacks(StackName=initOptions.stackName))["Stacks"][0]["StackStatus"]

        log.info("Update of stack '" + response["StackId"] + "' initiated.")
        updateRunning = True

    except botocore.exceptions.ClientError as exc:
        if exc.response["Error"]["Code"] == "ValidationError":
            log.info("No changes were requested - no update triggered.")
            pass

    if initOptions.wait and updateRunning:
        log.info("Waiting for stack update to complete.")

        stackStatus = (cfConn.describe_stacks(StackName=initOptions.stackName))["Stacks"][0]["StackStatus"]
        lastStatus = stackStatus

        log.info("Stack status is now " + stackStatus)

        while stackStatus not in ("UPDATE_COMPLETE", "UPDATE_ROLLBACK_COMPLETE", "UPDATE_ROLLBACK_FAILED"):

            stackStatus = (cfConn.describe_stacks(StackName=initOptions.stackName))["Stacks"][0]["StackStatus"]
            if stackStatus != lastStatus:
                log.info("Stack status has changed from " + lastStatus + " to " + stackStatus)
                lastStatus = stackStatus

            if stackStatus == "UPDATE_COMPLETE":
                log.info("Stack update completed successfully.")
            elif stackStatus in ("UPDATE_ROOLBACK_COMPLETE", "UPDATE_ROLLBACK_FAILED"):
                msg = "Stack update failed!  Final status: " + stackStatus
                log.error(msg)
                if not initOptions.ignoreFail:
                    raise RuntimeError(msg)
            else:
                log.debug("Still waiting for stack update to complete.")
                time.sleep(15)
