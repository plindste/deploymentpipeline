__author__ = 'rsood'

import vns.vns3api as vns3api
import sys


def configureVnsInstance(id, publicIp, region, gitMountPoint):
    # The instance id is the default password for a newly-built appliance
    target_api_uid = "api"
    target_api_pwd = id
    target_topology_token = id
    target_topology_name = id
    try:
        if sys.platform == "win32":
            license_key_file = gitMountPoint + "\\tsc-config-storage\\vns3\\vns3-manager-test-license.txt"
            license_cfg_file = gitMountPoint + "\\tsc-config-storage\\vns3\\vns3-manager-config-standard.json"
            firewall_ruleset_file = gitMountPoint + "\\tsc-config-storage\\vns3\\vns3-firewall-rules-standard.txt"


        elif sys.platform in ("linux2", "darwin"):
            license_key_file = gitMountPoint + "/tsc-config-storage/vns3/vns3-manager-test-license.txt"
            license_cfg_file = gitMountPoint + "/tsc-config-storage/vns3/vns3-manager-config-standard.json"
            firewall_ruleset_file = gitMountPoint + "/tsc-config-storage/vns3/vns3-firewall-rules-standard.txt"

        # Run the various provisioning steps (see the vns3api.py file for more details)

        vns3api.vns3_wait_for_appliance_online(publicIp, target_api_uid, target_api_pwd, 600)
        vns3api.vns3_provision_new_instance(publicIp, target_api_uid, target_api_pwd, license_key_file,
                                            license_cfg_file, target_topology_token, target_topology_name, 1,
                                            firewall_ruleset_file, target_api_pwd, "vnscubed", id)

    except Exception, e:

        print "Could not configure the VNS3 instance", sys.exc_info()[0]
        print "VNS3 API Exception is : ", e
        raise
        sys.exit()
