import urllib3

__author__ = 'dvulcan, ahmed'
from datetime import datetime
from urlparse import urljoin

import argparse
import logging
import os
import requests
import sys
import tempfile
import urllib3
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session

'''

'''


class HTTPstatusError(Exception):
    def __init__(self, code):
        self.code = code


def check_httpstatus(statuscode):
    if statuscode != 200:
        raise HTTPstatusError(statuscode)


def check_nodes_services_status(session):
    json = fetch_nodes_services_instances(session)

    # Fetch information about all WEB_PLAYER destinations
    r = session.get(urljoin(baseurl, '/spotfire/wp/routing/destinations'))
    check_httpstatus(r.status_code)
    destinations_json = r.json()

    # 'Now'
    current_timestamp_string = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S,%f')[:-3]

    for server in json:
        node = server['node']

        server_id = node.get('serverId', '')
        server_name = node.get('serverName', '')
        platform = node.get('platform', '')
        online = node.get('online', False)
        product_version = node.get('productVersion', '')
        bundle_version = node.get('bundleVersion', '')
        remote = node.get('remote', False)

        print('%s;%s;%s;%s;%s;%s;%s;%s;%s' % (
            current_timestamp_string, server_id, server_name, 'ONLINE' if online else 'OFFLINE',
            'NM' if remote else 'TSS',
            server_name, platform, product_version, bundle_version))
        services = server.get('services')
        if services is None:
            continue

        for service in services:
            if service is None:
                continue

            # installedService = service.get('installedService')
            service_instances = service.get('serviceInstances', None)
            if service_instances is None:
                continue

            for serviceInstance in service_instances:
                if serviceInstance is None:
                    continue

                alias = serviceInstance.get('alias')
                base_service_config = serviceInstance.get('baseServiceConfig')
                name = base_service_config.get('name')
                service_instance_id = base_service_config.get('id')
                status = base_service_config.get('status')
                area_id = base_service_config.get('deploymentArea', '')
                # areaName = getDeploymentAreaName(deploymentAreas, area_id)
                capability_name = base_service_config.get('capabilities', [])[0]
                product_version = base_service_config.get('version', '')
                bundle_version = base_service_config.get('bundleVersion', '')
                service_url = base_service_config.get('serviceURL', '')

                # Currently only for instances with WEB_PLAYER capability
                destination_status = ''
                if capability_name == 'WEB_PLAYER':
                    destination = get_destination(service_url, destinations_json)
                    destination_status = '-' + destination.get('status', '') if destination is not None else '-MISSING'

                # TODO Check communication edges
                # /spotfire/nodemanager/status/service/<serviceInstanceId>

                print('%s;%s;%s;%s;%s;%s;%s;%s;%s' % (
                    current_timestamp_string, service_instance_id, server_name, status + destination_status,
                    capability_name,
                    alias if alias is not None else name, platform, product_version, bundle_version))

    return


def fetch_nodes_services_instances(session):
    r = session.get(urljoin(baseurl, '/spotfire/nodemanager/list/topology/services'))
    check_httpstatus(r.status_code)
    json = r.json()
    return json


def get_destination(service_url, destinations):
    for destination in destinations:
        uri = destination.get('uri', '')
        if uri.lower() == service_url.lower():
            return destination

    return None


def print_headers(args):
    print('Timestamp;Id;Host Name;Status;Capability;Name;Platform;Product Version;Version')

    return


def check_status(args):
    session = create_session(args)

    check_nodes_services_status(session)


def create_session(args):
    global baseurl
    urllib3.disable_warnings()
    if hasattr(args, '__dict__'):
        args = vars(args)
    baseurl = args['server']
    if not args['oauth']:
        # Initialize the web session
        session = requests.Session()
        session.auth = (args['user'], args['password'])
    else:
        # Initialize the web session
        session = OAuth2Session(client=BackendApplicationClient(client_id=args['user']))

    # Get the CSRF cookie
    r = session.get(urljoin(baseurl, '/spotfire/manifest'))
    check_httpstatus(r.status_code)
    # TODO Parse technical version to check if the internal APIs are available
    token = {'X-XSRF-TOKEN': session.cookies['XSRF-TOKEN']}
    session.headers.update(token)
    if args['oauth']:
        session.fetch_token(token_url=urljoin(baseurl,
                                              '/spotfire/oauth2/token?grant_type=client_credentials&scope=internal.tsc'
                                              '.monitor'),
                            client_id=args['user'], client_secret=args['password'])
    session.headers.update({"Content-Type": "application/json", "Accept": "application/json, text/plain, */*"})
    return session


def fetch_node_count(args):
    urllib3.disable_warnings()
    ret = {'TSS': 0}
    session = create_session(args)
    json = fetch_nodes_services_instances(session)

    for server in json:
        node = server['node']

        online = node.get('online', False)
        remote = node.get('remote', False)

        if online:
            if not remote:
                ret['TSS'] += 1

            services = server.get('services')
            if services is None:
                continue

            for service in services:
                if service is None:
                    continue

                service_instances = service.get('serviceInstances', None)
                if service_instances is None:
                    continue

                for serviceInstance in service_instances:
                    if serviceInstance is None:
                        continue

                    base_service_config = serviceInstance.get('baseServiceConfig')
                    capability_name = base_service_config.get('capabilities', [])[0]

                    if capability_name in ret:
                        ret[capability_name] += 1
                    else:
                        ret[capability_name] = 1

    print ret
    return ret


def main(argv):
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(help='commands')

    # Print headers
    print_headers_parser = subparsers.add_parser('print-headers', help='Prints the column headers')
    print_headers_parser.set_defaults(func=print_headers)

    # Check status
    check_status_parser = subparsers.add_parser('check-status', help='Checks the status of a Spotfire Server site')
    check_status_parser.add_argument('--user', '-u', help='User', default=None, required=True)
    check_status_parser.add_argument('--password', '-p', help='Password', default=None, required=True)
    check_status_parser.add_argument('--server', '-s', help='URL to Spotfire server with Admin functionality',
                                     default=None, required=True)
    check_status_parser.add_argument('--oauth',
                                     help='If oauth2 should be used, user argument will be used for ClientId and '
                                          'password for ClientSecret',
                                     action='store_true')
    check_status_parser.set_defaults(func=check_status)

    fetch_node_count_parser = subparsers.add_parser('fetch-node-count',
                                                    help='Fetches the number of nodes of a Spotfire Server site')
    fetch_node_count_parser.add_argument('--user', '-u', help='User', default=None, required=True)
    fetch_node_count_parser.add_argument('--password', '-p', help='Password', default=None, required=True)
    fetch_node_count_parser.add_argument('--server', '-s', help='URL to Spotfire server with Admin functionality',
                                         default=None, required=True)
    fetch_node_count_parser.add_argument('--oauth',
                                         help='If oauth2 should be used, user argument will be used for ClientId and '
                                              'password for ClientSecret',
                                         action='store_true')
    fetch_node_count_parser.set_defaults(func=fetch_node_count)

    logfile = os.path.join(tempfile.gettempdir(), 'current_status.log')
    logging.basicConfig(level=logging.DEBUG,
                        filename=logfile,
                        format='current_status %(asctime)s %(levelname)s %(message)s'
                        )
    try:
        args = parser.parse_args(argv)
        args.func(args)

    except HTTPstatusError as e:
        logging.error("Invalid HTTP Status code! Expected 200 but received %s" % e.code)
        sys.exit(1)

    except requests.exceptions.Timeout:
        logging.exception("Request timed out! Is tss available?\n")
        sys.exit(1)

    except requests.exceptions.ConnectionError:
        logging.exception("A networking error was encountered!\n")
        sys.exit(1)

    except (KeyboardInterrupt, SystemExit):
        logging.debug("Keyboard interrupt received, exiting")
        sys.exit()

    except:
        logging.exception("An unknown error occurred!\n")
        sys.exit(1)

    finally:
        logging.debug('Exited')


if __name__ == "__main__":
    main(sys.argv[1:])
