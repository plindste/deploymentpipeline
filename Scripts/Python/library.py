import os
import shutil
import subprocess
import logging
import re


def set_logger():
    """
    This function sets the logger to level DEBUG
    :return: logger Object
    """
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s')
    log = logging.getLogger(__name__)
    log.setLevel(level=logging.DEBUG)
    log.info("Logging set to debug level.")
    return log


log = set_logger()


def cat(file_name):
    with open(file_name, 'r') as fin:
        print fin.read()


def natural_sort(l):
    """
    Sorts naturally: 1 2 10
    instead of 1 10 2
    :param l: list to be sorted
    :return: sorted list
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {}
    for o in intersect_keys: 
        if d1[o] != d2[o]:
            modified[o] = (d1[o], d2[o])
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return added, removed, modified, same


def run_command_get_output(cmd):
    """
    This function returns the specified commadn and returns output of the command run
    :return: command run output
    """
    log.info('Executing command:' + cmd)
    try:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        p_status = p.wait()
        log.debug("Command execution return code is:" + str(p_status))
        if p_status != 0:
            raise Exception("Command execution return code is not zero. return code is :" + str(p_status))
        return output
    except ValueError as verr:
        log.error("Value error exception occured while executing command: " + cmd + "\n Error is:" + verr)
        raise Exception
    except OSError as oserr:
        log.error("OS Error occured while executing command: " + cmd + "\n Error is:" + oserr)
        raise Exception
    except Exception as exc:
        log.info('There was an exception while executing command  : ' + cmd + " Error is :" + str(exc.returncode))
        raise Exception


def mount_unc(source_mount_location, unc_domain, unc_user, unc_pass, path_base):
    """
    This function mounts specified source directory on a linux machine on specified base path
    :param source_mount_location: Specify absoute path of the directory on remote machine to mount.
    :param unc_domain: valid domain name
    :param unc_user: valid user name to connect
    :param unc_pass: valid password
    :param path_base: absolute path to the mount directory
    :return:
    """
    log.info("Checking for path :" + path_base + '/Products')
    if not os.path.ismount(path_base + '/Products'):
        log.info("Checking for path :" + path_base + '/Products' + " does not exists attempting to mount.")
        mountcmd = 'mount -t cifs ' + source_mount_location + ' ' + path_base + ' -o username=' + unc_user + ',domain=' + unc_domain + ',password=' + unc_pass
        run_command_get_output(mountcmd)
        log.info("Mount succeeded")
    else:
        log.info("Mount path " + path_base + " Already exists")


def copyContentDeliveryFile(sourceDir, vNumber, rolesFile):
    if not sourceDir.endswith('/'):
        sourceDir += '/'

    if os.path.exists(rolesFile):
        os.remove(rolesFile)

    shutil.copy2(sourceDir + vNumber + '/ContentDelivery/' + rolesFile, '.')
    cat(rolesFile)
