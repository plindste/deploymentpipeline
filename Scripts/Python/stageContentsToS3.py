import argparse
import yaml
import re
from library import *
import urllib
import os

currentWorkingDirectory = os.getcwd()
log = set_logger()


def download_file(url, to_directory):
    '''
    This function downloads the file from specified url to the specified directory
    :param url: Url path to the file on WEB
    :param to_directory: Absolute path to directory on machine where you want the file to get downloaded
    :return:
    '''
    log.info("Downloading from url:" + url)
    log.info("Downloading to directory:" + to_directory)
    log.info("target file name is:" + url.split("/")[-1])
    # urllib.urlretrieve(url, to_directory+"/"+url.split("/")[-1])
    # url = " https://s3.amazonaws.com/redshift-downloads/drivers/AmazonRedshiftODBC32-1.3.1.1000.msi "
    webFile = urllib.urlopen(url)
    localFile = open(to_directory + "/" + url.split('/')[-1], 'w')
    localFile.write(webFile.read())
    webFile.close()
    localFile.close()


def get_manifest_file(args):
    '''
    This function returns path of the manifest file
    :param args: args object
    :return: manifest file path object
    '''

    manifest_dir = os.path.join(args.unc_path_base, 'Manifests', args.manifest_products_directory, args.manifestVersion,
                                args.manifestBuild)
    log.info("Getting manifest file from mounted location:" + manifest_dir)
    os.chdir(manifest_dir)
    files = os.listdir(os.getcwd())
    manifest_file = os.path.join(manifest_dir, files[0])
    log.info("Manifest file is:" + manifest_file)
    return manifest_file


def get_md5sum(path, is_directory, md5file):
    '''
    This function fetches md5 sum of the specified file or files in the specified directory and appends to the specified md5 file.
    :param path: File or directory path for md5 sum
    :param is_directory:  True/False
    :param md5file: Output Md5sum file file
    :return:
    '''
    if is_directory:
        run_command = "cd \"" + path + "\" && find . -type f -exec md5sum {} +>>" + md5file
        recursive = " --recursive"
    else:
        run_command = '/usr/bin/md5sum "%s" >> "%s"' % (path, md5file)
        recursive = ""
    log.info("Running command :" + run_command)
    retvalue = os.system(run_command)
    if retvalue != 0:
        log.info('An error occurred while creating md5 checksum. Exiting program.')
        raise Exception


def upload_products(manifest_yml, roles_yml, md5file, unc_path_base, s3bucket, manifestVersion, manifestBuild):
    '''
    This function uploads all installers to S3 with respect to manifest.yml and roles.yml file
    :param manifest_yml: path to manifest.yaml file
    :param roles_yml: path to roles.yaml file
    :param md5file: Output path of md5 file.
    :param unc_path_base: mount directory base path
    :param s3bucket: path to s3 bucket
    :param manifestVersion: manifest version
    :param manifestBuild: manifest build
    :return:
    '''
    channel = manifest_yml['channel']
    for role in roles_yml['roles']:
        log.info('Role is:' + role['role'])
        artifacts_list = role['artifacts'].split(",")
        stage_artifacts(artifacts_list, manifestBuild, manifestVersion, manifest_yml, md5file, role, s3bucket,
                        unc_path_base)
    # Logic for downloads.
    if 'downloads' in roles_yml:
        for download in roles_yml['downloads']:
            log.info('download for :' + download['download'])
            artifacts_list = download['artifacts'].split(",")
            for downloads_artifacts in artifacts_list:
                artifact_found = False
                artifact_found = match_and_upload(artifact_found, channel, download, downloads_artifacts, manifestBuild,
                                                  manifestVersion, manifest_yml, md5file, s3bucket, unc_path_base)
                if not artifact_found:
                    log.info("No artifact found matching regular expression: " + downloads_artifacts)
                    raise Exception

    log.info('Upload complete.')


def match_and_upload(artifact_found, channel, download, downloads_artifacts, manifestBuild, manifestVersion,
                     manifest_yml, md5file, s3bucket, unc_path_base):
    for item in manifest_yml['platform']:
        for artifact in item['artifacts']:
            match_result = re.search(downloads_artifacts, artifact['artifact'])
            if match_result is not None:
                installer = (unc_path_base + "/Products/" + artifact['artifact']).replace("//", "/")
                log.info("installer to copy is:" + installer)
                is_directory = False
                if os.path.isdir(installer):
                    recursive = " --recursive"
                    is_directory = True
                else:
                    recursive = ""
                get_md5sum(installer, is_directory, md5file)

                # if os.path.basename(installer).lower() == role['role'].lower():
                if "deploymentscripts" in installer.lower():
                    installer_name = "Product/DeploymentScripts"
                else:
                    installer_name = "Product/" + os.path.basename(installer)
                # Get stage directory

                if 's3bucket' in download:
                    newS3Bucket = download['s3bucket']
                    print("Found s3 bucket.")
                else:
                    print("S3 new s3 bucket not found adding it to the original one.")
                    newS3Bucket = s3bucket

                if 'channel' in download:
                    newChannel = download['channel']
                    print("new channel is:" + newChannel)
                else:
                    print("Channel is not specified picking up the original one which is :" + channel)
                    newChannel = channel

                if 's3subdirs' in download:
                    s3subdirs = download['s3subdirs'] + "/"
                else:
                    s3subdirs = ""

                stage_dir = newS3Bucket + "/" + s3subdirs + manifestVersion + '/' + manifestBuild + '/' + installer_name

                # Stage installer to s3
                commandOutput = run_command_get_output(
                    'aws s3 cp "' + installer + '" "' + stage_dir + '"' + recursive)
                log.info("Command output:" + commandOutput)
                if "error" in commandOutput or "AccessDenied" in commandOutput:
                    log.error("There exists an error while copy to s3 please check.")
                    exit(1)
                artifact_found = True
    return artifact_found


def stage_artifacts(artifacts_list, manifestBuild, manifestVersion, manifest_yml, md5file, role, s3bucket,
                    unc_path_base):
    for role_artifact in artifacts_list:
        artifact_found = False
        for item in manifest_yml['platform']:
            for artifact in item['artifacts']:
                match_result = re.search(role_artifact, artifact['artifact'])
                if match_result is not None:
                    if artifact['artifact'].replace("'", "").startswith('URL:'):
                        installer, installer_name, recursive = handle_url_download(artifact)
                    else:
                        installer, installer_name, recursive = handle_sw_download(artifact, md5file, unc_path_base)
                    stage_dir = s3bucket + '/' + role[
                        'role'] + '/' + manifestVersion + '/' + manifestBuild + '/' + installer_name

                    # Stage installer to s3
                    commandOutput = run_command_get_output(
                        'aws s3 cp "' + installer + '" "' + stage_dir + '"' + recursive)
                    log.info("Command output:" + commandOutput)
                    if "error" in commandOutput or "AccessDenied" in commandOutput:
                        log.error("There exists an error while copy to s3 please check.")
                        exit(1)
                    artifact_found = True
        if not artifact_found:
            log.info("No artifact found matching regular expression: " + role_artifact)
            raise Exception


def handle_sw_download(artifact, md5file, unc_path_base):
    installer = (unc_path_base + "/Products/" + artifact['artifact']).replace("//", "/")
    log.info("installer to copy is:" + installer)
    is_directory = False
    if os.path.isdir(installer):
        recursive = " --recursive"
        is_directory = True
    else:
        recursive = ""
    get_md5sum(installer, is_directory, md5file)
    if "deploymentscripts" in installer.lower():
        installer_name = "Product/DeploymentScripts"
    else:
        installer_name = "Product/" + os.path.basename(installer)

    return installer, installer_name, recursive


def handle_url_download(artifact):
    # Handle for URL
    log.info("Artifact starts with URL:" + artifact['artifact'])
    downloadUrl = artifact['artifact'].replace("'", "").replace("URL:", "")
    download_file(downloadUrl, currentWorkingDirectory)
    installer = currentWorkingDirectory + "/" + downloadUrl.split('/')[-1]
    installer_name = "Product/" + downloadUrl.split('/')[-1]
    file_size = os.path.getsize(installer)
    recursive = ""
    log.info("Size of file on disk is:" + file_size.__str__())
    if file_size < 500:
        log.error(
            "Download from url " + downloadUrl + " Not completed successfully. size of the file is less than 500 Please verify the url " + downloadUrl)
        exit(1)
    else:
        log.info(
            "Download of file from url " + downloadUrl + " Completed successfully. size of the file is:" + file_size.__str__())
    return installer, installer_name, recursive


def configure_for_md5sum(manifest_version, manifest_build):
    '''
    Presetup for md5 sum file. This function creates the checksum file directory if it is not available, If md5 sum file already available then it removes that file.
    :param manifest_version: manifest version
    :param manifest_build: manifest build
    :return: Path to md5 sum file
    '''
    md5path = 'checksums/'
    md5dir = os.path.join(os.environ['WORKSPACE'], md5path)
    if not os.path.isdir(md5dir):
        os.makedirs(md5dir)
    else:
        pass
    md5filename = manifest_version + manifest_build + '.md5'
    md5file = os.path.join(md5dir, md5filename)
    # Delete if md5 sum file already exists.
    try:
        os.remove(md5file)
    except OSError:
        log.info("Failed to remove md5 sum file :" + md5file)
        pass
    return md5file


def main():
    '''
      This is a main function
     How to Run?
      python stageContentsToS3.py  --manifest_file /root/manifestfile.yml  --manifestVersion 99.0.0.0 --manifestBuild V01
     --s3bucket s3://pune-auto/manifest_installers  --unc_source_mount_location "//emea-got-filer1.emea.tibco.com/SWFactory"
     --unc_domain emea --unc_user iss-access --unc_password en0RierL1F --unc_path_base /mnt/softwarefactory/ --roles_yml roles.yml

    :return:
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--manifestVersion', help='Version of Yaml manifest file', required=True)
    parser.add_argument('--manifestBuild', help='Build of Yaml manifest file', required=True)
    parser.add_argument('--s3bucket', help='Base path of the S3 Bucket', required=True)
    parser.add_argument('--unc_domain', help='Domain of the LDAP service account', required=True)
    parser.add_argument('--unc_user', help='Username of the LDAP service account', required=True)
    parser.add_argument('--unc_password', help='Password for the LDAP service account', required=True)
    parser.add_argument('--unc_path_base', help='Location of the mounted SWFactory share', required=True)
    parser.add_argument('--manifest_file',
                        help='Hard-coded path to manifestFile this is optional if not specified it will automatically take from mount location',
                        required=False, default="")
    parser.add_argument('--unc_source_mount_location', help='Source mount directory', required=True)
    parser.add_argument('--roles_yml', help='Roles yml file path', required=True)
    parser.add_argument('--manifest_products_directory',
                        help='Specify manifest product directory. This is optional argument default value is TIBCO Spotfire Cloud',
                        required=False, default="TIBCO Spotfire Cloud")

    args = parser.parse_args()

    mount_unc(args.unc_source_mount_location, args.unc_domain, args.unc_user, args.unc_password, args.unc_path_base)

    # Get manifest file from mount
    if args.manifest_file == "":
        manifest_file = get_manifest_file(args)
    else:

        manifest_dir = os.path.join(args.unc_path_base, 'Manifests', args.manifest_products_directory,
                                    args.manifestVersion,
                                    args.manifestBuild)
        manifest_file = manifest_dir + "/" + args.manifest_file
    log.info("manifest file is :" + manifest_file)

    # Load manifest file
    manifest_yml = yaml.load(open(manifest_file))
    roles_yaml = yaml.load(open(args.roles_yml))

    # Pre configuration for md5 sum file
    md5file = configure_for_md5sum(args.manifestVersion, args.manifestBuild)

    # Read manifest file and upload products to s3
    log.info('Uploading products to s3\n')
    upload_products(manifest_yml, roles_yaml, md5file, args.unc_path_base, args.s3bucket, args.manifestVersion,
                    args.manifestBuild)

    # Upload manifest file to s3
    # run_command_get_output('aws s3 cp "' + manifest_file + '" "' + args.s3bucket +'/' + args.manifestVersion + '/' + args.manifestBuild + '/' + '"')
    # log.info ("Successfully uploaded manifest file to s3")

    # Upload manifest file to s3
    run_command_get_output(
        'aws s3 cp "' + manifest_file + '" "' + args.s3bucket + '/EDGE/' + args.manifestVersion + '/' + args.manifestBuild + '/Product/' + '"')
    log.info("Successfully uploaded manifest file to s3")

    # Upload md5 checksum file to s3
    s3_md5file = args.s3bucket + '/' + args.manifestVersion + '/' + args.manifestBuild + '/' + 'manifest.md5'
    # log.info ('Pushing checksum file for manifest to s3')
    # run_command_get_output('aws s3 cp "' + md5file + '" "' + s3_md5file + ' "')
    # log.info ('Checksum file uploaded successfully.')


main()
